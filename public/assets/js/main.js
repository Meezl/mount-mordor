$(function() {

    "use strict";


    //===== Prealoder

    $(window).on('load', function(event) {
        $('.preloader').delay(500).fadeOut(500);
    });


    //===== Mobile Menu

    $(".navbar-toggler").on('click', function() {
        $(this).toggleClass('active');
    });

    $(".navbar-nav a").on('click', function() {
        $(".navbar-toggler").removeClass('active');
    });


    //===== close navbar-collapse when a  clicked

    $(".navbar-nav a").on('click', function () {
        $(".navbar-collapse").removeClass("show");
    });


    //===== Sticky

    $(window).on('scroll',function(event) {
        var scroll = $(window).scrollTop();
        if (scroll < 10) {
            $(".header-area").removeClass("sticky");
        }else{
            $(".header-area").addClass("sticky");
        }
    });


    //===== One Page Nav

    $('#nav').onePageNav({
        filter: ':not(.external)',
        currentClass: 'active',
    });


    //=====  Slick

    function mainSlider() {
        var BasicSlider = $('.slider-active');
        BasicSlider.on('init', function(e, slick) {
            var $firstAnimatingElements = $('.single-slider:first-child').find('[data-animation]');
            doAnimations($firstAnimatingElements);
        });
        BasicSlider.on('beforeChange', function(e, slick, currentSlide, nextSlide) {
            var $animatingElements = $('.single-slider[data-slick-index="' + nextSlide + '"]').find('[data-animation]');
            doAnimations($animatingElements);
        });
        BasicSlider.slick({
            autoplay: true,
            autoplaySpeed: 5000,
            dots: true,
            fade: true,
			arrows: false,
            responsive: [
                { breakpoint: 767, settings: { dots: false, arrows: false } }
            ]
        });

        function doAnimations(elements) {
            var animationEndEvents = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
            elements.each(function() {
                var $this = $(this);
                var $animationDelay = $this.data('delay');
                var $animationType = 'animated ' + $this.data('animation');
                $this.css({
                    'animation-delay': $animationDelay,
                    '-webkit-animation-delay': $animationDelay
                });
                $this.addClass($animationType).one(animationEndEvents, function() {
                    $this.removeClass($animationType);
                });
            });
        }
    }
    mainSlider();



    //=====  Slick product items active

    $('.product-items-active').slick({
        dots: false,
        infinite: true,
        speed: 600,
        slidesToShow: 3,
        slidesToScroll: 1,
        adaptiveHeight: true,
        arrows:true,
        prevArrow:'<span class="prev"><i class="lni-chevron-left"></i></span>',
        nextArrow: '<span class="next"><i class="lni-chevron-right"></i></span>',
        responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 3,
          }
        },
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 2,
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 2,
            arrows: false,
          }
        },
        {
          breakpoint: 576,
          settings: {
            slidesToShow: 1,
            arrows: false,
          }
        }
        ]
    });


    //=====  Slick Showcase active

    $('.showcase-active').slick({
        dots: false,
        infinite: true,
        speed: 600,
        slidesToShow: 1,
        arrows:true,
        prevArrow:'<span class="prev"><i class="lni-arrow-left"></i></span>',
        nextArrow: '<span class="next"><i class="lni-arrow-right"></i></span>',
        adaptiveHeight: true,
        responsive: [
        {
          breakpoint: 768,
          settings: {
            arrows: false,
          }
        },
        {
          breakpoint: 576,
          settings: {
            arrows: false,
          }
        }
        ]
    });


    //=====  Slick testimonial active

    $('.testimonial-active').slick({
        dots: false,
        autoplay: true,
        autoplaySpeed: 2000,
        infinite: true,
        speed: 600,
        slidesToShow: 1,
        arrows:false,
        adaptiveHeight: true,
    });


    //====== Magnific Popup

    $('.Video-popup').magnificPopup({
        type: 'iframe'
        // other options
    });


    //===== Back to top

    // Show or hide the sticky footer button
    $(window).on('scroll', function(event) {
        if($(this).scrollTop() > 600){
            $('.back-to-top').fadeIn(200)
        } else{
            $('.back-to-top').fadeOut(200)
        }
    });

    //Animate the scroll to yop
    $('.back-to-top').on('click', function(event) {
        event.preventDefault();

        $('html, body').animate({
            scrollTop: 0,
        }, 1500);
    });


    //====== Slick product image

    $('.product-image').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        prevArrow: '<span class="prev"><i class="lni-chevron-left"></i></span>',
        nextArrow: '<span class="next"><i class="lni-chevron-right"></i></i></span>',
        dots: false,
    });


    //====== Nice Number

    $('input[type="number"]').niceNumber({

    });


    //=====  Rating selection

    var $star_rating = $('.star-rating .fa');

    var SetRatingStar = function() {
      return $star_rating.each(function() {
        if (parseInt($star_rating.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
          return $(this).removeClass('fa-star-o').addClass('fa-star');
        } else {
          return $(this).removeClass('fa-star').addClass('fa-star-o');
        }
      });
    };

    $star_rating.on('click', function() {
      $star_rating.siblings('input.rating-value').val($(this).data('rating'));
      return SetRatingStar();
    });

    SetRatingStar();

});


// var io={ajax:function(e,n,t,r){var o=new XMLHttpRequest;o.open(r||"POST",e,!0),o.onreadystatechange=function(){4==o.readyState&&200!=o.status&&t&&t(null),4==o.readyState&&200==o.status&&t&&t(o.responseText?JSON.parse(o.responseText):null)},o.setRequestHeader("Content-type","application/x-www-form-urlencoded"),o.send(n?io.query(n):null)},urlencode:function(e){return e=(e+"").toString(),encodeURIComponent(e).replace(/!/g,"%21").replace(/'/g,"%27").replace(/\(/g,"%28").replace(/\)/g,"%29").replace(/\*/g,"%2A").replace(/%20/g,"+")},query:function(e,n,t){var r,o,u=[],a=this,c=function(e,n,t){var r,o=[];if(!0===n?n="1":!1===n&&(n="0"),null!==n&&"object"==typeof n){for(r in n)null!==n[r]&&o.push(c(e+"["+r+"]",n[r],t));return o.join(t)}return"function"!=typeof n?a.urlencode(e)+"="+a.urlencode(n):"function"==typeof n?"":void 0};t||(t="&");for(o in e)r=e[o],n&&!isNaN(o)&&(o=String(n)+o),u.push(c(o,r,t));return u.join(t)}};
//
// var params = {
//     "key" : "ae48488c6ef1ea95354d3ffb5c496ca8",
//     "entities" : {
//         "articles" : {
//             "entity" : "articles",
//             "details" : ["pageviews", "author", "category", "sources"],
//             "filters" : {
//                 "category" : "Newsdesk"
//             }
//         }
//     },
//     "options" : {
//         "period" : {
//             "name" : "month"
//         },
//         "per_page" : 4
//     }
// };
//
// io.ajax("https://api.onthe.io/ata6CLk8UhmPPvS3PfZYx5wKAloQz24K", params, function (r) {
//
//     var articles = r.articles.list;
//     var list = '<ul class="list-group list-group-flush card-anchor">';
//     articles.forEach(function (article) {
//         list += "<li class=\"list-group-item\">";
//         list += "<h4 class='mb-2'><a href='https://www.standardmedia.co.ke" + article.url + "'>" + article.page + "</a></h4>";
//         var author = article.author;
//         list += "<h6 class='my-1'><span class='by-line'>By " + author +  "</span></h6>";
//         list += "</li>";
//     });
//     list += '</ul>';
//     document.getElementById("popular-news").innerHTML = list;
// });
//
//
// var output={ajax:function(e,n,t,r){var o=new XMLHttpRequest;o.open(r||"POST",e,!0),o.onreadystatechange=function(){4==o.readyState&&200!=o.status&&t&&t(null),4==o.readyState&&200==o.status&&t&&t(o.responseText?JSON.parse(o.responseText):null)},o.setRequestHeader("Content-type","application/x-www-form-urlencoded"),o.send(n?io.query(n):null)},urlencode:function(e){return e=(e+"").toString(),encodeURIComponent(e).replace(/!/g,"%21").replace(/'/g,"%27").replace(/\(/g,"%28").replace(/\)/g,"%29").replace(/\*/g,"%2A").replace(/%20/g,"+")},query:function(e,n,t){var r,o,u=[],a=this,c=function(e,n,t){var r,o=[];if(!0===n?n="1":!1===n&&(n="0"),null!==n&&"object"==typeof n){for(r in n)null!==n[r]&&o.push(c(e+"["+r+"]",n[r],t));return o.join(t)}return"function"!=typeof n?a.urlencode(e)+"="+a.urlencode(n):"function"==typeof n?"":void 0};t||(t="&");for(o in e)r=e[o],n&&!isNaN(o)&&(o=String(n)+o),u.push(c(o,r,t));return u.join(t)}};
//
// var parameters = {
//     "key" : "ae48488c6ef1ea95354d3ffb5c496ca8",
//     "entities" : {
//         "articles" : {
//             "entity" : "articles",
//             "details" : ["pageviews", "author", "category", "sources"],
//             "filters" : {
//                 "category" : "Newsdesk"
//             }
//         }
//     },
//     "options" : {
//         "period" : {
//             "name" : "today"
//         },
//         "per_page" : 5
//     }
// };
//
// output.ajax("https://api.onthe.io/ata6CLk8UhmPPvS3PfZYx5wKAloQz24K", parameters, function (r) {
//
//     var articles = r.articles.list;
//     var theHtml = '<ul class="list-group list-group-flush">';
//     var i = 1;
//     articles.forEach(function (article) {
//         theHtml += "<a href='https://www.standardmedia.co.ke" + article.url + "'><li style='border-bottom: 1px solid rgba(0,0,0,.125);' class=\"list-group-item\"> ";
//         theHtml += article.page + "</li>";
//         theHtml += "</a>";
//         i++;
//     });
//     theHtml += '</ul>';
//     document.getElementById("trending-today").innerHTML = theHtml;
// });
//
//
// var category={ajax:function(e,n,t,r){var o=new XMLHttpRequest;o.open(r||"POST",e,!0),o.onreadystatechange=function(){4==o.readyState&&200!=o.status&&t&&t(null),4==o.readyState&&200==o.status&&t&&t(o.responseText?JSON.parse(o.responseText):null)},o.setRequestHeader("Content-type","application/x-www-form-urlencoded"),o.send(n?io.query(n):null)},urlencode:function(e){return e=(e+"").toString(),encodeURIComponent(e).replace(/!/g,"%21").replace(/'/g,"%27").replace(/\(/g,"%28").replace(/\)/g,"%29").replace(/\*/g,"%2A").replace(/%20/g,"+")},query:function(e,n,t){var r,o,u=[],a=this,c=function(e,n,t){var r,o=[];if(!0===n?n="1":!1===n&&(n="0"),null!==n&&"object"==typeof n){for(r in n)null!==n[r]&&o.push(c(e+"["+r+"]",n[r],t));return o.join(t)}return"function"!=typeof n?a.urlencode(e)+"="+a.urlencode(n):"function"==typeof n?"":void 0};t||(t="&");for(o in e)r=e[o],n&&!isNaN(o)&&(o=String(n)+o),u.push(c(o,r,t));return u.join(t)}};
//
// var parameter = {
//     "key" : "ae48488c6ef1ea95354d3ffb5c496ca8",
//     "entities" : {
//         "articles" : {
//             "entity" : "articles",
//             "details" : ["pageviews", "author", "category", "sources"],
//             "filters" : {
//                 "category" : "Newsdesk"
//             }
//         }
//     },
//     "options" : {
//         "period" : {
//             "name" : "today"
//         },
//         "per_page" : 5
//     }
// };
//
// output.ajax("https://api.onthe.io/ata6CLk8UhmPPvS3PfZYx5wKAloQz24K", parameter, function (r) {
//
//     var articles = r.articles.list;
//     var theHtml = '<ul class="list-group list-group-flush">';
//     var i = 1;
//     articles.forEach(function (article) {
//         theHtml += "<a href='https://www.standardmedia.co.ke" + article.url + "'><li style='border-bottom: 1px solid rgba(0,0,0,.125);' class=\"list-group-item\">";
//         theHtml += article.page + "</li>";
//         theHtml += "</a>";
//         i++;
//     });
//     theHtml += '</ul>';
//     document.getElementById("trending-category").innerHTML = theHtml;
// });




