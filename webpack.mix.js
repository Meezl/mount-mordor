const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css');

mix.styles([
    'public/assets/css/responsive.css',
    'public/assets/css/style.css',
    'public/assets/css/slick.css',
], 'public/assets/css/index.css');
mix.minify('public/assets/css/index.css')

mix.styles([
    'public/assets/css/animate.css',
    'public/assets/css/default.css',
    'public/assets/css/LineIcons.css',
    'public/assets/css/magnific-popup.css',
], 'public/assets/css/all.css');
mix.minify('public/assets/css/all.css')


mix.scripts([
    'public/assets/js/main.js',
    'public/assets/js/slick.min.js',
    'public/assets/js/jquery.nav.js',
], 'public/assets/js/all.js');
mix.minify('public/assets/js/all.js')

mix.scripts([
    'public/assets/js/jquery.magnific-popup.min.js',
    'public/assets/js/jquery.nice-number.min.js',
], 'public/assets/js/jq-mag-nav.js');
mix.minify('public/assets/js/jq-mag-nav.js')

mix.minify('public/assets/js/lazy.js')
