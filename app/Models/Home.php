<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use DateTime;

class Home extends Model{

    private $now;

    public function __construct(array $attributes = []){
        parent::__construct($attributes);
        $this->now = date("Y-m-d H:i:s");
    }

    public function getHomeLatest($limit, $start = 0){
        $articles = DB::table("std_article")
                        ->select("std_article.id", "std_article.title", "std_article.long_title", "std_article.thumbURL", "std_article.categoryid", "std_article.source", "std_article.publishdate", "std_article.author", "std_article.summary")
                        ->whereNull("inactive")
                        ->where("publishdate", "<=", $this->now)
                        ->whereNotNull("homepagelistorder")
                        ->where("source","main")
                        ->orderBy("publishday","desc")
                        ->orderBy("homepagelistorder","asc")
                        ->orderBy("listorder","asc")
                        ->limit($limit)
                        ->offset($start)
                        ->get();
        if (isset($articles)){
            return $articles;
        }
    }

    public function getLatest($limit, $start, $id = array(1)){
        $publishDate = $this->now;
        $articles    = DB::table("std_article")
                            ->select("std_article.id", "std_article.title", "std_article.long_title", "std_article.thumbURL", "std_article.categoryid" ,"std_article.source", "std_article.publishdate", "std_article.author", "std_article.summary")
                            ->whereNull("inactive")
                            ->where("publishdate","<=", $publishDate)
                            ->where("source",'main')
                            ->whereNotIn("id", $id)
                            ->orderBy("publishdate","DESC")
                            ->limit($limit)
                            ->offset($start)
                            ->get();
        if($articles) {
            if(count($articles) > 0) {
                return $articles;
            }
        }
    }

    public function getCategoryTop($catID, $limit , $start = 0){
        $publishDate = $this->now;
        $articles = DB::connection('mysql')->table("std_article")
                        ->select("std_article.id", "std_article.title", "std_article.long_title", "std_article.thumbURL", "std_article.categoryid" ,"std_article.source", "std_article.publishdate", "std_article.author", "std_article.summary")
                        ->join("std_category","std_category.id","=","std_article.categoryid")
                        ->whereNull("std_article.inactive")
                        ->where("std_article.publishdate", "<=", $publishDate)
                        ->where("std_article.source","main")
                        ->where("std_category.id","=", $catID)
                        ->orWhere("std_category.parentid","=", $catID)
                        ->orderBy("std_article.publishdate","DESC")
                        ->orderBy("std_article.listorder","ASC")
                        ->orderBy("std_article.parentcategorylistorder","ASC")
                        ->limit($limit)
                        ->offset($start)
                        ->get()->toArray();
        $rows = count($articles);
        if($articles) {
            if($rows > 0) {
                $nextBatch = $articles;
                if($rows < $limit) {
                    $id = $nextBatch[(int)($rows-1)]->id;
                    $limit = $limit - $rows;
                    $articlesOne = DB::connection('mysql3')->table('std_article')
                                    ->select("std_article.id", "std_article.title", "std_article.long_title", "std_article.thumbURL", "std_article.categoryid" ,"std_article.source", "std_article.publishdate", "std_article.author", "std_article.summary")
                                    ->join("std_category","std_category.id","=","std_article.categoryid")
                                    ->whereNull("std_article.inactive")
                                    ->where("publishdate", "<=", $publishDate)
                                    ->where("std_article.id", "<", $id)
                                    ->where("source","main")
                                    ->where("std_category.id","=", $catID)
                                    ->orWhere("std_category.parentid","=", $catID)
                                    ->orderBy("std_article.publishdate","DESC")
                                    ->orderBy("std_article.listorder","ASC")
                                    ->orderBy("std_article.parentcategorylistorder","ASC")
                                    ->limit($limit)
                                    ->offset($start)
                                    ->get()->toArray();
                    if($articles) {
                        return array_merge($nextBatch, $articlesOne);
                    }

                } else {
                    return $nextBatch;
                }
            } else {
                $articles = DB::connection('mysql3')->table('std_article')
                                ->select("std_article.id", "std_article.title", "std_article.long_title", "std_article.thumbURL", "std_article.categoryid" ,"std_article.source", "std_article.publishdate", "std_article.author", "std_article.summary")
                                ->join("std_category","std_category.id","=","std_article.categoryid")
                                ->whereNull("std_article.inactive")
                                ->where("std_article.source","main")
                                ->where("std_category.id","=", $catID)
                                ->orderBy("std_article.publishdate","DESC")
                                ->orderBy("std_article.listorder","ASC")
                                ->orderBy("std_article.parentcategorylistorder","ASC")
                                ->limit($limit)
                                ->offset($start)
                                ->get();
                if($articles) {
                    if(count($articles) > 0) {
                        return $articles;
                    } else {
                       $articles = DB::connection('mysql2')->table('std_article')
                                        ->select("std_article.id", "std_article.title", "std_article.long_title", "std_article.thumbURL", "std_article.categoryid" ,"std_article.source", "std_article.publishdate", "std_article.author", "std_article.summary")
                                        ->join("std_category","std_category.id","=","std_article.categoryid")
                                        ->whereNull("std_article.inactive")
                                        ->where("std_article.source","main")
                                        ->where("std_category.id","=", $catID)
                                        ->orWhere("std_category.parentid","=", $catID)
                                        ->orderBy("std_article.publishdate","DESC")
                                        ->orderBy("std_article.listorder","ASC")
                                        ->orderBy("std_article.parentcategorylistorder","ASC")
                                        ->limit($limit)
                                        ->offset($start)
                                        ->get();
                        if(count($articles) > 0) {
                            return $articles;
                        }
                    }
                }
            }
        }

    }

    public function getCategoryName($catId, $site = "main") {
        $catId 	    =	(int)$catId;
        $category = DB::table("std_category")
                            ->where("id",$catId)
                            ->where("site", $site)
                            ->whereNull("inactive")
                            ->get()->toArray();
        if (isset($category[0])){
            return $category[0]->name;
        }else {
            if ($catId == 6){
                return "sports";
            }elseif ($catId == 4){
                return "business";
            }elseif ($catId == 5){
                return "world";
            }elseif ($catId == 3){
                return "politics";
            }else {
                return "The Standard";
            }

        }
    }

    public function getCategoryNameOtherSite($catId, $site = "main") {
        $catId 	    =	(int)$catId;
        return DB::table("std_category")
                    ->where("id", $catId)
                    ->where("site", $site)
                    ->whereNull("inactive")
                    ->get();

    }
    public function getCatId($id){
        $id 	=	(int)$id;
        $result  = DB::table("std_category")
                        ->where("id",  $id)
                        ->select("id", "parentid as pid")
                        ->get()->toArray();
        if($result[0]) {
            return $result[0];
        }
    }

    public function getRelatedArticles($keywords, $id, $limit = 5, $start = 0){
        $words      =   explode(';', $keywords);
        $count = 0;
        $wordsCount = count($words);
        $firstQuery = DB::table('std_article')
                                ->select("std_article.id", "std_article.title", "std_article.long_title", "std_article.thumbURL", "std_article.categoryid", "std_article.source", "std_article.publishdate", "std_article.author", "std_article.summary")
                                ->whereNull("inactive")
                                ->where("source", "main")
                                ->whereNotIn("id", [$id])
                                ->where("keywords", "like", "%" . addslashes($words[0]) . "%");

        unset($words[0]);
        foreach($words as $value) {
            if ($count <= 3) {
                if ($wordsCount > 1 && $value != "") {
                    $subQuery = DB::table('std_article')
                                    ->select("std_article.id", "std_article.title", "std_article.long_title", "std_article.thumbURL", "std_article.categoryid", "std_article.source", "std_article.publishdate", "std_article.author", "std_article.summary")
                                    ->whereNull("inactive")
                                    ->where("source", "main")
                                    ->whereNotIn("id", [$id])
                                    ->where("keywords", "like", "%" . addslashes($value) . "%");
                    $query = $firstQuery->unionAll($subQuery);
                    $count++;
                }
            }
        }
        if(isset($query)){
            return $articles = $query->limit($limit)->offset($start)->get();
        }
    }


    public function getLatestVideos($limit, $start = 0){
        $videos = DB::table("ktn_video")
                ->join("ktn_video_category","ktn_video_category.id", "=", "ktn_video.categoryid")
                ->join("ktn_video_type","ktn_video_type.id", "=", "ktn_video_category.videotypeid")
                ->whereNull("ktn_video.inactive")
                ->orderBy("ktn_video.publishdate","DESC")
                ->select("ktn_video.*", "source")
                ->limit($limit)
                ->offset($start)
                ->get();

        if($videos) {
            if(count($videos) > 0) {
                return $videos;
            }
        }
    }

    public function getOtherSites($name, $limit, $start = 0) {
        $articles = DB::table('std_article')
                        ->where("source", $name)
                        ->whereNull("inactive")
                        ->orderBy("publishdate","DESC")
                        ->orderBy("homepagelistorder","ASC")
                        ->orderBy("listorder","ASC")
                        ->limit($limit)
                        ->offset($start)
                        ->get();
        if($articles) {
            if(count($articles) > 0) {
                return $articles;
            }
        }
    }

    public function getArticle($id){
        $id  =  (int)$id;
        $article = DB::table('std_article')
                        ->where("id", $id)
                        ->whereNull("inactive")
                        ->first();
        if($article) {
                return $article;
            } else {
                $article = DB::connection('mysql3')->table('std_article')
                    ->where("id", $id)
                    ->whereNull("inactive")
                    ->first();
                if ($article) {
                    return $article;
                } else {
                    $article = DB::connection('mysql2')->table('std_article')
                        ->where("id", $id)
                        ->whereNull("inactive")
                        ->first();
                    if ($article) {
                        return $article;
                    } else {
                        redirect('/');
                    }
                }
            }
    }

    public function getArticleCategory($id){
        $id 	=	(int)$id;
        $result 	= DB::table('std_article')
                        ->select("categoryid, title")
                        ->where("id", $id)
                        ->first();
        if($result){
            return $result->categoryid;
        }
    }


    public static function getVideo($id){
        $video = DB::table('ktn_video')->select("ktn_video.*","ktn_video_category.name as catname")
                        ->join("ktn_video_category","ktn_video_category.id","=","ktn_video.categoryid")
                        ->join("ktn_video_type","ktn_video_type.id","=","ktn_video_category.videotypeid")
                        ->whereNull("ktn_video.inactive")
                        ->where("ktn_video_type.source","main")
                        ->where("ktn_video.id",$id)
                        ->orderBy("ktn_video.publishdate","DESC")
                        ->orderBy("ktn_video.listorder","ASC")
                        ->first();
        if ($video){
            return $video;
        }
    }


    public function checkSource($id){
        $id  = 	(int)$id;
        $source =  DB::connection('mysql')->table('std_article')
                        ->join("std_category","std_category.id", "=" ,"std_article.categoryid")
                        ->where("std_article.id", $id)
                        ->select("std_category.id as id", "std_category.parentid as pid")
                        ->get()->toArray();
        if($source) {
            if(count($source) >  0) {
                return $source[0];
            } else {
                $source =  DB::connection('mysql3')->table('std_article')
                                ->join("std_category","std_category.id", "=" ,"std_article.categoryid")
                                ->where("std_article.id", $id)
                                ->select("std_category.id as id", "std_category.parentid as pid")
                                ->get()->toArray();
                if(count($source) > 0) {
                    return $source[0];
                } else {
                    $$source =  DB::connection('mysql2')->table('std_article')
                                    ->join("std_category","std_category.id", "=" ,"std_article.categoryid")
                                    ->where("std_article.id", $id)
                                    ->select("std_category.id as id", "std_category.parentid as pid")
                                    ->get()->toArray();
                    if(count($source) > 0) {
                        return $source[0];
                    }
                }
            }
        } else {
            print_r("Error!!");
        }

    }

    public static function getCategorySample($id,$limit=7,$start=0){
        $publishDate = date("Y-m-d H:i:s");
        $catID = (int)$id;
        $articles = DB::connection('mysql2')->table("std_article")
            ->select("std_article.id", "std_article.title", "std_article.long_title", "std_article.thumbURL", "std_article.categoryid" ,"std_article.source", "std_article.publishdate", "std_article.author", "std_article.summary")
            ->join("std_category","std_category.id","=","std_article.categoryid")
            ->whereNull("std_article.inactive")
            ->where("std_article.publishdate", "<=", $publishDate)
            ->where("std_article.source","main")
            ->where("std_category.id","=", $catID)
            ->orWhere("std_category.parentid","=", $catID)
            ->orderBy("std_article.publishdate","DESC")
            ->limit($limit)
            ->offset($start)
            ->get()->toArray();

        return count($articles);
    }


    public function getAuthorContent($name, $limit, $start=0) {
        $name 	= 	str_replace("-", " ",$name);
        $articles = DB::connection('mysql')->table('std_article')
                        ->select("std_article.id", "std_article.title", "std_article.long_title", "std_article.thumbURL", "std_article.categoryid", "std_article.source", "std_article.publishdate", "std_article.author", "std_article.summary")
                        ->whereNull("inactive")
                        ->where("author", "like", "%" . addslashes($name) . "%")
                        ->where("source","main")
                        ->orderBy("publishdate","DESC")
                        ->limit($limit)
                        ->offset($start)
                        ->get();
        if($articles) {
            if(count($articles) > 0) {
                return $articles;
            }else{
                $articlesOne = DB::connection('mysql3')->table('std_article')
                                    ->select("std_article.id", "std_article.title", "std_article.long_title", "std_article.thumbURL", "std_article.categoryid", "std_article.source", "std_article.publishdate", "std_article.author", "std_article.summary")
                                    ->whereNull("inactive")
                                    ->where("author", "like", "%" . addslashes($name) . "%")
                                    ->where("source","main")
                                    ->orderBy("publishdate","DESC")
                                    ->limit($limit)
                                    ->offset($start)
                                    ->get();
                if(count($articlesOne) > 0) {
                    return $articlesOne;
                }else{
                    $articlesTwo = DB::connection('mysql2')->table('std_article')
                                        ->select("std_article.id", "std_article.title", "std_article.long_title", "std_article.thumbURL", "std_article.categoryid", "std_article.source", "std_article.publishdate", "std_article.author", "std_article.summary")
                                        ->whereNull("inactive")
                                        ->where("author", "like", "%" . addslashes($name) . "%")
                                        ->where("source","main")
                                        ->orderBy("publishdate","DESC")
                                        ->limit($limit)
                                        ->offset($start)
                                        ->get();
                    if(count($articlesTwo) > 0) {
                        return $articlesTwo;
                    }
                }
            }
        }
    }

    public function getAuthorArticlesCount($name) {
        $name 	= 	str_replace("-", " ",$name);
        $articles = DB::connection('mysql')->table('std_article')
                        ->whereNull("inactive")
                        ->where("author", "like", "%" . addslashes($name) . "%")
                        ->where("source","main")
                        ->count();
            if($articles > 0) {
                $articlesOne = DB::connection('mysql3')->table('std_article')
                                    ->whereNull("inactive")
                                    ->where("author", "like", "%" . addslashes($name) . "%")
                                    ->where("source", "main")
                                    ->count();
                if($articlesOne > 0) {
                    $articlesTwo = DB::connection('mysql2')->table('std_article')
                                        ->whereNull("inactive")
                                        ->where("author", "like", "%" . addslashes($name) . "%")
                                        ->where("source","main")
                                        ->count();
                    if($articlesTwo > 0) {
                        return $articles + $articlesOne + $articlesTwo;
                    }else {
                        return $articles + $articlesOne;
                    }
                }else{
                    return $articles;
                }
            }else {
                return 0;
            }
    }


    public function getSlideShows($limit=8){
        $slides =  DB::table("std_slideshow")
                        ->where("source","main")
                        ->whereNull("inactive")
                        ->orderBy("publishdate","DESC")
                        ->limit($limit)
                        ->get();
        if ($slides){
            return $slides;
        }

    }

    public function getSlideShowTitle($id){
        $id = (int) $id;
        return DB::table("std_slideshow")
                    ->where("id", $id)
                    ->get()
                    ->first()->title;
    }
    public function getSlideShowPictures($id){
        $id = (int) $id;
        $pictures = DB::table("std_slideshow_picture")
                            ->where("slideshowid",$id)
                            ->whereNull("inactive")
                            ->orderBy("listorder","ASC")
                            ->get();
        if ($pictures){
            return $pictures;
        }
    }

    public function getTopic($name, $limit, $start = 0){
        $topic 	= str_replace("-", " ", $name);
        $articles	= DB::table("std_article")
                        ->select("std_article.id", "std_article.title", "std_article.long_title", "std_article.thumbURL", "std_article.categoryid", "std_article.source", "std_article.publishdate", "std_article.author", "std_article.summary")
                        ->whereNull("inactive")
                        ->where("keywords", "like", "%" .$topic . "%")
                        ->where("source","main")
                        ->orderBy("publishdate","DESC")
                        ->limit($limit)
                        ->offset($start)
                        ->get();

        if ($articles){
            return $articles;
        }
    }

    public function breakingNews(){
        $now = $this->now;
        $from = date("Y-m-d H:i", strtotime("-30 minutes", strtotime($now)));
        $article = DB::table("std_article")
                        ->select("std_article.id", "std_article.title", "std_article.long_title", "std_article.thumbURL", "std_article.categoryid", "std_article.source", "std_article.publishdate", "std_article.author", "std_article.summary")
                        ->whereNull("std_article.inactive")
                        ->where("std_article.source","main")
                        ->where("std_article.publishdate", ">=", $from)
                        ->where("std_article.breaking", 1)
                        ->orderBy("std_article.id","DESC")
                        ->limit(1)
                        ->first();

        if ($article){
            return $article;
        }
    }
    public function getLiveLink(){
        $live = DB::table("ktn_livestream_settings")
                    ->where("id",1)
                    ->first();
        if ($live){
            return $live;
        }
    }

    public function getReadMoreArticles($id, $catId, $start, $limit){
        $id = (int)$id;
        $catId = (int)$catId;
        $articles = DB::table("std_article")
                        ->select("std_article.id", "std_article.title", "std_article.long_title", "std_article.thumbURL", "std_article.categoryid", "std_article.source", "std_article.publishdate", "std_article.author", "std_article.summary")
                        ->whereNull("inactive")
                        ->whereNotIn('std_article.id', [$id])
                        ->whereNotNull("std_article.homepagelistorder")
                        ->where("std_article.source","main")
                        ->where("std_article.categoryid","=", $catId)
                        ->orderBy("std_article.publishdate","DESC")
                        ->limit($limit)
                        ->offset($start)
                        ->get()
                        ->toArray();
        $rows = count($articles);
        if ($articles) {
            if ($rows < $limit) {
                $newLimit = $limit - $rows;
                $articlesOne = DB::connection('mysql3')->table('std_article')
                                    ->select("std_article.id", "std_article.title", "std_article.long_title", "std_article.thumbURL", "std_article.categoryid", "std_article.source", "std_article.publishdate", "std_article.author", "std_article.summary")
                                    ->whereNull("inactive")
                                    ->whereNotIn('std_article.id', [$id])
                                    ->whereNotNull("std_article.homepagelistorder")
                                    ->where("std_article.source", "main")
                                    ->where("std_article.categoryid", "=", $catId)
                                    ->orderBy("std_article.publishdate", "DESC")
                                    ->limit($newLimit)
                                    ->offset($start)
                                    ->get()
                                    ->toArray();

                $rowsCount = count($articlesOne);
                if ($articlesOne) {
                    $newestLimit = $limit - $rows;
                    if ($rowsCount < $newestLimit) {
                        $articlesTwo = DB::connection('mysql3')->table('std_article')
                                            ->select("std_article.id", "std_article.title", "std_article.long_title", "std_article.thumbURL", "std_article.categoryid", "std_article.source", "std_article.publishdate", "std_article.author", "std_article.summary")
                                            ->whereNull("inactive")
                                            ->whereNotIn('std_article.id', [$id])
                                            ->whereNotNull("std_article.homepagelistorder")
                                            ->where("std_article.source", "main")
                                            ->where("std_article.categoryid", "=", $catId)
                                            ->orderBy("std_article.publishdate", "DESC")
                                            ->limit($newestLimit)
                                            ->offset($start)
                                            ->get()
                                            ->toArray();

                        if ($articlesTwo){
                            return array_merge($articles, $articlesOne, $articlesTwo);
                        }

                    }else {
                        return array_merge($articles, $articlesOne);
                    }
                }
            }else{
                return $articles;
            }
        }
    }

    public function getRecommendedArticles($keywords, $id, $limit = 5, $start = 0) {

        if(($keywords !== NULL) or ($keywords !== "")) {
            $keywords	=   str_replace(":", ";", $keywords);
            $keywords	=   str_replace(",", ";", $keywords);
            $keywords	= 	explode(";",$keywords);

            if(is_array($keywords)) {
                $first		  =	array_shift($keywords);
                $articles     = DB::table("std_article")
                                    ->select("std_article.id", "std_article.title", "std_article.long_title", "std_article.thumbURL", "std_article.categoryid", "std_article.source", "std_article.publishdate", "std_article.author", "std_article.summary")
                                    ->whereNull("inactive")
                                    ->where("source", "main")
                                    ->whereNotIn("id", [$id])
                                    ->where("keywords", "like", "%" .$first. "%");


                if(is_array($keywords)) {
                    foreach ($keywords as $value) {
                        $articles->orWhere("keywords", "like", "%" . $value . "%");
                    }
                }
                $articles 	= 	 $articles->orderBy("std_article.publishdate","DESC")
                                          ->limit($limit)
                                          ->offset($start)
                                          ->get();

                if($articles) {
                    if(count($articles) > 0) {
                        return $articles;
                    }
                }
            }
        }

    }
}
