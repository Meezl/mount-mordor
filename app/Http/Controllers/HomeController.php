<?php

namespace App\Http\Controllers;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Cache;

class HomeController extends BaseController {

    public function index(){

        $roadBlockTime = $this->roadBlockTime();
        $live = $this->home->getLiveLink();
        $page = "home";
        $logo = asset('assets/images/sglogo.png');
        $utils = $this;
        $topStories  = $this->home->getHomeLatest(3,0);
        $topStoriesTwo = $this->home->getHomeLatest(2,3);
        $insider =  $this->home->getCategoryTop(486,4, 0);
        $breaking = $this->home->breakingNews();
        $popularNews =  Cache::remember("trending.articles", now()->addSeconds(1800), function () {
            return $this->getTrendingArticles(5, "today");
        });
        $economy    = $this->home->getCategoryTop(4,4);
        $world      = $this->home->getCategoryTop(5,6);
        $kenya      = $this->home->getCategoryTop(1,4);
        $entertainment =  $this->home->getOtherSites("entertainment",4);
        $politics   = $this->home->getCategoryTop(3,4);
        $sports     = $this->home->getCategoryTop(6,4);
        $latest     = $this->home->getLatest(4,0, $this->filterArticles($topStories, "id"));
        $latestVideos =  $this->home->getLatestVideos(5);

        $checkpoint =  $this->home->getCategoryTop(480,4);

        $eveWoman =  $this->home->getOtherSites("evewoman",6);

        $slideShows =  $this->home->getSlideShows(12);

        return view('pages.home', compact('topStories', 'entertainment', 'politics', 'economy' , 'world', 'page',
            'insider', 'latest', 'latestVideos', 'live', 'breaking', 'utils', 'kenya', 'sports', 'topStoriesTwo', 'checkpoint', 'roadBlockTime', 'slideShows', 'eveWoman', 'popularNews', 'logo'));

    }

    public function category($id, $name = null){

        $roadBlockTime = $this->roadBlockTime();
        $page = "category";
        $breaking = $this->home->breakingNews();
        $logo = asset('assets/images/sglogo.png');
        $utils = $this;
        $secondList     = $this->home->getCategoryTop($id,4);
        $lastList     = $this->home->getCategoryTop($id,8,5);
        $insider =  $this->home->getCategoryTop(486,4, 0);
        $categoryName = $this->home->getCategoryName($id);
        $allList     = $this->home->getCategoryTop($id,30,14);
        $checkpoint =  $this->home->getCategoryTop(480,5);

        $slideShows =  $this->home->getSlideShows(12);
        $mostRead = Cache::remember("trending.articles", now()->addSeconds(1800), function ()  {
            return $this->getTrendingArticles(5, "today");
        });
        $catId = $id;
        $parentId = $utils->home->getCatId($id)->pid;
        return view('pages.category', compact('insider',  'breaking' ,'slideShows', 'logo',  'allList', 'roadBlockTime', 'utils', 'secondList' , 'lastList', 'checkpoint', 'id', 'parentId', 'categoryName', 'mostRead' , 'page', 'catId'));

    }

    public function article($id, $title = null){

        $roadBlockTime = $this->roadBlockTime();
        $utils = $this;
        $breaking = $this->home->breakingNews();
        $logo = asset('assets/images/sglogo.png');
        $page = "article";
        $article = $this->home->getArticle($id);
        $topics = explode(";",$article->keywords);
        $keywords =	str_replace(";",",", $article->keywords);
        $description	=	$article->summary;
        $image	=	$utils->imageLocation($article->thumbURL);
        $author      =   $article->author;
        $title	=	$article->title;
        $story = $this->assist->removeEmptyParagraphs($article->story);
        $story = explode("<p>", $story);
        $story = str_replace("/images/", "https://cdn.standardmedia.co.ke/images/", $story);
        $relatedArticles = $this->home->getRecommendedArticles($article->keywords, $id, 5, 1);
        $recommended = $this->home->getRecommendedArticles($article->keywords, $id, 5, 5);
        $topStories =  $this->home->getHomeLatest(5, 0);
        $hashTags = explode(';', trim($article->keywords));
        $mostRead = Cache::remember("trending.articles", now()->addSeconds(1800), function () {
            return $this->getTrendingArticles(5, "today");
        });
        $insider =  $this->home->getCategoryTop(486,4, 0);
        $checkpoint =  $this->home->getCategoryTop(480,4);
        $hashTag = "StandardDigital";
        if (isset($hashTagd)) {
            $hashTag = str_replace(' ', '', $hashTags[0]);
        }
        $catId = $article->categoryid;
        $moreNews = $this->home->getReadMoreArticles($id, $catId,0, 4);
        $parentId = $utils->home->getCatId($catId)->pid;
        $actual_link = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $redirectUrl = "subscription/sign-in?returnurl=" . $actual_link;
        if ($catId == 486) {
            if (Session::has('user')) {
                $user = Session::get("user");
                if ($user == null) {
                    return redirect($redirectUrl);
                } else {
                    return view('pages.article', compact('article', 'checkpoint', 'utils', 'story', 'moreNews', 'topStories', 'hashTag', 'topics', 'parentId', 'catId', 'mostRead',
                        'keywords', 'author', 'title', 'image', 'relatedArticles', 'insider', 'recommended', 'description', 'page', 'breaking', 'logo', 'roadBlockTime', 'parentId'));
                }
            }else {
                return redirect($redirectUrl);
            }
        } else {
            return view('pages.article', compact('article', 'checkpoint', 'utils', 'story', 'moreNews', 'topStories', 'hashTag', 'topics', 'parentId', 'catId', 'mostRead',
                'keywords', 'author', 'title', 'image', 'recommended', 'insider', 'description', 'relatedArticles', 'breaking', 'page', 'logo', 'roadBlockTime', 'parentId'));
        }
    }

    public function author($name){

        $roadBlockTime = $this->roadBlockTime();
        $breaking = $this->home->breakingNews();
        $page = "author";
        $logo = asset('assets/images/sglogo.png');
        $utils          = $this;
        $topStories = $this->home->getHomeLatest(3,0);
        $topStoriesTwo = $this->home->getHomeLatest(5,3);
        $latest =  $this->home->getLatest(4,0, $this->filterArticles($topStories, "id"));

        $mostRead =  Cache::remember("trending.articles", now()->addSeconds(1800), function () {
            return $this->getTrendingArticles(5, "today");
        });
        $firstList =  $this->home->getAuthorContent($name,3);

        $secondList =  $this->home->getAuthorContent($name,2,3);

        $lastList =  $this->home->getAuthorContent($name,12,5);

        $insider =  $this->home->getCategoryTop(486,5, 0);

        $author         = ucwords(str_replace("-", " ", $name));
        $checkpoint =  $this->home->getCategoryTop(480,7);

        $latestOne = $this->home->getLatest(7,0, $this->filterArticles($topStoriesTwo, "id"));

        return view('pages.author', compact('utils', 'checkpoint', 'firstList', 'secondList', 'lastList', 'author', 'latest', 'insider',
            'latestOne', 'author', 'page', 'roadBlockTime', 'breaking', 'logo', 'mostRead'));

    }

    public function categoryLoadMore(Request $request){

        $row = $request->row;
        $html = '';
        $id = $request->id;
        $categories = $this->home->getCategory($id, 6, $row);
        foreach ($categories as $articles) {
            foreach ($articles as $value) {
                $html .= '<div class="card mb-3" id="post_' . $value->id . '">
                                <div class="row no-gutters">
                                    <div class="col-md-6">
                                        <div class="single-standard">
                                            <div class="standard-image">
                                                <a href="' . $this->assetLink("article", $value->id, $value->title) . '">';
                $html .= '<img src="' . $this->imageLocation($value->thumbURL) . '" class="card-img-top" alt="' . $value->title . '">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="card-body">
                                            <a href="' . $this->assetLink("category", $value->categoryid, $this->home->getCategoryName($value->categoryid)) . '">
                                                <p class="bigred">' . $this->home->getCategoryName($value->categoryid) . '</p>
                                            </a>
                                            <a href="' . $this->assetLink("article", $value->id, $value->title) . '">
                                                <h5 class="card-title boldblack">' . $value->title . '</h5>
                                                <p class="card-text hortext">' . $value->summary . '</p>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>';
            }
            echo $html;
        }
    }

    public function search(){

        $breaking = $this->home->breakingNews();
        $roadBlockTime = $this->roadBlockTime();
        $page =  "search";
        $logo = asset('assets/images/sglogo.png');
        $utils          = $this;
        $topStories =  $this->home->getHomeLatest(15,0);

        $insider =  $this->home->getCategoryTop(486,4, 0);

        return view('pages.search', compact('topStories', 'breaking', 'utils', 'insider', 'page', 'logo', 'roadBlockTime'));
    }

    public function topic($name){

        $breaking = $this->home->breakingNews();
        $roadBlockTime = $this->roadBlockTime();
        $page = "topic";
        $logo = asset('assets/images/sglogo.png');
        $utils          = $this;
        $topStories =  $this->home->getHomeLatest(14,0);
        $articles = $this->home->getTopic($name, 15);
        $topic = str_replace("-", " ", $name);
        $insider =  $this->home->getCategoryTop(486,4, 0);
        return view('pages.topic', compact('topStories','breaking', 'roadBlockTime', 'logo', 'utils', 'articles', 'topic', 'insider' , 'page'));
    }

    public function subscribeUser(Request $request){
        $validatedData = $request->validate([
            'email' => 'required|max:255|email',
        ]);
        $params = [
            "email"=> $validatedData["email"],
            "category_id" => 17,
        ];
        $url = 'https://mail.standarddigitalworld.co.ke/api/subscription';
        $client = new Client(['headers' => [ 'Content-Type' => 'application/x-www-form-urlencoded', "appkey" => "VHV1VElESjRMNklTYnNubGJlZVY0WkRieVNiZU9vendsSm5mRU56NDdYR2w2WXBESEF2UnVQWU9MdXhm5eaa84f9ba4bc" ],'verify'=> true,'http_errors'=>false]);
        try {
            $result = $client->request('POST', $url, [ 'form_params' => $params]);
            $response = $result->getBody();
            if (isset($response)){
                $response = json_decode($response);
                return redirect('/')->with("success", $response->message);
            }
        }catch (ClientException $e){
            return redirect('/')->with("error", "Ooops!, something went wrong, We're working on it");
        }
    }

    public function policy(){
        $roadBlockTime = $this->roadBlockTime();
        $page = "policy";
        $logo = asset('assets/images/sglogo.png');
        return view('pages.policy', compact('roadBlockTime', 'logo', 'page'));
    }

    public function terms(){
        $roadBlockTime = $this->roadBlockTime();
        $page = "terms";
        $logo = asset('assets/images/sglogo.png');
        return view('pages.terms', compact('roadBlockTime', 'logo', 'page'));
    }

    public function rss(){
        $roadBlockTime = $this->roadBlockTime();
        $page = "rss";
        $logo = asset('assets/images/sglogo.png');
        $mostRead =  Cache::remember("trending.articles", now()->addSeconds(1800), function () {
            return $this->getTrendingArticles(5, "today");
        });
        return view('pages.rss', compact('roadBlockTime', 'logo', 'page', 'mostRead'));
    }
}
