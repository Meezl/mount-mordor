<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Auth;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    public function index(){
        return view('auth.passwords.reset');
    }

    public function resetPassword(Request $request){

        $url = "https://vas.standardmedia.co.ke/api/email/password";

        $client = new Client(['headers' => [ 'Content-Type' => 'application/json' ], 'verify'=> false, 'http_errors'=>false]);
        try {
            $response = $client->request('POST', $url, [
                'form_params' => [
                    'email' => $request->email,
                    'redirect_url' => $request->redirecturl,
                ]]);
            $result = $response->getBody();
            if (isset($result)){
                $redirectUrl = $request->redirecturl;

                if ($redirectUrl == ""){
                    return redirect('/')->with("success", "Welcome, thanks for subscribing!");
                }else {
                    return redirect($redirectUrl)->with("success", "Welcome, thanks for subscribing!");
                }
            }
        } catch (ClientException $e) {
            return back()->with('error','Your Email is invalid....Please try again!');
        } catch (ServerException $e) {
            return back()->with('error','An error has occurred....Please try again!');
        }
    }
}
