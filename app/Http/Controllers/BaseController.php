<?php

namespace App\Http\Controllers;

use App\Libraries\Utils;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Client;

class BaseController extends Controller
{
    public $assist;
    public $home;
    public function __construct(){
        $this->assist = new Utils();
        $this->home = (new \App\Models\Home);
        $this->setCookie('roadBlock','roadBlock', 1440);
    }

    public function imageLocation($thumburl, $site = 'main')
    {
        switch($site)
        {
            case 'default':
            case 'sde':
            case 'main':
                return "https://cdn.standardmedia.co.ke".$thumburl;
            case 'ureport':
                return 'https://www.standardmedia.co.ke/ureport-uploads/'.$thumburl;
            case 'youtube':
                return 'https://i.ytimg.com/vi/'.$thumburl.'/mqdefault.jpg';
        }
    }

    public function articleRedirect($source,$id,$title)
    {
        switch($source)
        {
            case "evewoman":
                redirect("https://www.standardmedia.co.ke/evewoman/article/".$id."/".$this->assist->slugify($title),"refresh");
                break;
            case "ureport":
                redirect("https://www.standardmedia.co.ke/ureport/article/".$id."/".$this->assist->slugify($title),"refresh");
                break;
            case "entertainment":
                redirect("https://www.sde.co.ke/article/".$id."/".$this->assist->slugify($title),"refresh");
                break;
            case "farmers":
                redirect("https://www.farmers.co.ke/article/".$id."/".$this->assist->slugify($title),"refresh");
                break;
            case "sports":
                redirect("https://www.standardmedia.co.ke/sports/article/".$id."/".$this->assist->slugify($title),"refresh");
                break;
        }
    }
    public function filterArticles($array, $key)
    {
        foreach($array as $value)
        {
            $x[] = $value->$key;
        }
        return $x;
    }

    public function assetLink($type, $id, $title = NULL, $cat = NULL)  {
        if(($type=='author')||($type=='topic')|| ($type=='pictures'))
        {
            $x=($title!=NULL)?"/".$this->assist->slugify($title):$title;
            return url($type."/".$this->assist->slugify($id).$x);
        }
        else
        {
            if($cat === "ureport")
            {
                return "https://www.standardmedia.co.ke/ureport/".$type."/".$id."/".$this->assist->slugify($title);
            }
            elseif($cat === "evewoman")
            {
                return "https://www.standardmedia.co.ke/evewoman/".$type."/".$id."/".$this->assist->slugify($title);
            }
            elseif($cat === "farmers")
            {
                return "https://www.farmers.co.ke/".$type."/".$id."/".$this->assist->slugify($title);
            }
            elseif($cat === "sde")
            {
                return "https://www.sde.co.ke/".$type."/".$id."/".$this->assist->slugify($title);
            }
            elseif($cat === "obituaries")
            {
                return "https://www.standardmedia.co.ke/obituaries/".$type."/".$id."/".$this->assist->slugify($title);
            }
            else
            {

                if($type == 'article')
                {
                    $check  =	$this->home->checkSource($id);

                    if((@$check->pid == '6') or (@$check->id == '6'))
                    {
                        return url("sports/".$type."/".$id."/".$this->assist->slugify($title));
                    }
                    elseif((@$check->pid == '4') or (@$check->id == '4'))
                    {
                        return url("business/".$type."/".$id."/".$this->assist->slugify($title));
                    }
                    elseif((@$check->pid == '41') or (@$check->id == '41'))
                    {
                        return url("health/".$type."/".$id."/".$this->assist->slugify($title));
                    }
                    elseif((@$check->pid == '9') or (@$check->id == '9'))
                    {
                        return url("lifestyle/".$type."/".$id."/".$this->assist->slugify($title));
                    }
                    elseif((@$check->pid == '486') or (@$check->id == '486'))
                    {
                        return url("the-standard-insider/".$type."/".$id."/".$this->assist->slugify($title));
                    }
                    else
                    {
                        return url($type."/".$id."/".$this->assist->slugify($title));
                    }
                }
                elseif($type == 'category')
                {
                    $check = $this->home->getCatId($id);

                    if((@$check->pid == '6') or ($id == '6'))
                    {
                        return url("sports/".$type."/".$id."/".$this->assist->slugify($title));
                    }
                    elseif((@$check->pid == '4') or ($id == '4'))
                    {
                        return url("business/".$type."/".$id."/".$this->assist->slugify($title));
                    }
                    elseif((@$check->pid == '41') or ($id == '41'))
                    {
                        return url("health/".$type."/".$id."/".$this->assist->slugify($title));
                    }
                    elseif((@$check->pid == '9') or ($id == '9'))
                    {
                        return url("lifestyle/".$type."/".$id."/".$this->assist->slugify($title));
                    }
                    else
                    {
                        return url($type."/".$id."/".$this->assist->slugify($title));
                    }
                }
                return url($type."/".$id."/".$this->assist->slugify($title));
            }
        }
    }
    public function videoLink($site,$type,$id,$title){
        switch($site)
        {
            case "newsvideos":
                if($type=="watch")
                {
                    return "https://www.standardmedia.co.ke/ktnnews/video/".$id."/".$this->assist->slugify($title);
                }
                break;
            case "sdv":
                if($type=="watch")
                {
                    return "https://www.standardmedia.co.ke/videos/view/".$id."/".$this->assist->slugify($title);
                }
                break;
            case "farmers":
                if($type=="watch")
                {
                    return "https://www.farmers.co.ke/video/".$id."/".$this->assist->slugify($title);
                }
                break;
        }
    }

    public function getTrendingArticles($perPage, $period){
        $params = [
            'key' => 'ae48488c6ef1ea95354d3ffb5c496ca8',
            'entities' => [
                'articles' => [
                    'entity' => 'articles',
                    'details' => ["author", "category"],
                    "filters" => [
                        "category" => "Newsdesk"
                    ]
                ]
            ],
            'options' => [
                'period' => [
                    'name' => $period
                ],
                'per_page' => $perPage
            ]
        ];
        $url = 'https://api.onthe.io/ata6CLk8UhmPPvS3PfZYx5wKAloQz24K';

        $client = new Client(['headers' => [ 'Content-Type' => 'application/x-www-form-urlencoded' ],'verify'=> true,'http_errors'=>false]);

        try {
            $result = $client->request('POST', $url, [ 'form_params' => $params]);
            $response = $result->getBody();
            $response = json_decode($response);
            return $response->articles->list;

        }catch (ClientException $e){
            echo "Ooopsy, you just hit a wall, my bad!";
        }

    }

    public function setCookie($name, $value, $duration){
        $response  = new Response('Set Cookie');
        $response->withCookie(cookie($name, $value, $duration));
        return $response;
    }

    public	function roadBlockTime(){
        if(strtotime(date('Y-m-d H:i:s'))>=strtotime('2020-02-19 00:59:59') && strtotime(date('Y-m-d H:i:s'))<=strtotime('2020-02-19 23:59:59')){
            return true;
        }
    }


}
