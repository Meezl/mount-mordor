<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Route::get('/' ,'HomeController@index');
Route::get('/article/{id}/{title}' ,'HomeController@article');
Route::get('/the-standard-insider/article/{id}/{title}' ,'HomeController@article');
Route::get('/business/article/{id}/{title}' ,'HomeController@article');
Route::get('/health/article/{id}/{title}' ,'HomeController@article');
Route::get('/sports/article/{id}/{title}' ,'HomeController@article');
Route::get('/lifestyle/article/{id}/{title}' ,'HomeController@article');
Route::get('/category/{id}/{name}' ,'HomeController@category');
Route::get('/business/category/{id}/{name}' ,'HomeController@category');
Route::get('/health/category/{id}/{name}' ,'HomeController@category');
Route::get('/sports/category/{id}/{name}' ,'HomeController@category');
Route::get('/author/{name}' ,'HomeController@author');
Route::get('/lifestyle/category/{id}/{name}' ,'HomeController@category');
Route::post('/category-load-more', 'HomeController@categoryLoadMore');
Route::get('/subscription/sign-in' ,'Auth\LoginController@index');
Route::get('/subscription/sign-up' ,'Auth\RegisterController@index');
Route::get('/search' ,'HomeController@search');
Route::get('trending','HomeController@trending');
Route::get('/topic/{name}','HomeController@topic');
Route::post('/subscribe/email', 'HomeController@subscribeUser');
Route::get('privacy-policy','HomeController@policy');
Route::get('terms-and-conditions','HomeController@terms');
Route::get('/ads.txt',function(){return view('includes.ads');});
Route::get('/rssfeeds','HomeController@rss');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/register', 'Auth\RegisterController@registration');
Route::post('/profile/{id}', 'HomeController@profile')->name('profile');
Route::post('/reset-password', 'Auth\ResetPasswordController@resetPassword')->name('reset-password');
Route::get('/subscription/reset-password', 'Auth\ResetPasswordController@index');

