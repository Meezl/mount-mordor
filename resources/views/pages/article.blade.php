@extends('layout.structure')
@section('content')
<script>
    window._io_config = window._io_config || {};
    window._io_config["0.2.0"] = window._io_config["0.2.0"] || [];
    window._io_config["0.2.0"].push({
        page_url: "{{ url()->current() }}",
        page_url_canonical: "{{ url()->current() }}",
        page_title: "{{ $article->title }}",
        page_type: "article",
        page_language: "en",
        article_authors: ["{{ $article->author }}"],
        article_categories: ["Newsdesk"],
        article_subcategories: ["{{ $utils->home->getCategoryName($article->categoryid)  }}"],
        article_type: "longread",
        article_word_count: "{{ str_word_count($article->story) }}",
        article_publication_date: "{{ date('D, d M Y H:i:s T', strtotime($article->publishdate)) }} ?>"
    });
</script>
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "NewsArticle",
  "mainEntityOfPage": "{{ url()->current() }}",
  "headline": "{{ $article->title  }}",
  "image":{
      "@type": "ImageObject",
      "url": "{{ $utils->imageLocation($article->thumbURL) }}",
      "height": 500,
      "width": 800
    },
  "datePublished": "{{ date('D, d M Y H:i:s T',strtotime( $article->publishdate)) }}",
  "dateModified": "{{ date('D, d M Y H:i:s T',strtotime( $article->updateddate)) }} ",
  "author": {
    "@type": "Person",
    "name": "{{ $article->author}}"
  },
   "publisher": {
    "@type": "Organization",
    "name": "Standard Digital",
    "logo": {
      "@type": "ImageObject",
      "url": "{{ asset('assets/images/logo.png') }} ",
      "width": 600,
      "height": 60
    }
  },
  "description": "{{ $description ?? "Kenya's number one website that delivers realtime news across the globe. The top headlines covers politics, citizen journalism, culture, business, sports and entertainment. Standard Digital - Your Gateway" }}"
}
</script>
    <script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [{
    "@type": "ListItem",
    "position": 1,
    "name": "Home",
    "item": "{{ url('/') }}"
  },{
    "@type": "ListItem",
    "position": 2,
    "name": "{{ $utils->home->getCategoryName($article->categoryid) }}",
    "item": "{{ $utils->assetLink("category", $article->categoryid, $utils->home->getCategoryName($article->categoryid)) }}"
  },{
    "@type": "ListItem",
    "position": 3,
    "name": "{{ $article->long_title }}",
    "item": "{{ url()->current() }}"
  }]
}
</script>
<!-------------------------------- Start Article Section---------------------------------------->
    <section class="standard-area pt-100">
        <div class="container mx-auto w-100">
            <div class="offset-md-4 row my-2">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                @if ($message = Session::get('error'))
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
            </div>
            <div class="row mt-2">
                @if((new Jenssegers\Agent\Agent())->isDesktop())
                    @if($parentId == 4 || $catId == 4)
                        <div class="ad-728-90 offset-md-2 col-md-8 mb-1">
                            @include('Ads.desktop-ads.business_main_top_inner_advert1')
                        </div>
                    @elseif($parentId == 5 || $catId == 5)
                        <div class="ad-728-90 offset-md-2 col-md-8 mb-1">
                            @include('Ads.desktop-ads.world_top_advert')
                        </div>
                    @elseif($parentId == 41 || $catId == 41)
                        <div class="ad-728-90 offset-md-2 col-md-8 mb-1">
                            @include('Ads.desktop-ads.health_top_advert')
                        </div>
                    @elseif($parentId == 9 || $catId == 9)
                        <div class="ad-728-90 offset-md-2 col-md-8 mb-1">
                            @include('Ads.desktop-ads.lifestyle_main_top_inner_advert1')
                        </div>
                    @else
                        <div class="ad-728-90 offset-md-2 col-md-8 mb-1">
                            @include('Ads.desktop-ads.home_main_top_inner_advert1')
                        </div>
                    @endif
                @endif
            </div>
            <!-------------------------------- Breaking news ---------------------------------------->
            @if(isset($breaking) && ($breaking != null))
                @include('includes.breakingNews')
            @endif
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <nav>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{ url('/') }}">
                                    Home
                                </a>
                            </li>
                            @if($utils->home->getCatId($article->categoryid)->pid != 0)
                                <li class="breadcrumb-item">
                                    <a href="{{ $utils->assetLink("category", $article->categoryid, $utils->home->getCategoryName($utils->home->getCatId($article->categoryid)->pid)) }}">
                                        {{ $utils->home->getCategoryName($utils->home->getCatId($article->categoryid)->pid) }}
                                    </a>
                                </li>
                            @endif
                            <li class="breadcrumb-item">
                                <a href="{{ $utils->assetLink("category", $article->categoryid, $utils->home->getCategoryName($article->categoryid)) }}">
                                    {{ $utils->home->getCategoryName($article->categoryid) }}
                                </a>
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="row justify-content-center mt-1">
                <div class="col-md-8 col-sm-12 mx-2 mx-md-0">
                    @if((new Jenssegers\Agent\Agent())->isMobile())
                        <div class="ad-320-100 justify-content-center mb-1">
                            @if($parentId == 1 || $catId == 1)
                                @include('Ads.desktop-ads.home_main_top_inner_advert1')
                            @elseif($parentId == 5 || $catId == 5)
                                @include('Ads.desktop-ads.world_top_advert')
                            @elseif($parentId == 4 || $catId == 4)
                                @include('Ads.desktop-ads.business_main_top_inner_advert1')
                            @elseif($parentId == 9 || $catId == 9)
                                @include('Ads.desktop-ads.lifestyle_main_top_inner_advert1')
                            @elseif($parentId == 41 || $catId == 41)
                                @include('Ads.desktop-ads.health_article_top_advert')
                            @else
                                @include('Ads.desktop-ads.home_main_top_inner_advert1')
                            @endif
                        </div>
                    @endif
                        <div class="single-standard mainimg story  px-md-3 pb-5">
                            <div class="single-standard">
                                <h3 class="heading">
                                    <a href="#" class="articleheading">
                                        @if($article->long_title != "")
                                             {{ $article->long_title }}
                                        @else
                                             {{ $article->title }}
                                        @endif
                                    </a>
                                </h3>
                                <div class="icon-bar">
                                    <p class="greysmall mt-2 mt-md-2 "> <span class="categoryname mb-1 pl-2"> By <a href="{{ $utils->assetLink("author", $article->author) }}">{{ $article->author }}</a> |</span>
                                         {{ date('F jS Y \a\t h:i:s \G\M\T  O', strtotime($article->publishdate)) }}
                                    </p>
                                </div>
                                <div class="d-flex justify-content-between my-1">
                                    <div>
                                        <div class="socialmedia-buttons btn-group">
                                            <a href="https://www.facebook.com/sharer.php?u={{ url()->current() }}" class="btn btn-facebook rounded-0 text-white">
                                                <span class="fa fa-facebook"></span>
                                            </a>
                                            <a href="https://twitter.com/share?url={{ url()->current() }}&hashtags={{ $hashTag }}&text={{ urlencode($article->title) }}" class="btn btn-twitter rounded-0 text-white">
                                                <span class="fa fa-twitter texu-white"></span>
                                            </a>
                                            <a href="https://telegram.me/share/url?url={{ url()->current() }}&text={{  $article->title }}" class="btn btn-telegram rounded-0 text-white">
                                                <span class="fa fa-telegram"></span>
                                            </a>
                                            <a href="https://web.whatsapp.com/send?text={{ url()->current() }}" class="btn btn-whatsapp rounded-0 text-white" >
                                                <span class="fa fa-whatsapp"></span>
                                            </a>
                                            <a href="" class="btn btn-linkedin rounded-0 text-white">
                                                <span  class="fa fa-linkedin"></span>
                                            </a>
                                            <a href="" class="btn btn-email rounded-0 text-white">
                                                <span  class="fa fa-envelope"></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="standard-content ">
                                <div class="content ">
                                    @php $i = 1; $adCount = 0; $art = 0; @endphp
                                    @foreach($story as $value)
                                        {!! $value !!}
                                        @if($i == 1)
                                            <p class="darktext pb-0 pl-2 mb-2">
                                                <font class="categoryname mb-4">{{ $utils->home->getCategoryName($article->categoryid) }}</font><br>
                                                {{ $article->summary }}
                                            </p>
                                        @endif
                                        @if($i == 2)
                                            @php $adCount += 1; @endphp
                                            @if((new Jenssegers\Agent\Agent())->isDesktop())
                                                @if($parentId == 4 || $catId == 4)
                                                   <div class="ad-article-page-desktop my-2">
                                                       @include('Ads.desktop-ads.business_article_top_advert')
                                                   </div>
                                                @elseif($parentId == 1 || $catId == 1)
                                                    <div class="ad-article-page-desktop my-2">
                                                        @include('Ads.desktop-ads.home_main_inner_middle_advert1')
                                                    </div>
                                                @elseif($parentId == 41 || $catId == 41)
                                                    <div class="ad-article-page-desktop my-2">
                                                        @include('Ads.desktop-ads.health_article_top_advert')
                                                    </div>
                                                @else
                                                    <div class="ad-article-page-desktop my-2">
                                                        @include('Ads.desktop-ads.home_main_inner_middle_advert1')
                                                    </div>
                                                @endif
                                            @elseif((new \Jenssegers\Agent\Agent())->isMobile())
                                                @if($parentId == 4 || $catId == 4)
                                                    <div class="ad-300-250-article justify-content-center my-2">
                                                        @include('Ads.desktop-ads.business_main_rightpanel_advert1')
                                                    </div>
                                                @elseif($parentId == 9 || $catId == 9)
                                                    <div class="ad-300-250-article justify-content-center my-2">
                                                        @include('Ads.desktop-ads.lifestyle_main_rightpanel_advert1')
                                                    </div>
                                                @elseif($parentId == 1 || $catId == 1)
                                                    <div class="ad-300-250-article justify-content-center my-2">
                                                        @include('Ads.mobile-ads.mobile_inner_article_advert1')
                                                    </div>
                                                @else
                                                    <div class="ad-300-250-article justify-content-center my-2">
                                                        @include('Ads.mobile-ads.mobile_inner_article_advert1')
                                                    </div>
                                                @endif
                                            @endif
                                        @endif
                                        @if($i == 6)
                                            @php $adCount += 1; @endphp
                                            @if((new \Jenssegers\Agent\Agent())->isMobile())
                                                @if($parentId == 4 || $catId == 4)
                                                    <div class="ad-300-250-article justify-content-center my-2">
                                                        @include('Ads.desktop-ads.business_main_rightpanel_advert2')
                                                    </div>
                                                @elseif($parentId == 9 || $catId == 9)
                                                    <div class="ad-300-250-article justify-content-center my-2">
                                                        @include('Ads.desktop-ads.lifestyle_main_rightpanel_advert2')
                                                    </div>
                                                @elseif($parentId == 1 || $catId == 1)
                                                    <div class="ad-300-250-article justify-content-center my-2">
                                                        @include('Ads.mobile-ads.mobile_inner_article_advert2')
                                                    </div>
                                                @else
                                                    <div class="ad-300-250-article justify-content-center my-2">
                                                        @include('Ads.mobile-ads.mobile_inner_article_advert2')
                                                    </div>
                                                @endif
                                            @endif
                                        @endif
                                        @if($i == 12)
                                            @php $adCount += 1; @endphp
                                            @if((new \Jenssegers\Agent\Agent())->isMobile())
                                                @if($parentId == 4 || $catId == 4)
                                                    <div class="ad-300-250-article justify-content-center my-2">
                                                        @include('Ads.desktop-ads.business_main_rightpanel_advert3')
                                                    </div>
                                                @elseif($parentId == 9 || $catId == 9)
                                                    <div class="ad-300-250-article justify-content-center my-2">
                                                        @include('Ads.desktop-ads.lifestyle_main_rightpanel_advert3')
                                                    </div>
                                                @elseif($parentId == 1 || $catId == 1)
                                                    <div class="ad-300-250-article justify-content-center my-2">
                                                        @include('Ads.mobile-ads.mobile_inner_article_advert3')
                                                    </div>
                                                @else
                                                    <div class="ad-300-250-article justify-content-center my-2">
                                                        @include('Ads.mobile-ads.mobile_inner_article_advert3')
                                                    </div>
                                                @endif
                                            @endif
                                        @endif
                                        @if($i == 20)
                                            @php $adCount += 1; @endphp
                                            @if((new \Jenssegers\Agent\Agent())->isMobile())
                                                @if($parentId == 4 || $catId == 4)
                                                    <div class="ad-300-250-article justify-content-center my-2">
                                                        @include('Ads.desktop-ads.business_main_rightpanel_advert4')
                                                    </div>
                                                @elseif($parentId == 9 || $catId == 9)
                                                    <div class="ad-300-250-article justify-content-center my-2">
                                                        @include('Ads.desktop-ads.lifestyle_main_rightpanel_advert4')
                                                    </div>
                                                @elseif($parentId == 1 || $catId == 1)
                                                    <div class="ad-300-250-article justify-content-center my-2">
                                                        @include('Ads.mobile-ads.mobile_inner_article_advert4')
                                                    </div>
                                                @else
                                                    <div class="ad-300-250-article justify-content-center my-2">
                                                        @include('Ads.mobile-ads.mobile_inner_article_advert4')
                                                    </div>
                                                @endif
                                            @endif
                                        @endif
                                        @if(($i % 5 == 0) && ($art < 3))
                                            @if(isset($relatedArticles[$art]))
                                                <p class="mb-2"><a class="seealso" href="{{ $utils->assetLink("article", $relatedArticles[$art]->id, $relatedArticles[$art]->title) }}">SEE ALSO: {{ $relatedArticles[$art]->title }}</a></p>
                                            @endif
                                            @php $art++; @endphp
                                        @endif
                                        @php $i++; @endphp
                                    @endforeach
                                </div>
                            </div>
                            <hr class="my-4">
                            @if(isset($topics))
                                <h5 class="related-tags my-2">Related Topics</h5>
                                @foreach($topics as $value)
                                <a href="{{ url('topic/')}}/{{ $utils->assist->slugify($value) }}" class="btn btn-danger btn-tags my-2">{{ $value}}</a>
                                @endforeach
                            @endif

                            <!-- Google Ads -->
                            <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                            <ins class="adsbygoogle"
                                 style="display:block"
                                 data-ad-format="autorelaxed"
                                 data-ad-client="ca-pub-2204615711705377"
                                 data-ad-slot="7206855923"></ins>
                            <script>
                                (adsbygoogle = window.adsbygoogle || []).push({});
                            </script>
                            <!-- Google Ads End -->

                            <!-- Article comments -->
                            <div class="fb-comments" data-href="{{ url()->current() }}" data-width="100%" data-numposts="30"></div>

                            <!-- Article comments End -->


                    </div>
                </div>
                <div class="col-md-4 col-sm-12">
                    <div class="card national p-2">
                        @if((new Jenssegers\Agent\Agent())->isDesktop())
                            @if($parentId == 4 || $catId == 4)
                                <div class="ad-300-250-article justify-content-center my-4 mt-md-0 mx-auto">
                                    @include('Ads.desktop-ads.business_main_rightpanel_advert1')
                                </div>
                            @elseif($parentId == 5 || $catId == 5)
                                <div class="ad-300-250-article justify-content-center my-2">
                                    @include('Ads.desktop-ads.world_rightpanel_advert1')
                                </div>
                            @elseif($parentId == 9 || $catId == 9)
                                <div class="ad-300-250-article justify-content-center my-2">
                                    @include('Ads.desktop-ads.lifestyle_main_rightpanel_advert1')
                                </div>
                            @elseif($parentId == 41 || $catId == 41)
                                <div class="ad-300-250-article justify-content-center my-2">
                                    @include('Ads.desktop-ads.health_main_rightpanel_advert1')
                                </div>
                            @elseif($parentId == 1 || $catId == 1)
                                <div class="ad-300-250-article justify-content-center my-2">
                                    @include('Ads.desktop-ads.home_main_rightpanel_advert1')
                                </div>
                            @else
                                <div class="ad-300-250 justify-content-center my-4 mt-md-0 mx-auto">
                                    @include('Ads.desktop-ads.home_main_rightpanel_advert1')
                                </div>
                            @endif
                        @endif
                        @if($adCount == 3)
                                @if((new \Jenssegers\Agent\Agent())->isMobile())
                                    @if($parentId == 4 || $catId == 4)
                                        <div class="ad-300-250-article justify-content-center my-2">
                                            @include('Ads.desktop-ads.business_main_rightpanel_advert5')
                                        </div>
                                    @elseif($parentId == 9 || $catId == 9)
                                        <div class="ad-300-250-article justify-content-center my-2">
                                            @include('Ads.desktop-ads.lifestyle_main_rightpanel_advert5')
                                        </div>
                                    @elseif($parentId == 1 || $catId == 1)
                                        <div class="ad-300-250-article justify-content-center my-2">
                                            @include('Ads.mobile-ads.mobile_inner_article_advert5')
                                        </div>
                                    @else
                                        <div class="ad-300-250-article justify-content-center my-2">
                                            @include('Ads.mobile-ads.mobile_inner_article_advert5')
                                        </div>
                                    @endif
                                @endif
                        @endif
                    </div>
                    <div class="card national mt-30 p-md-2">
                        <div class="single-standard">
                            <h4 class="section-heading my-1">MOST READ</h4>
                        </div>
                        @if(isset($mostRead))
                        <div class="card p-0">
                            <div class="card-body p-0">
                                <div class="media popular-news">
                                    <ul class="list-group list-group-flush">
                                        @foreach($mostRead as  $value)
                                            <li class="list-group-item">
                                                <h4 class='mb-2'><a href='{{ url('/') }}{{ $value->url }}'>{{ htmlspecialchars_decode($value->page, ENT_QUOTES) }} </a></h4>
                                                <h6 class='my-1'><span class='by-line'>{{ $value->author }} </span></h6>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <hr class="my-2">
                            </div>
                        </div>
                        @endif
                    </div>
                    <div class="card national mt-30 p-md-3">
                        @if(isset($recommended))
                            @if((new Jenssegers\Agent\Agent())->isDesktop())
                                @if($parentId == 4 || $catId == 4)
                                    <div class="ad-300-250-article my-2">
                                        @include('Ads.desktop-ads.business_main_rightpanel_advert2')
                                    </div>
                                @elseif($parentId == 5 || $catId == 5)
                                    <div class="ad-300-250-article my-2">
                                        @include('Ads.desktop-ads.world_rightpanel_advert2')
                                    </div>
                                @elseif($parentId == 41 || $catId == 41)
                                    <div class="ad-300-250-article my-2">
                                        @include('Ads.desktop-ads.health_main_rightpanel_advert2')
                                    </div>
                                @elseif($parentId == 9 || $catId == 9)
                                    <div class="ad-300-250-article my-2">
                                        @include('Ads.desktop-ads.lifestyle_main_rightpanel_advert2')
                                    </div>
                                @else
                                    <div class="ad-300-250-article my-2">
                                        @include('Ads.desktop-ads.home_main_rightpanel_advert2')
                                    </div>
                                @endif
                            @endif
                        <div class="single-standard">

                            @if(count($recommended) > 0)
                            <h4 class="section-heading my-2 mx-0">RELATED STORIES </h4>
                                @php $i = 1; @endphp
                                @foreach($recommended as $value)
                                    @if($i == 1)
                                        <div class="standard-image">
                                            <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                                <img src="{{ asset('assets/images/pic.jpg') }}" data-src="{{ $value->thumbURL != "" ? $utils->imageLocation($value->thumbURL) : 'https://www.standardmedia.co.ke/images/pic.jpg' }}" class="card-img-top lazy" alt="{{ $value->title }}">
                                            </a>
                                        </div>
                                        <div class="card-body bg-white">
                                            <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}"> <h5 class="card-title">  {{ $value->title }} </h5></a>
                                        </div>
                                        <ul class="list-group list-group-flush">
                                            @else
                                                <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}"> <li class="list-group-item"> {{ $value->title }}</li></a>
                                            @endif
                                            @php $i++; @endphp
                                            @endforeach
                                        </ul>
                        </div>
                        @endif
                        @endif
                        @if(isset($topStories))
                            @if((new Jenssegers\Agent\Agent())->isDesktop())
                                @if($parentId == 4 || $catId == 4)
                                    <div class="ad-300-250-article my-2">
                                        @include('Ads.desktop-ads.business_main_rightpanel_advert3')
                                    </div>
                                @elseif($parentId == 5 || $catId == 5)
                                    <div class="ad-300-250-article my-2">
                                        @include('Ads.desktop-ads.world_rightpanel_advert3')
                                    </div>
                                @elseif($parentId == 41 || $catId == 41)
                                    <div class="ad-300-250-article my-2">
                                        @include('Ads.desktop-ads.health_main_rightpanel_advert3')
                                    </div>
                                @elseif($parentId == 9 || $catId == 9)
                                    <div class="ad-300-250-article my-2">
                                        @include('Ads.desktop-ads.lifestyle_main_rightpanel_advert3')
                                    </div>
                                @else
                                    <div class="ad-300-250-article my-2">
                                        @include('Ads.desktop-ads.home_main_rightpanel_advert3')
                                    </div>
                                @endif
                            @endif
                            <div class="single-standard">
                                <h4 class="section-heading my-2">LATEST STORIES</h4>
                                @php $i = 1; @endphp
                                @foreach($topStories as $value)
                                    @if($i == 1)
                                        <div class="standard-image">
                                                <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                                    <img src="{{ asset('assets/images/pic.jpg') }}" data-src="{{ $value->thumbURL != "" ? $utils->imageLocation($value->thumbURL) : 'https://www.standardmedia.co.ke/images/pic.jpg' }}" class="card-img-top lazy" alt="{{ $value->title }}">
                                                </a>
                                        </div>
                                        <div class="card-body bg-white">
                                            <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}"> <h5 class="card-title">  {{ $value->title }} </h5></a>
                                        </div>
                                        <ul class="list-group list-group-flush">
                                            @else
                                                <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}"> <li class="list-group-item"> {{ $value->title }}</li></a>
                                            @endif
                                            @php $i++; @endphp
                                            @endforeach
                                        </ul>
                                @endif
                            </div>
                            <div class="my-4 mx-auto text-center">
                                @if((new Jenssegers\Agent\Agent())->isDesktop())
                                    @if($parentId == 4 || $catId == 4)
                                        <div class="ad-300-250-article my-2">
                                            @include('Ads.desktop-ads.business_main_rightpanel_advert4')
                                        </div>
                                    @elseif($parentId == 5 || $catId == 5)
                                        <div class="ad-300-250-article my-2">
                                            @include('Ads.desktop-ads.world_rightpanel_advert4')
                                        </div>
                                    @elseif($parentId == 41 || $catId == 41)
                                        <div class="ad-300-250-article my-2">
                                            @include('Ads.desktop-ads.health_main_rightpanel_advert4')
                                        </div>
                                    @elseif($parentId == 9 || $catId == 9)
                                        <div class="ad-300-250-article my-2">
                                            @include('Ads.desktop-ads.lifestyle_main_rightpanel_advert4')
                                        </div>
                                    @else
                                        <div class="ad-300-250-article my-2">
                                            @include('Ads.desktop-ads.home_main_rightpanel_advert4')
                                        </div>
                                    @endif
                                @endif
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <hr class="my-4">
</section>
<!--------------------------------  End of Article Section ---------------------------------------->

<!-------------------------------- The Standard Insider ---------------------------------------->
<section class="">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="section-heading my-2 mx-0">THE STANDARD INSIDER</h4>
                <div class="card-deck insider">
                    @if(isset($insider))
                        @foreach($insider as $value)
                            <div class="card premiumcontent mb-4">
                                <div class="single-standard">
                                    <div class="standard-image">
                                            <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                                <img src="{{ asset('assets/images/pic.jpg') }}" data-src="{{ $value->thumbURL != "" ? $utils->imageLocation($value->thumbURL) : 'https://www.standardmedia.co.ke/images/pic.jpg' }}" class="card-img-top lazy" alt="{{ $value->title }}">
                                            </a>
                                        <p class="premium"><i class="fa fa-lock"></i></p>
                                    </div>
                                </div>
                                <div class="card-body">
                                        <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                            <p class="card-text black">{{ $value->title }}</p>
                                        </a>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>

<!-------------------------------- End The Standard Insider ---------------------------------------->

<!-------------------------------- Read More ---------------------------------------->
<section>
    <div class="container">
        @if(isset($moreNews))
        @if(count($moreNews) > 0)
            <h4 class="section-heading my-2 mx-0">Read More</h4>
            <div class="card-deck">
                @foreach($moreNews as $value)
                    <div class="card check">
                        <div class="single-standard">
                            <div class="standard-image">
                                <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                    <img src="{{ asset('assets/images/pic.jpg') }}" data-src="{{ $value->thumbURL != "" ? $utils->imageLocation($value->thumbURL) : 'https://www.standardmedia.co.ke/images/pic.jpg' }}" class="card-img-top lazy" alt="{{ $value->title }}">
                                </a>
                                <a href="{{ $utils->assetLink("category", $value->categoryid, $utils->home->getCategoryName($value->categoryid)) }}">
                                    <p class="imgbtn">{{ $utils->home->getCategoryName($value->categoryid) }}</p>
                                </a>
                            </div>
                        </div>
                        <div class="card-body bg-white">
                            <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                <p class="card-text black">{{ $value->title }} </p>
                                <p class="card-text mt-3 grey"> {{ $value->long_title }}</p>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        @endif
        @endif
    </div>
</section>

<!-------------------------------- End of Read More ---------------------------------------->
@endsection
