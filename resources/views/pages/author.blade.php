@extends('layout.structure')
@section('content')
    <section id="standard" class="standard-area pt-100 mt-3 mt-md-2 author">
        <div class="container mx-auto mt-4 mt-lg-0 mt-md-5">
            <div class="row my-2">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                @if ($message = Session::get('error'))
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
            </div>
            <!-------------------------------- Breaking news ---------------------------------------->
            @if(isset($breaking) && ($breaking != null))
                @include('includes.breakingNews')
            @endif
            @if(isset($author))
            <h4 class="section-heading my-2 mx-0 authorer">
                <a href="{{ $utils->assetLink("author", $author) }}">{{ $author }}</a>
                , The Standard
{{--                <small class="float-right">{{ $articlesCount }} Articles</small>--}}
            </h4>
            @endif
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-lg-7 col-md-7 mb-2">
                            <div class="single-standard mainimg">
                            @if(isset($firstList))
                                @php $i = 1; @endphp
                                @foreach($firstList as $value)
                                    @if($i ==1 )
                                <div class="standard-image">
                                    <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                        <img src="{{ asset('assets/images/pic.jpg') }}" data-src="{{ $value->thumbURL != "" ? $utils->imageLocation($value->thumbURL) : 'https://www.standardmedia.co.ke/images/pic.jpg' }}" class="h-100  m-0 p-0 lazy" alt="{{ $value->title }}">
                                    </a>
                                </div>
                                <div class="standard-content">
                                    <div class="content content-category-card">
                                        <p class="smalltext py-1 px-4"><font class="redtext">By {{ $value->author }}</font> {{ $utils->assist->timeAgo($value->publishdate) }}</p>
                                        <h4 class="bigtext my-2 py-1 px-3 px19"><a href="{{ $utils->assetLink("article", $value->id, $value->title) }}"><b>{{ $value->title }}</b></a></h4>
                                        <h4 class="title mb-2"><a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">{{ $value->summary }}</a></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-5">
                            @else
                            <div class="wrapper bg-white  px-3 mb-2 ">
                                <div class="single-standard row">
                                    <div class="standard-image">
                                        <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                            <img src="{{ asset('assets/images/pic.jpg') }}" data-src="{{ $value->thumbURL != "" ? $utils->imageLocation($value->thumbURL) : 'https://www.standardmedia.co.ke/images/pic.jpg' }}" class="h-100 lazy m-0 p-0" alt="{{ $value->title }}">
                                        </a>
                                    </div>
                                    <div class="standard-content  border-0 p-0 pb-1 p-2 ">
                                        <div class="content p-0 pl-2">
                                            <p class="smalltext py-1 px-1"><font class="redtext">By {{ $value->author }}</font> {{ $utils->assist->timeAgo($value->publishdate) }}</p>
                                            <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                                <h4 class="bigtext py-1">{{ $value->title }}</h4>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            @php $i++; @endphp
                            @endforeach
                            @endif
                        </div>
                    </div>
                    <div class="row mt-4">
                        @if(isset($secondList))
                            @foreach($secondList as $value)
                        <div class="col-md-6">
                            <div class="wrapper bg-white  px-3 mb-4">
                                <div class="single-standard row biggercard">
                                    <div class="standard-image">
                                        <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                            <img src="{{ asset('assets/images/pic.jpg') }}" data-src="{{ $value->thumbURL != "" ? $utils->imageLocation($value->thumbURL) : 'https://www.standardmedia.co.ke/images/pic.jpg' }}" class="h-100 lazy m-0 p-0 px300" alt="{{ $value->title }}">
                                        </a>
                                    </div>
                                    <div class="standard-content  border-0 p-0 pb-3 p-2 ">
                                        <div class="content p-0 pl-2">
                                            <p class="smalltext py-2 px-1"><font class="redtext">By {{ $value->author }}</font> {{ $utils->assist->timeAgo($value->publishdate) }}</p>
                                            <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                                <h4 class="bigtext py-2">{{ $value->title }}</h4>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="moreauthor">
                        @if(isset($author))
                        <h4 class="section-heading my-2 mx-0">More From <a href="{{ $utils->assetLink("author", $author) }}">{{ $author }}</a></h4>
                        @endif
                        <div class="row">
                        @if(isset($lastList))
                            @foreach($lastList as $value)
                            <div class="col-md-4 mb-3">
                                <div class="card national">
                                    <div class="single-standard">
                                        <div class="standard-image">
                                            <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                                <img src="{{ asset('assets/images/pic.jpg') }}" data-src="{{ $value->thumbURL != "" ? $utils->imageLocation($value->thumbURL) : 'https://www.standardmedia.co.ke/images/pic.jpg' }}" class="card-img-top lazy" alt="{{ $value->title }}">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="card-body card-national">
                                        <a href="">
                                            <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                                <p class="card-text black"> {{ $value->title }}</p>
                                            </a>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        @endif
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    @if((new Jenssegers\Agent\Agent())->isDesktop())
                        <div class="ad-300-250 justify-content-center">
                            @include('Ads.desktop-ads.home_main_rightpanel_advert1')
                        </div>
                    @endif<div class="card national mt-4 p-md-2">
                        <h4 class="section-heading mb-1 mt-2 mx-0">MOST READ</h4>
                        @if(isset($mostRead))
                            <div class="card p-0">
                                <div class="card-body p-0">
                                    <div class="media popular-news">
                                        <ul class="list-group list-group-flush">
                                            @foreach($mostRead as  $value)
                                                <li class="list-group-item">
                                                    <h4 class='mb-2'><a href='{{ url('/') }}{{ $value->url }}'>{{ htmlspecialchars_decode($value->page, ENT_QUOTES) }}  </a></h4>
                                                    <h6 class='my-1'><span class='by-line'>{{ $value->author }} </span></h6>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <hr class="my-2">
                                </div>
                            </div>
                        @endif
                    </div>
                        <div class="text-center my-2">
                            @if((new Jenssegers\Agent\Agent())->isDesktop())
                                <div class="ad-300-250 justify-content-center">
                                    @include('Ads.desktop-ads.home_main_rightpanel_advert2')
                                </div>
                            @endif
                        </div>
                    <div class="card national mt-30 mt-md-0 p-md-2">
                        @if($latestOne)
                        <div class="single-standard">
                            <h4 class="section-heading my-2 mx-0">LATEST STORIES</h4>
                        </div>
                            <div class="card p-0">
                                <div class="card-body p-0">
                                    <div class="media popular-news">
                                        <ul class="list-group list-group-flush">
                                            @foreach($latestOne as  $value)
                                                <li class="list-group-item">
                                                    <h4 class='mb-2'><a href='{{ $utils->assetLink("article", $value->id, $value->title) }}'>{{ $value->title}} </a></h4>
                                                    <h6 class='my-1'><span class='by-line'>{{ $value->author }} </span></h6>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <hr class="my-2">
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="text-center my-2">
                        @if((new Jenssegers\Agent\Agent())->isDesktop())
                            <div class="ad-300-250 justify-content-center">
                                @include('Ads.desktop-ads.home_main_rightpanel_advert3')
                            </div>
                        @endif
                    </div>
                    <div class="card national mt-4 p-md-2">
                        @if(isset($checkpoint))
                        <div class="single-standard">
                            <h4 class="section-heading my-2 mx-0">CHECKPOINT</h4>
                        </div>
                        <div class="card p-0">
                            <div class="card-body p-0">
                                <div class="media popular-news">
                                    <ul class="list-group list-group-flush">
                                        @foreach($checkpoint as  $value)
                                            <li class="list-group-item">
                                                <h4 class='mb-2'><a href='{{ $utils->assetLink("article", $value->id, $value->title) }}'>{{ $value->title}} </a></h4>
                                                <h6 class='my-1'><span class='by-line'>{{ $value->author }} </span></h6>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <hr class="my-2">
                            </div>
                        </div>
                        @endif
                    </div>

                </div>
            </div>
            <hr class="my-4">
        </div>
    </section>
       <section class="container">
        <div class="">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="section-heading my-2 mx-0">THE STANDARD INSIDER</h4>
                    <div class="card-deck insider">
                        @if(isset($insider))
                            @foreach($insider as $value)
                                <div class="card premiumcontent mb-4">
                                    <div class="single-standard">
                                        <div class="standard-image">
                                            @if(Session::get('user') != null)
                                                <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                                    <img src="{{ asset('assets/images/pic.jpg') }}" data-src="{{ $value->thumbURL != "" ? $utils->imageLocation($value->thumbURL) : 'https://www.standardmedia.co.ke/images/pic.jpg' }}" class="card-img-top lazy" alt="{{ $value->title }}">
                                                </a>
                                            @else
                                                <a data-scroll-nav="0" data-toggle="modal" data-dismiss="modal" data-target="#loginModal" href="#">
                                                    <img src="{{ asset('assets/images/pic.jpg') }}" data-src="{{ $value->thumbURL != "" ? $utils->imageLocation($value->thumbURL) : 'https://www.standardmedia.co.ke/images/pic.jpg' }}" class="card-img-top  lazy" alt="{{ $value->title }}">
                                                </a>
                                            @endif
                                            <p class="premium"><i class="fa fa-lock"></i></p>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        @if(Session::get('user') != null)
                                            <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                                <p class="card-text black">{{ $value->title }}</p>
                                            </a>
                                        @else
                                            <a data-scroll-nav="0" data-toggle="modal" data-dismiss="modal" data-target="#loginModal" href="#">
                                                <p class="card-text black">{{ $value->title }}</p>
                                            </a>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
       <section>
        <div class="container">
            <h4 class="section-heading my-2 mx-0">LATEST NEWS</h4>
            <div class="card-deck">
                @if(isset($latest))
                    @foreach($latest as $value)
                        <div class="card check">
                            <div class="single-standard">
                                <div class="standard-image">
                                    <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                        <img src="{{ asset('assets/images/pic.jpg') }}" data-src="{{ $value->thumbURL != "" ? $utils->imageLocation($value->thumbURL) : 'https://www.standardmedia.co.ke/images/pic.jpg' }}" class="card-img-top lazy" alt="{{ $value->title }}">
                                    </a>
                                    <a href="{{ $utils->assetLink("category", $value->categoryid, $utils->home->getCategoryName($value->categoryid)) }}">
                                        <p class="imgbtnup">{{ $utils->home->getCategoryName($value->categoryid) }}</p>
                                    </a>
                                </div>
                            </div>
                            <div class="card-body bg-white">
                                <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                    <p class="card-text black">{{ $value->title }} </p>
                                    <p class="card-text mt-3 grey"> {{ substr($value->summary, 0, 100) }}...</p>
                                </a>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </section>
@endsection
