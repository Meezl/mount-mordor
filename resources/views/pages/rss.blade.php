@extends('layout.structure')
@section('content')
    <section id="standard" class="standard-area pt-100 mt-3 mt-md-2">
        <div class="container mx-auto mt-4 mt-lg-0 mt-md-5">
            <style>
                .rss-table{
                    width: 650px;
                    margin-top: 10px;
                }
                .rss-table th{
                    background: #ccc;
                    padding-bottom: 8px;
                    padding-top: 8px;
                    text-transform: uppercase;
                    text-align: left;
                    padding-left: 10px;
                    border-bottom: 1px solid #000080;

                }
                .rss-table td{
                    padding-left: 10px;
                    padding-top: 5px;
                    padding-bottom: 5px;
                    font-size: 12px;
                    border-bottom: 1px solid #ccc;
                }
                .rss-table a{
                    color: #0065B1;

                }
            </style>
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <nav>
                        <ol class="breadcrumb" style="background: transparent !important;">
                            <li class="breadcrumb-item">
                                <a href="{{ url('/') }}"> Home </a>
                            </li>
                            <li class="breadcrumb-item active">
                                <a href="{{  url()->current() }}">
                                    RSS feed
                                </a>
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="row my-2">
                <div class="col-md-8">
                    <h2 class="cat-title">Standard Digital  RSS</h2>
                    <div class="clr"></div>
                    Copy the RSS URL of the topic of your interest and paste it on your RSS Reader
                    <div class="clr"></div>
                    <table border="0" cellspacing="0" cellpadding="0" class="rss-table w-100">
                        <tbody>
                        <tr>
                            <th>Categories</th><th>RSS URL</th></tr>
                        <tr>
                            <td>Main headlines</td><td><a href="https://www.standardmedia.co.ke/rss/headlines.php">https://www.standardmedia.co.ke/rss/headlines.php</a></td></tr>
                        <tr>
                            <td>Kenya News</td>
                            <td><a href="https://www.standardmedia.co.ke/rss/kenya.php">https://www.standardmedia.co.ke/rss/kenya.php</a></td>
                        </tr>

                        <tr>
                            <td>World News</td>
                            <td><a href="https://www.standardmedia.co.ke/rss/world.php">https://www.standardmedia.co.ke/rss/world.php</a></td>
                        </tr>
                        <tr>
                            <td>Politics</td>
                            <td><a href="https://www.standardmedia.co.ke/rss/politics.php">https://www.standardmedia.co.ke/rss/politics.php</a></td>
                        </tr>
                        <tr>
                            <td>Opinion</td>
                            <td><a href="https://www.standardmedia.co.ke/rss/opinion.php">https://www.standardmedia.co.ke/rss/opinion.php</a></td>
                        </tr>


                        <tr><td>Sports</td><td><a href="https://www.standardmedia.co.ke/rss/sports.php">https://www.standardmedia.co.ke/rss/sports.php</a></td></tr>
                        <tr><td>Business</td><td><a href="https://www.standardmedia.co.ke/rss/business.php">https://www.standardmedia.co.ke/rss/business.php</a></td></tr>
                        <tr><td>Columnists</td><td><a href="https://www.standardmedia.co.ke/rss/columnists.php">https://www.standardmedia.co.ke/rss/columnists.php</a></td></tr>

                        <tr>
                            <td>Magazines/Pullouts</td>
                            <td><a href="https://www.standardmedia.co.ke/rss/magazines.php">https://www.standardmedia.co.ke/rss/magazines.php</a></td>
                        </tr>

                        <tr>
                            <td>Agriculture</td>
                            <td><a href="https://www.standardmedia.co.ke/rss/agriculture.php">https://www.standardmedia.co.ke/rss/agriculture.php</a></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>KTN videos</td>
                            <td><a href="https://www.standardmedia.co.ke/rss/ktnvideos.php">https://www.standardmedia.co.ke/rss/ktnvideos.php</a></td>
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>

                        <tr>
                            <td><strong>Eve Woman</strong></td>
                            <td><strong><a href="https://www.standardmedia.co.ke/rss/evewoman.php">https://www.standardmedia.co.ke/rss/evewoman.php</a></strong></td>
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td><strong>Entertainment (SDE)</strong></td>
                            <td><strong><a href="https://www.standardmedia.co.ke/rss/entertainment.php">https://www.standardmedia.co.ke/rss/entertainment.php</a></strong></td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="clr"></div>
                </div>
                <div class="col-md-4 col-sm-12">
                    <div class="card national mt-30 p-md-2">
                        <div class="single-standard">
                            <h4 class="section-heading my-1">MOST READ</h4>
                        </div>
                        @if(isset($mostRead))
                            <div class="card p-0">
                                <div class="card-body p-0">
                                    <div class="media popular-news">
                                        <ul class="list-group list-group-flush">
                                            @foreach($mostRead as  $value)
                                                <li class="list-group-item">
                                                    <h4 class='mb-2'><a href='{{ url('/') }}{{ $value->url }}'>{{ $value->page }} </a></h4>
                                                    <h6 class='my-1'><span class='by-line'>{{ $value->author }} </span></h6>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <hr class="my-2">
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
</section>

