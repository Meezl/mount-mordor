@extends('layout.structure')
@section('content')
    <section id="standard" class="standard-area pt-100 mt-3 mt-md-2">
        <div class="container mx-auto mt-4 mt-lg-0 mt-md-5">
            <div class="row my-2">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                @if ($message = Session::get('error'))
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
            </div>
            @if($topic === 'coronavirus')
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div style="background: url('https://www.standardmedia.co.ke/flash/banner_corona.jpg'); width: 100%; height: 210px; margin-bottom: 20px; background-size: cover;">
                            <div class="covid">The Coronavirus(Covid-19) Pandemic</div>
                        </div>
                    </div>
                </div>
            @endif
            <!-------------------------------- Breaking news ---------------------------------------->
            @if(isset($breaking) && ($breaking != null))
                @include('includes.breakingNews')
            @endif
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <nav>
                        <ol class="breadcrumb" style="background: transparent !important;">
                            <li class="breadcrumb-item">
                                <a href="{{ url('/') }}"> Home </a>
                            </li>
                            <li class="breadcrumb-item active">
                                <a href="{{  url()->current() }}">
                                    {{ ucwords($topic) }}
                                </a>
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="row">
                        <div class="moreauthor">
                            @if(isset($topic))
                                <h4 class="section-heading my-2 mx-0"> <a href="#">{{ strtoupper($topic) }}</a></h4>
                            @endif
                            <div class="row">
                                @if(isset($articles))
                                    @foreach($articles as $value)
                                        <div class="col-md-4 mb-3">
                                            <div class="card national">
                                                <div class="single-standard">
                                                    <div class="standard-image">
                                                        <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                                            <img src="{{ asset('assets/images/pic.jpg') }}" data-src="{{ $value->thumbURL != "" ? $utils->imageLocation($value->thumbURL) : 'https://www.standardmedia.co.ke/images/pic.jpg' }}" class="card-img-top lazy" alt="{{ $value->title }}">
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="card-body card-national">
                                                    <a href="">
                                                        <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                                            <p class="card-text black"> {{ $value->title }}</p>
                                                            <h6 class="topic-author mt-2"> {{ $value->author  }}</h6>
                                                        </a>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 mt-1 mb-3">
                    @if($topic == 'coronavirus' || "Coronavirus" || "covid19" || "Covid-19" || "Covid19" || "covid-19")
                    <h4 class="section-heading mb-2 ">Watch Live</h4>
                    <iframe width="100%" height="240" src="https://www.youtube.com/embed/live_stream?channel=UCKVsdeoHExltrWMuK0hOWmg&autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    @endif
                    @if((new Jenssegers\Agent\Agent())->isDesktop())
                        <div class="ad-300-250 justify-content-center">
                            @include('Ads.desktop-ads.home_main_rightpanel_advert1')
                        </div>
                    @endif
                    <h4 class="section-heading mb-1 mt-4">Latest News</h4>
                    <div class="card p-0">
                        <div class="card-body p-0">
                            <div class="media popular-news">
                                <ul class="list-group list-group-flush card-anchor">
                                    @if(isset($topStories))
                                        @foreach($topStories as $value)
                                            <li class="list-group-item">
                                                <h4 style="font-size: 12px;" class="mb-2"><a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">{{ $value->title }}</a></h4>
                                                <h6 class='my-2'><span class='by-line'>{{ ucwords($value->author) }}</span></h6>
                                            </li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                            <hr class="my-1">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-------------------------------- The Standard Insider ---------------------------------------->
    <section class="">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="section-heading my-2 mx-0">THE STANDARD INSIDER</h4>
                    <div class="card-deck insider">
                        @if(isset($insider))
                            @foreach($insider as $value)
                                <div class="card premiumcontent mb-4">
                                    <div class="single-standard">
                                        <div class="standard-image">
                                            @if(Session::get('user') != null)
                                                <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                                    <img src="{{ asset('assets/images/pic.jpg') }}" data-src="{{ $value->thumbURL != "" ? $utils->imageLocation($value->thumbURL) : 'https://www.standardmedia.co.ke/images/pic.jpg' }}" class="card-img-top lazy" alt="{{ $value->title }}">
                                                </a>
                                            @else
                                                <a data-scroll-nav="0" data-toggle="modal" data-dismiss="modal" data-target="#loginModal" href="#">
                                                    <img src="{{ asset('assets/images/pic.jpg') }}" data-src="{{ $value->thumbURL != "" ? $utils->imageLocation($value->thumbURL) : 'https://www.standardmedia.co.ke/images/pic.jpg' }}" class="card-img-top lazy" alt="{{ $value->title }}">
                                                </a>
                                            @endif
                                            <p class="premium"><i class="fa fa-lock"></i></p>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        @if(Session::get('user') != null)
                                            <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                                <p class="card-text black">{{ $value->title }}</p>
                                            </a>
                                        @else
                                            <a data-scroll-nav="0" data-toggle="modal" data-dismiss="modal" data-target="#loginModal" href="#">
                                                <p class="card-text black">{{ $value->title }}</p>
                                            </a>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-------------------------------- End of The Standard Insider ---------------------------------------->
@endsection
