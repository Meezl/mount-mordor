@extends('layout.structure')
@section('content')
    <script>
        window._io_config = window._io_config || {};
        window._io_config["0.2.0"] = window._io_config["0.2.0"] || [];
        window._io_config["0.2.0"].push({
            page_url: "{{ url()->current() }}",
            page_url_canonical: "{{ url()->current() }}",
            page_title: "{{ $title ?? "The Standard - Breaking News, Kenya News, World News and Videos" }}",
            page_type: "main",
            page_language: "en"
        });
    </script>
    <!-------------------------------- Top Section ---------------------------------------->
    <section id="standard" class="standard-area pt-100 homept mt-md-2 mt-lg-3">
        <div class="container mx-auto mt-4 mt-lg-0 mt-md-5">
            <div class="offset-md-4 row my-2">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                @if ($message = Session::get('error'))
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
            </div>
            <div class="row">
                @if((new Jenssegers\Agent\Agent())->isDesktop())
                    <div class="ad-728-90 offset-md-2 col-md-8 mb-1">
                        @include('Ads.desktop-ads.home_main_advert1')
                    </div>
                @endif
            </div>
            <!-------------------------------- Breaking news ---------------------------------------->
            @if(isset($breaking) && ($breaking != null))
                @include('includes.breakingNews')
            @endif
            @if(isset($live) && ($live != null))
                @if($live->isactive == 1)\
                    @include('includes.liveStream')
                @endif
            @endif

            <!-------------------------------- End Breaking news ---------------------------------------->
            <div class="row justify-content-center">
                <div class="col-md-8 top-margin">
                    @if((new Jenssegers\Agent\Agent())->isMobile())
                        <div class="ad-728-90 offset-md-2 col-md-8 mb-1">
                            @include('Ads.mobile-ads.home_mobile_main_top_320x100')
                        </div>
                    @endif
                    <div class="card-deck">
                    @if(isset($topStories))
                        @php $i = 1; @endphp
                        @foreach($topStories as $value)
                            @if($i ==1 )
                        <div class="col-lg-7 col-md-7 p-0">
                        <div class="card p-3 h512">
                            <div class="card-body">
                                <h3 class="heading"><a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">{{ $value->title }} </a></h3>
                            </div>
                             <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                               <img src="{{ $utils->imageLocation($value->thumbURL) }}" class="card-img-bottom" alt="{{ $value->title }}">
                             </a>
                        </div>
                        </div>
                        <div class="col-lg-5 col-md-5 p-0">
                            @if((new \Jenssegers\Agent\Agent())->isMobile())
                                <div class="ad-300-250 justify-content-center">
                                    @include('Ads.mobile-ads.home_mobile_advert1')
                                </div>
                            @endif
                            @else
                                <div class="wrapper bg-white  px-3 pt-2 mt-1 mb-4 justify-content-start">
                                    <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                        <h4 class="title py-2">{{ $value->title }}</h4>
                                    </a>
                                    <div class="single-standard row p-2">
                                        <div class="standard-image col-md-6 m-0 p-0 pb-3">
                                            <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">   <img src="{{ $utils->imageLocation($value->thumbURL) }}" class="" alt="{{ $value->title }}"></a>
                                        </div>
                                        <div class="standard-content col-md-6 border-0 p-0 pb-">
                                            <div class="content p-0 pl-2">
                                                <a href="{{ $utils->assetLink("category", $value->categoryid, $utils->home->getCategoryName($value->categoryid)) }}">
                                                    <h5 class="categoryname">{{ $utils->home->getCategoryName($value->categoryid) }}</h5>
                                                </a>
                                                <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                                    <p class="small-text">{{ substr($value->summary, 0, 70) }}...</p>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            @php $i++; @endphp
                            @endforeach
                            @endif
                        @if((new \Jenssegers\Agent\Agent())->isMobile())
                            <div class="ad-300-250 justify-content-center">
                                @include('Ads.mobile-ads.home_mobile_advert2')
                            </div>
                        @endif
                        </div>
                    </div>
                    <div class="row mt-4">
                        @if(isset($topStoriesTwo))
                            @foreach($topStoriesTwo as $value)
                                <div class="single-standard row p-md-2 pt-2 col-md-6 ml-2 substories mb-4">
                                    <div class="standard-image col-md-6 m-0 bg-white  py-2">
                                        <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                            <img src="{{ asset('assets/images/pic.jpg') }}" data-src="{{ $value->thumbURL != "" ? $utils->imageLocation($value->thumbURL) : 'https://www.standardmedia.co.ke/images/pic.jpg' }}" class="smallimg lazy" alt="{{ $value->title }}">
                                        </a>
                                    </div>
                                    <div class="standard-content col-md-6 border-0  py-2">
                                        <div class="content p-md-0 p-3">
                                            <a href="{{ $utils->assetLink("category", $value->categoryid, $utils->home->getCategoryName($value->categoryid)) }}">
                                                <h5 class="categoryname">{{ $utils->home->getCategoryName($value->categoryid) }}</h5>
                                            </a>
                                            <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                                <p class="small-text"> {{ $value->title }}</p>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 mt-1 mb-3 top-margin">
                    <div class="card national p-2">
                        @if((new Jenssegers\Agent\Agent())->isDesktop())
                            <div class="ad-300-250 justify-content-center">
                                    @include('Ads.desktop-ads.home_main_rightpanel_advert1')
                            </div>
                        @endif
                        @if((new \Jenssegers\Agent\Agent())->isMobile())
                            <div class="ad-300-250 justify-content-center">
                            @include('Ads.mobile-ads.home_mobile_advert3')
                            </div>
                        @endif
                    </div>
                    <h4 class="section-heading mb-1 mt-2 mx-0">MOST READ</h4>
                        @if(isset($popularNews))
                    <div class="card p-0">
                        <div class="card-body p-0">
                            <div class="media popular-news">
                                <ul class="list-group list-group-flush">
                                    @foreach($popularNews as  $value)
                                    <li class="list-group-item">
                                        <h4 class='mb-2'><a href='{{ url('/') }}{{ $value->url }}'>{{ htmlspecialchars_decode($value->page, ENT_QUOTES)  }} </a></h4>
                                        <h6 class='my-1'><span class='by-line'>{{ $value->author }} </span></h6>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                            <hr class="my-2">
                        </div>
                    </div>
                    @endif
                </div>
            </div>
            <hr class="my-4">
        </div>
    </section>
    <!-------------------------------- Enf of Top Section ---------------------------------------->

    <!-------------------------------- The Standard Insider ---------------------------------------->
    <section class="container">
        <div class="">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="section-heading my-2 mx-0">THE STANDARD INSIDER</h4>
                    <div class="card-deck insider">
                        @if(isset($insider))
                            @foreach($insider as $value)
                                <div class="card premiumcontent mb-4">
                                    <div class="single-standard">
                                        <div class="standard-image">
                                                <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                                    <img src="{{ asset('assets/images/pic.jpg') }}" data-src="{{ $value->thumbURL != "" ? $utils->imageLocation($value->thumbURL) : 'https://www.standardmedia.co.ke/images/pic.jpg' }}" class="card-img-top lazy" alt="{{ $value->title }}">
                                                </a>
                                            <p class="premium"><i class="fa fa-lock"></i></p>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                            <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                                <p class="card-text black">{{ $value->title }}</p>
                                            </a>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                    @if((new \Jenssegers\Agent\Agent())->isMobile())
                        <div class="ad-300-250 justify-content-center">
                            @include('Ads.mobile-ads.home_mobile_advert4')
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>

    <!-------------------------------- End of The Standard Insider ---------------------------------------->

    <!-------------------------------- Latest News Section ------------------------------------------------>
    <section>
        <div class="container">
            <h4 class="section-heading my-2 mx-0">LATEST NEWS</h4>
            <div class="card-deck">
                @if(isset($latest))
                    @foreach($latest as $value)
                        <div class="card check">
                            <div class="single-standard">
                                <div class="standard-image">
                                    <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                        <img src="{{ asset('assets/images/pic.jpg') }}" data-src="{{ $value->thumbURL != "" ? $utils->imageLocation($value->thumbURL) : 'https://www.standardmedia.co.ke/images/pic.jpg' }}" class="card-img-top lazy" alt="{{ $value->title }}">
                                    </a>
                                    <a href="{{ $utils->assetLink("category", $value->categoryid, $utils->home->getCategoryName($value->categoryid)) }}">
                                        <p class="imgbtnup">{{ $utils->home->getCategoryName($value->categoryid) }}</p>
                                    </a>
                                </div>
                            </div>
                            <div class="card-body bg-white">
                                <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                    <p class="card-text black">{{ $value->title }} </p>
                                    <p class="card-text mt-3 grey"> {{ substr($value->summary, 0, 100) }}...</p>
                                </a>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </section>
    <!-------------------------------- End of Latest News Section ---------------------------------------->

    <!-------------------------------- Economy & Latest Videos Section ----------------------------------->
    <section class="d-none d-md-block d-lg-block my-4">
        <div class="container">
            <div class="ad">
                <div class="row py-5 mx-0">
                        @if((new Jenssegers\Agent\Agent())->isDesktop())
                            <div class="ad-728-90 offset-md-2 col-md-8 ">
                                @include('Ads.desktop-ads.home_main_advert2')
                            </div>
                        @endif
                        @if((new \Jenssegers\Agent\Agent())->isMobile())
                            <div class="card national p-2">
                                <div class="ad-300-250 justify-content-center">
                                    @include('Ads.mobile-ads.home_mobile_advert5')
                                </div>
                            </div>
                        @endif
                </div>
            </div>
        </div>
    </section>
    <section class="">
        <div class="container">
            <div class="row">
                <div class="col-md-8 economycards">
                    <h4 class="section-heading my-2 mx-0">ECONOMY</h4>
                    <div class="row">
                        @if(isset($economy))
                            @foreach($economy as $value)
                                <div class="col-md-6 mb-3">
                                    <div class="card national">
                                        <div class="single-standard">
                                            <div class="standard-image">
                                                <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                                    <img src="{{ asset('assets/images/pic.jpg') }}" data-src="{{ $value->thumbURL != "" ? $utils->imageLocation($value->thumbURL) : 'https://www.standardmedia.co.ke/images/pic.jpg' }}" class="card-img-top  lazy" alt="{{ $value->title }}">
                                                </a>
                                                <a href="{{ $utils->assetLink("category", $value->categoryid, $utils->home->getCategoryName($value->categoryid)) }}">
                                                    <p class="imgbtnhome">{{ $utils->home->getCategoryName($value->categoryid) }}</p>
                                                </a>
                                            </div></div>
                                        <div class="card-body card-national bg-white">
                                            <a href="">
                                                <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                                    <p class="card-text black"> {{ $value->title }}</p>
                                                </a>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <h4 class="section-heading my-2 mx-0">LATEST VIDEOS</h4>
                    <div class="card">
                        @php
                            $i = 1;
                        @endphp
                        @if(isset($latestVideos))
                            <div class="card-body ">
                            @foreach($latestVideos as $value)
                                            <div class="media my-2">
                                                <div class="single-standard">
                                                    <i class="fa fa-play vidasmall"></i>
                                                    <a href="{{ $utils->videoLink($value->source,"watch",$value->id,$value->title) }}">
                                                        @if($value->platform == "vimeo")
                                                            <img src="{{ url('/') }}{!!  $value->videothumb !!}" class="mr-md-3" alt="{{ $value->title }}">
                                                        @elseif($value->platform=="dailymotion")
                                                            <img src="{{ "https://www.dailymotion.com/thumbnail/video/".$value->videoURL }}" class="mr-md-3g" alt="{{ $value->title }}">
                                                        @else
                                                            <img src="{{  $utils->imageLocation($value->videoURL,'youtube') }}" class="mr-md-3" alt="{{ $value->title }}">
                                                        @endif
                                                    </a>
                                                </div>

                                                <div class="media-body ml-3 ml-md-0 news-videos card-anchor">
                                                    <a href="{{ $utils->videoLink($value->source,"watch",$value->id,$value->title) }}"> {{ $value->title }} </a>
                                                    <h4 class="my-1"><span class="videos-byline">{{ ucwords($value->source) }}</span></h4>
                                                </div>
                                            </div>
                                            <hr class="my-2">
                                        @endforeach
                                        @endif
                                    </div>
                    </div>
                </div>
            </div>

            <hr class="my-2">
        </div>
    </section>

    <!-------------------------------- End of Economy & Latest Videos Section ----------------------------------->

    <!------------------------------- National, Politics, Entertainment & Sports Section ------------------------>
    <section class="stripedcard">
        <div class="container">
            <div class="row card-deck">
                <div class="card bgtrans">
                    <h4 class="section-heading my-2 mx-md-0 mx-3">NATIONAL</h4>
                    <div class="card national mx-md-0 mx-3 home-categories-card">
                        @if(isset($kenya))
                            @php $i = 1; @endphp
                            @foreach($kenya as $value)
                                @if($i == 1)
                                    <div class="single-standard">
                                        <div class="standard-image">
                                            <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                                <img src="{{ asset('assets/images/pic.jpg') }}" data-src="{{ $value->thumbURL != "" ? $utils->imageLocation($value->thumbURL) : 'https://www.standardmedia.co.ke/images/pic.jpg' }}" class="card-img-top lazy" alt="{{ $value->title }}">
                                            </a>
                                            <a href="{{ $utils->assetLink("category", $value->categoryid, $utils->home->getCategoryName($value->categoryid)) }}">
                                                <p class="imgbtnhome">{{ $utils->home->getCategoryName($value->categoryid) }}</p>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="card-body categories-card bg-white px-2">
                                        <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                            <h5 class="card-title p-3">{{ $value->title }} </h5>
                                        </a>
                                        <ul class="list-group list-group-flush card-anchor px-0">
                                            @else
                                                <li class="list-group-item">
                                                    <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                                        {{ $value->title }}
                                                    </a>
                                                </li>
                                            @endif
                                            @php $i++; @endphp
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                    </div>
                </div>
                <div class="card bgtrans">
                    <h4 class="section-heading my-2 mx-md-0 mx-3">POLITICS</h4>
                    <div class="card national mx-md-0 mx-3 home-categories-card">
                        @if(isset($politics))
                            @php $i = 1; @endphp
                            @foreach($politics as $value)
                                @if($i == 1)
                                    <div class="single-standard">
                                        <div class="standard-image">
                                            <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                                <img src="{{ asset('assets/images/pic.jpg') }}" data-src="{{ $value->thumbURL != "" ? $utils->imageLocation($value->thumbURL) : 'https://www.standardmedia.co.ke/images/pic.jpg' }}" class="card-img-top lazy" alt="{{ $value->title }}">
                                            </a>
                                            <a href="{{ $utils->assetLink("category", $value->categoryid, $utils->home->getCategoryName($value->categoryid)) }}">
                                                <p class="imgbtnhome">{{ $utils->home->getCategoryName($value->categoryid) }}</p>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="card-body categories-card bg-white px-2">
                                        <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                            <h5 class="card-title p-3">{{ $value->title }} </h5>
                                        </a>
                                        <ul class="list-group list-group-flush card-anchor">
                                            @else
                                                <li class="list-group-item px-0">
                                                    <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                                        {{ $value->title }}
                                                    </a>
                                                </li>
                                            @endif
                                            @php $i++; @endphp
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                    </div>
                </div>
                <div class="card bgtrans">
                    <h4 class="section-heading my-2 mx-md-0 mx-3">ENTERTAINMENT</h4>
                    <div class="card national mx-3 mx-md-0 home-categories-card">
                        @if(isset($entertainment))
                            @php $i = 1; @endphp
                            @foreach($entertainment as $value)
                                @if($i == 1)
                                    <div class="single-standard">
                                        <div class="standard-image">
                                            <a href="{{ $utils->assetLink("article", $value->id, $value->title, "sde") }}">
                                                <img src="{{ asset('assets/images/pic.jpg') }}" data-src="{{ $value->thumbURL != "" ? $utils->imageLocation($value->thumbURL, "sde") : 'https://www.standardmedia.co.ke/images/pic.jpg' }}" class="card-img-top lazy" alt="{{ $value->title }}">
                                            </a>
                                            <p class="imgbtnhome">Entertainment</p>
                                        </div>
                                    </div>
                                    <div class="card-body categories-card bg-white px-0">
                                        <a href="{{ $utils->assetLink("article", $value->id, $value->title, "sde") }}">
                                            <h5 class="card-title p-3">{{ $value->title }} </h5>
                                        </a>
                                        <ul class="list-group list-group-flush card-anchor">
                                            @else
                                                <li class="list-group-item px-0">
                                                    <a href="{{ $utils->assetLink("article", $value->id, $value->title , "sde") }}">
                                                        {{ $value->title }}
                                                    </a>
                                                </li>
                                            @endif
                                            @php $i++; @endphp
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                    </div>
                </div>
                <div class="card bgtrans">
                    <h4 class="section-heading my-2 mx-md-0 mx-3">SPORTS</h4>
                    <div class="card national mx-3 mx-md-0 home-categories-card">
                        @if(isset($sports))
                            @php $i = 1; @endphp
                            @foreach($sports as $value)
                                @if($i == 1)
                                    <div class="single-standard">
                                        <div class="standard-image">
                                            <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                                <img src="{{ asset('assets/images/pic.jpg') }}" data-src="{{ $value->thumbURL != "" ? $utils->imageLocation($value->thumbURL) : 'https://www.standardmedia.co.ke/images/pic.jpg' }}" class="card-img-top lazy" alt="{{ $value->title }}">
                                            </a>
                                            <a href="{{ $utils->assetLink("category", $value->categoryid, $utils->home->getCategoryName($value->categoryid)) }}">
                                                <p class="imgbtnhome">{{ $utils->home->getCategoryName($value->categoryid) }}</p>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="card-body categories-card bg-white px-2">
                                        <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                            <h5 class="card-title p-3">{{ $value->title }} </h5>
                                        </a>
                                        <ul class="list-group list-group-flush card-anchor">
                                @else
                                                <li class="list-group-item px-0">
                                                    <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                                        {{ $value->title }}
                                                    </a>
                                                </li>
                                @endif
                                @php $i++; @endphp
                                @endforeach
                                        </ul>
                                </div>
                        @endif
                    </div>
                </div>
            </div>
            <hr class="my-2">
        </div>
    </section>
    <!------------------------------- End of National, Politics, Entertainment & Sports Section ------------------------>

    <!------------------------------------------------ Slideshows ------------------------------------------------------>
    @if(isset($slideShows))
        <section>
            <div class="container py-2 my-1">
                <h4 class="section-heading my-2 mx-0">PICTURE POWER</h4>
                <div class="owl-carousel slideShow py-1">
                    @foreach($slideShows as $value)
                        <div class="card rounded-0 border-0 ">
                            <div class="single-standard">
                                {{--                        <a href="{{ $utils->assetLink("pictures", $value->id, $value->title) }}">--}}
                                <a href="#">
                                    <div style="height:180px; overflow:hidden;">
                                        <img src="{{ asset('assets/images/pic.jpg') }}" data-src="{{ $value->imgURL != "" ? $utils->imageLocation($value->imgURL) : 'https://www.standardmedia.co.ke/images/pic.jpg' }}"   alt="{{ $value->title }}" class="w-100 objectfit lazy">
                                    </div>
                                </a>
                            </div>
                            <div class="card-body p-3 bg-white" style="height:120px; overflow:hidden;">
                                <small class="text-muted">
                                    <span class="text-danger fa fa-calender"></span>{{ date('d',strtotime($value->publishdate)) }}<sup>{{ date('S',strtotime($value->publishdate))}}</sup> {{ date('F Y',strtotime($value->publishdate))}}
                                </small>
                                <div class="black mb-2">
                                    {{--                                <a class="text-dark"  href="{{ $utils->assetLink("pictures", $value->id, $value->title) }}">  --}}<a style="color: #000;" href="#">{{ $value->title}}</a>
                                </div>

                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    @endif
    <!--------------------------------------- End of Slideshows -------------------------------------------->
    <!---------------------------------------- World Section ----------------------------------------------->
    <section class="">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <h4 class="section-heading mb-2 mx-0">WORLD</h4>
                    <div class="row">
                        @if(isset($world))
                            @php $i = 1; @endphp
                            @foreach($world as $value)
                                <div class="col-md-4 mb-4 half">
                                    <div class="card national world-card">
                                        <div class="single-standard">
                                            <div class="standard-image">
                                                <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                                    <img src="{{ asset('assets/images/pic.jpg') }}" data-src="{{ $value->thumbURL != "" ? $utils->imageLocation($value->thumbURL) : 'https://www.standardmedia.co.ke/images/pic.jpg' }}" class="card-img-top lazy" alt="{{ $value->title }}">
                                                </a>
                                                <a href="{{ $utils->assetLink("category", $value->categoryid, $utils->home->getCategoryName($value->categoryid)) }}">
                                                    <p class="imgbtnhome">{{ $utils->home->getCategoryName($value->categoryid) }}</p>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="card-body bg-white">
                                            <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                                <p class="card-text black"> {{ $value->title }} </p>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card national p-2">
                        @if((new Jenssegers\Agent\Agent())->isDesktop())
                            <div class="ad-300-250 justify-content-center">
                                @include('Ads.desktop-ads.home_main_rightpanel_advert2')
                            </div>
                        @endif
                        @if((new \Jenssegers\Agent\Agent())->isMobile())
                            <div class="ad-300-250 justify-content-center">
                                @include('Ads.mobile-ads.home_mobile_advert7')
                            </div>
                        @endif
                    </div>
                    <h4 class="section-heading mb-0 mt-1 mx-0">CHECKPOINT</h4>
                    <div class="card ">
                        @php
                            $i = 1;
                        @endphp
                        @if(isset($checkpoint))
                            <div class="card-body ">
                                @foreach($checkpoint as $value)
                                    <div class="media my-2">
                                        <div class="single-standard">
                                            <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                                <img src="{{ asset('assets/images/pic.jpg') }}" data-src="{{ $value->thumbURL != "" ? $utils->imageLocation($value->thumbURL) : 'https://www.standardmedia.co.ke/images/pic.jpg' }}" class="mr-md-3 lazy" alt="{{ $value->title }}">
                                            </a>
                                        </div>
                                        <div class="media-body ml-3 ml-md-0 news-videos card-anchor">
                                            <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}"> {{ $value->title }} </a>
                                            <h4 class="my-1"><span class="videos-byline">{{ ucwords($value->author) }}</span></h4>
                                        </div>
                                    </div>
                                    <hr class="my-2">
                                @endforeach
                                @endif
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!---------------------------------------- End of World Section ----------------------------------------------->
@endsection
