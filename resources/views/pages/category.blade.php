@extends('layout.structure')
@section('content')
<script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "BreadcrumbList",
      "itemListElement": [{
        "@type": "ListItem",
        "position": 1,
        "name": "Home",
        "item": "{{ url('/') }}"
      },{
        "@type": "ListItem",
        "position": 2,
        "name": "{{ $utils->home->getCategoryName($id) }}",
        "item": "{{ $utils->assetLink("category", $id, $utils->home->getCategoryName($id)) }}"
      },{
        "@type": "ListItem",
        "position": 3,
        "name": "{{ $utils->home->getCategoryName($id) }}",
        "item": "{{ url()->current() }}"
      }]
    }
</script>
    <section id="standard" class="standard-area pt-100 homept mt-1">
        <div class="container mx-auto">
            <div class="offset-md-4 row my-2">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                @if ($message = Session::get('error'))
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
            </div>
            <div class="row">
                @if((new Jenssegers\Agent\Agent())->isDesktop())
                    @if($parentId == 4 || $id == 4)
                        <div class="ad-728-90 offset-md-2 col-md-8 mb-1">
                            @include('Ads.desktop-ads.business_main_top_inner_advert1')
                        </div>
                    @elseif($parentId == 5 || $id == 5)
                        <div class="ad-728-90 offset-md-2 col-md-8 mb-1">
                            @include('Ads.desktop-ads.world_top_advert')
                        </div>
                    @elseif($parentId == 9 || $id == 9)
                        <div class="ad-728-90 offset-md-2 col-md-8 mb-1">
                            @include('Ads.desktop-ads.lifestyle_main_top_inner_advert1')
                        </div>
                    @elseif($parentId == 41 || $id == 41)
                        <div class="ad-728-90 offset-md-2 col-md-8 mb-1">
                            @include('Ads.desktop-ads.health_article_top_advert')
                        </div>
                    @else
                        <div class="ad-728-90 offset-md-2 col-md-8 mb-1">
                            @include('Ads.desktop-ads.home_main_top_inner_advert1')
                        </div>
                    @endif
                @endif
            </div>
            <!-------------------------------- Breaking news ---------------------------------------->
            @if(isset($breaking) && ($breaking != null))
                @include('includes.breakingNews')
            @endif
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    @if((new Jenssegers\Agent\Agent())->isMobile())
                        <div class="ad-320-100 justify-content-center mb-1">
                            @if($parentId == 1 || $id == 1)
                                @include('Ads.desktop-ads.home_main_top_inner_advert1')
                            @elseif($parentId == 5 || $id == 5)
                                @include('Ads.desktop-ads.world_top_advert')
                            @elseif($parentId == 4 || $id == 4)
                                @include('Ads.desktop-ads.business_main_top_inner_advert1')
                            @elseif($parentId == 9 || $id == 9)
                                @include('Ads.desktop-ads.lifestyle_main_top_inner_advert1')
                            @elseif($parentId == 41 || $id == 41)
                                @include('Ads.desktop-ads.health_article_top_advert')
                            @else
                                @include('Ads.desktop-ads.home_main_top_inner_advert1')
                            @endif
                        </div>
                    @endif
                    <nav>
                        <ol class="breadcrumb" style="background: transparent !important;">
                            <li class="breadcrumb-item">
                                <a href="{{ url('/') }}"> Home </a>
                            </li>
                            @if($parentId)
                            <li class="breadcrumb-item">
                                <a href="{{ $utils->assetLink("category", $parentId, $utils->home->getCategoryName($parentId)) }}">
                                    {{ $utils->home->getCategoryName($parentId) }}
                                </a>
                            </li>
                            @endif
                            <li class="breadcrumb-item active">
                                <a href="{{  url()->current() }}">
                                    {{ $categoryName }}
                                </a>
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="row mt-2">
                        @if(isset($secondList))
                            @php $i = 1; @endphp
                            @foreach($secondList as $value)
                            <div class="col-md-6">
                                    <div class="wrapper bg-white  px-3 mb-4">
                                        <div class="single-standard row">
                                            <div class="standard-image">
                                                <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                                    <img src="{{ asset('assets/images/pic.jpg') }}" data-src="{{ $value->thumbURL != "" ? $utils->imageLocation($value->thumbURL) : 'https://www.standardmedia.co.ke/images/pic.jpg' }}" class="h-100 lazy m-0 p-0 px300" alt="{{ $value->title }}">
                                                </a>
                                            </div></div>
                                            <div class="standard-content  border-0 p-0 pb-3 p-2 bg-white">
                                                <div class="content p-0 pl-2">
                                                    <p class="smalltext py-2 px-1"><font class="redtext">By <a href="{{ $utils->assetLink("author", $value->author) }}">{{ $value->author }}</a></font> {{ $utils->assist->timeAgo($value->publishdate) }}</p>
                                                    <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                                        <h4 class="bigtext py-2">{{ $value->title }}</h4>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                @if($i == 2)
                                    @if((new Jenssegers\Agent\Agent())->isMobile())
                                        <div class="row my-2">
                                            <div class="col-sm-12">
                                                @if($parentId == 4 || $id == 4)
                                                    <div class="ad-300-250 justify-content-center mx-auto">
                                                        @include('Ads.desktop-ads.business_main_rightpanel_advert1')
                                                    </div>
                                                @elseif($parentId == 5 || $id == 5)
                                                    <div class="ad-300-250 justify-content-center mx-auto">
                                                        @include('Ads.desktop-ads.world_rightpanel_advert1')
                                                    </div>
                                                @elseif($parentId == 9 || $id == 9)
                                                    <div class="ad-300-250 justify-content-center mx-auto">
                                                        @include('Ads.desktop-ads.lifestyle_main_rightpanel_advert1')
                                                    </div>
                                                @elseif($parentId == 41 || $id == 41)
                                                    <div class="ad-300-250 justify-content-center mx-auto">
                                                        @include('Ads.desktop-ads.health_main_rightpanel_advert1')
                                                    </div>
                                                @else
                                                    <div class="ad-300-250 justify-content-center mx-auto">
                                                        @include('Ads.mobile-ads.home_mobile_advert1')
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    @endif
                                @endif
                                    </div>
                                @php $i++; @endphp
                            @endforeach
                        @endif
                   </div>
                    <div class="row">
                        @if((new Jenssegers\Agent\Agent())->isDesktop())
                            <div class="ad-article-page-desktop mx-auto ">
                                @include('Ads.desktop-ads.home_main_inner_middle_advert1')
                            </div>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    @if((new Jenssegers\Agent\Agent())->isDesktop())
                        @if($parentId == 4 || $id == 4)
                            <div class="ad-300-250 justify-content-center my-4 mt-md-0 mx-auto">
                                @include('Ads.desktop-ads.business_main_rightpanel_advert1')
                            </div>
                        @elseif($parentId == 5 || $id == 5)
                            <div class="ad-300-250 justify-content-center mx-auto">
                                @include('Ads.desktop-ads.world_rightpanel_advert1')
                            </div>
                        @elseif($parentId == 41 || $id == 41)
                            <div class="ad-300-250 justify-content-center mx-auto">
                                @include('Ads.desktop-ads.health_main_rightpanel_advert1')
                            </div>
                        @elseif($parentId == 9 || $id == 9)
                            <div class="ad-300-250 justify-content-center mx-auto">
                                @include('Ads.desktop-ads.lifestyle_main_rightpanel_advert1')
                            </div>
                        @else
                            <div class="ad-300-250 justify-content-center my-4 mt-md-0 mx-auto">
                                @include('Ads.desktop-ads.home_main_rightpanel_advert1')
                            </div>
                        @endif
                    @endif
                    <div class="card national mt-30 mb-3 p-md-2">
                        <div class="single-standard">
                            <h4 class="section-heading my-1 mx-0">MOST READ</h4>
                        </div>
                        @if(isset($mostRead))
                            <div class="card p-0">
                                <div class="card-body p-0">
                                    <div class="media popular-news">
                                        <ul class="list-group list-group-flush">
                                            @foreach($mostRead as  $value)
                                                <li class="list-group-item">
                                                    <h4 class='mb-2'><a href='{{ url('/') }}{{ $value->url }}'>{{ htmlspecialchars_decode($value->page, ENT_QUOTES) }} </a></h4>
                                                    <h6 class='my-1'><span class='by-line'>{{ $value->author }} </span></h6>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <hr class="my-2">
                                </div>
                            </div>
                        @endif
                    </div>
                        @if((new Jenssegers\Agent\Agent())->isDesktop())
                            @if($parentId == 4 || $id == 4)
                                <div class="ad-300-250 justify-content-center my-4 mt-md-0 mx-auto">
                                    @include('Ads.desktop-ads.business_main_rightpanel_advert2')
                                </div>
                            @elseif($parentId == 5 || $id == 5)
                                <div class="ad-300-250 justify-content-center">
                                    @include('Ads.desktop-ads.world_rightpanel_advert2')
                                </div>
                            @elseif($parentId == 41 || $id == 41)
                                <div class="ad-300-250 justify-content-center">
                                    @include('Ads.desktop-ads.health_main_rightpanel_advert2')
                                </div>
                            @elseif($parentId == 9 || $id == 9)
                                <div class="ad-300-250 justify-content-center">
                                    @include('Ads.desktop-ads.lifestyle_main_rightpanel_advert2')
                                </div>
                            @else
                                <div class="ad-300-250 justify-content-center my-4 mt-md-0 mx-auto">
                                    @include('Ads.desktop-ads.home_main_rightpanel_advert2')
                                </div>
                            @endif
                        @endif
                </div>
            </div>
            <hr class="my-4">
        </div>
        <!-- container -->
    </section>
    <!-------------------------------- The Standard Insider ---------------------------------------->
    <section class="">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @if((new Jenssegers\Agent\Agent())->isMobile())
                        <div class="row my-2">
                            <div class="col-sm-12">
                            @if($parentId == 4 || $id == 4)
                                <div class="ad-300-250 justify-content-center">
                                    @include('Ads.desktop-ads.business_main_rightpanel_advert2')
                                </div>
                            @elseif($parentId == 5 || $id == 5)
                                <div class="ad-300-250 justify-content-center">
                                    @include('Ads.desktop-ads.world_rightpanel_advert2')
                                </div>
                            @elseif($parentId == 9 || $id == 9)
                                <div class="ad-300-250 justify-content-center">
                                    @include('Ads.desktop-ads.lifestyle_main_rightpanel_advert2')
                                </div>
                            @elseif($parentId == 41 || $id == 41)
                                <div class="ad-300-250 justify-content-center">
                                    @include('Ads.desktop-ads.health_main_rightpanel_advert2')
                                </div>
                            @else
                                <div class="ad-300-250 justify-content-center">
                                    @include('Ads.mobile-ads.home_mobile_advert2')
                                </div>
                            @endif
                            </div>
                        </div>
                    @endif
                    <h4 class="section-heading my-2 mx-0">THE STANDARD INSIDER</h4>
                    <div class="card-deck insider">
                        @if(isset($insider))
                            @foreach($insider as $value)
                                    <div class="card premiumcontent mb-4">
                                        <div class="single-standard">
                                            <div class="standard-image">
                                                    <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                                        <img src="{{ asset('assets/images/pic.jpg') }}" data-src="{{ $value->thumbURL != "" ? $utils->imageLocation($value->thumbURL) : 'https://www.standardmedia.co.ke/images/pic.jpg' }}" class="card-img-top lazy" alt="{{ $value->title }}">
                                                    </a>
                                                <p class="premium"><i class="fa fa-lock"></i></p>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                                <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                                    <p class="card-text black">{{ $value->title }}</p>
                                                </a>
                                        </div>
                                    </div>
                            @endforeach
                        @endif
                    </div>

                </div>
            </div>
        </div>
    </section>

    <!-------------------------------- End of The Standard Insider ---------------------------------------->

    <!-------------------------------- Ad Big ---------------------------------------->
    <section class="d-none d-md-block d-lg-block my-4">
        <div class="container">
            <div class="ad">
                <div class="row py-5 mx-0">
                    @if((new Jenssegers\Agent\Agent())->isDesktop())
                        <div class="ad-728-90 offset-md-2 col-md-8 ">
                            @include('Ads.desktop-ads.home_main_advert2')
                        </div>
                    @endif
                    @if((new \Jenssegers\Agent\Agent())->isMobile())
                        <div class="ad-300-250 justify-content-center">
                            @include('Ads.mobile-ads.home_mobile_advert3')
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
    <!-------------------------------- End Big Ad---------------------------------------->

    <!-------------------------------- Picture Power ---------------------------------------->
    @if(isset($slideShows))
        <section>
            <div class="container py-2 my-1">
                <h4 class="section-heading my-2 mx-0">PICTURE POWER</h4>
                <div class="owl-carousel slideShow py-1">
                    @foreach($slideShows as $value)
                        <div class="card rounded-0 border-0 ">
                            <div class="single-standard">
                                {{--                        <a href="{{ $utils->assetLink("pictures", $value->id, $value->title) }}">--}}
                                <a href="#">
                                    <div style="height:180px; overflow:hidden;">
                                        <img src="{{ asset('assets/images/pic.jpg') }}" data-src="{{ $value->imgURL != "" ? $utils->imageLocation($value->imgURL) : 'https://www.standardmedia.co.ke/images/pic.jpg' }}"   alt="{{ $value->title }}" class="w-100 objectfit lazy">
                                    </div>
                                </a>
                            </div>
                            <div class="card-body p-3 bg-white" style="height:120px; overflow:hidden;">
                                <small class="text-muted">
                                    <span class="text-danger fa fa-calender"></span>{{ date('d',strtotime($value->publishdate)) }}<sup>{{ date('S',strtotime($value->publishdate))}}</sup> {{ date('F Y',strtotime($value->publishdate))}}
                                </small>
                                <div class="black mb-2">
                                    {{--                                <a class="text-dark"  href="{{ $utils->assetLink("pictures", $value->id, $value->title) }}">  --}}<a style="color: #000;" href="#">{{ $value->title}}</a>
                                </div>

                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    @endif

    <!-------------------------------- End of Picture Power ---------------------------------------->

    <!-------------------------------- More from Category ---------------------------------------->
    <section>
        <div class="container my-4">
            <div class="row">
                <div class="col-md-8 bg-white p-3">
                    @if((new Jenssegers\Agent\Agent())->isMobile())
                        <div class="row my-2">
                            <div class="col-sm-12">
                            @if($parentId == 4 || $id == 4)
                                <div class="ad-300-250 justify-content-center">
                                    @include('Ads.desktop-ads.business_main_rightpanel_advert3')
                                </div>
                            @elseif($parentId == 5 || $id == 5)
                                <div class="ad-300-250 justify-content-center">
                                    @include('Ads.desktop-ads.world_rightpanel_advert3')
                                </div>
                            @elseif($parentId == 9 || $id == 9)
                                <div class="ad-300-250 justify-content-center">
                                    @include('Ads.desktop-ads.lifestyle_main_rightpanel_advert3')
                                </div>
                            @elseif($parentId == 41 || $id == 41)
                                <div class="ad-300-250 justify-content-center">
                                    @include('Ads.desktop-ads.health_main_rightpanel_advert3')
                                </div>
                            @else
                                <div class="ad-300-250 justify-content-center">
                                    @include('Ads.mobile-ads.home_mobile_advert4')
                                </div>
                            @endif
                            </div>
                        </div>
                    @endif
                    @if(isset($lastList))
                        @foreach($lastList as $value)
                            <div class="card mb-3" id="post_{{ $value->id }}">
                                <div class="row no-gutters">
                                    <div class="col-md-6">
                                        <div class="single-standard">
                                            <div class="standard-image">
                                                <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                                    <img src="{{ asset('assets/images/pic.jpg') }}" data-src="{{ $value->thumbURL != "" ? $utils->imageLocation($value->thumbURL) : 'https://www.standardmedia.co.ke/images/pic.jpg' }}" class="card-img-top lazy" alt="{{ $value->title }}">
                                                </a>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="card-body">
                                            <a href="{{ $utils->assetLink("category", $value->categoryid, $utils->home->getCategoryName($value->categoryid)) }}">
                                                <p class="bigred">{{ $utils->home->getCategoryName($value->categoryid) }}</p>
                                            </a>
                                            <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                                <h5 class="card-title boldblack">{{ $value->title }}</h5>
                                                <p class="card-text hortext">{{ $value->summary }}</p>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif

                </div>
                <div class="col-md-4 cardhalf">
                    @if((new Jenssegers\Agent\Agent())->isMobile())
                        <div class="row my-2">
                            <div class="col-sm-12">
                            @if($parentId == 4 || $id == 4)
                                <div class="ad-300-250 justify-content-center">
                                    @include('Ads.desktop-ads.business_main_rightpanel_advert4')
                                </div>
                            @elseif($parentId == 5 || $id == 5)
                                <div class="ad-300-250 justify-content-center">
                                    @include('Ads.desktop-ads.world_rightpanel_advert4')
                                </div>
                            @elseif($parentId == 9 || $id == 9)
                                <div class="ad-300-250 justify-content-center">
                                    @include('Ads.desktop-ads.lifestyle_main_rightpanel_advert4')
                                </div>
                            @elseif($parentId == 41 || $id == 41)
                                <div class="ad-300-250 justify-content-center">
                                    @include('Ads.desktop-ads.health_main_rightpanel_advert4')
                                </div>
                            @else
                                <div class="ad-300-250 justify-content-center">
                                    @include('Ads.mobile-ads.home_mobile_advert5')
                                </div>
                            @endif
                            </div>
                        </div>
                    @endif
                    <h4 class="section-heading mb-5 mx-0 mb-2">CHECKPOINT</h4>

                    @if(isset($checkpoint))
                        @foreach($checkpoint as $value)

                            <div class="wrapper bg-white  px-3 mb-3">

                                <div class="single-standard row">
                                    <div class="standard-image">
                                        <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                            <img src="{{ asset('assets/images/pic.jpg') }}" data-src="{{ $value->thumbURL != "" ? $utils->imageLocation($value->thumbURL) : 'https://www.standardmedia.co.ke/images/pic.jpg' }}" class="px280 m-0 p-0  lazy" alt="{{ $value->title }}">
                                        </a>
                                    </div>
                                    <div class="standard-content border-0 p-0 pb-3 p-2 ">
                                        <div class="content p-0 pl-2">
                                            <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                                <h4 class="black py-2"> {{ $value->title }}</h4>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>

            </div>
        </div>
    </section>
    <section>
        <div class="container my-4">
            <div class="row cardhalf">
                @if(isset($allList))
                    @php $j = 1; @endphp
                    @foreach($allList as $value)
                        <div class="col-md-4 mb-4">
                            <div class="card national">
                                <div class="single-standard">
                                    <div class="standard-image">
                                        <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                            <img src="{{ asset('assets/images/pic.jpg') }}" data-src="{{ $value->thumbURL != "" ? $utils->imageLocation($value->thumbURL) : 'https://www.standardmedia.co.ke/images/pic.jpg' }}" class="card-img-top lazy" alt="{{ $value->title }}">
                                        </a>
                                    </div>
                                </div>
                                <div class="card-body bg-white card-national">
                                    <a href="">
                                        <a href="{{ $utils->assetLink("article", $value->id, $value->title) }}">
                                            <p class="card-text black"> {{ $value->title }}</p>
                                        </a>
                                    </a>
                                </div>
                            </div>
                        </div>
                        @if($i == 3)
                            @if((new Jenssegers\Agent\Agent())->isMobile())
                                <div class="row my-2">
                                   <div class="col-sm-12">
                                       @if($parentId == 4 || $id == 4)
                                           <div class="ad-300-250 justify-content-center">
                                               @include('Ads.desktop-ads.business_main_rightpanel_advert5')
                                           </div>
                                       @elseif($parentId == 5 || $id == 5)
                                           <div class="ad-300-250 justify-content-center">
                                               @include('Ads.desktop-ads.world_rightpanel_advert4')
                                           </div>
                                       @elseif($parentId == 9 || $id == 9)
                                           <div class="ad-300-250 justify-content-center">
                                               @include('Ads.desktop-ads.lifestyle_main_rightpanel_advert5')
                                           </div>
                                       @elseif($parentId == 41 || $id == 41)
                                           <div class="ad-300-250 justify-content-center">
                                               @include('Ads.desktop-ads.health_main_rightpanel_advert4')
                                           </div>
                                       @else
                                           <div class="ad-300-250 justify-content-center">
                                               @include('Ads.mobile-ads.home_mobile_advert6')
                                           </div>
                                       @endif
                                   </div>
                                </div>
                            @endif
                        @endif
                        @php $j++; @endphp
                    @endforeach
                @endif
            </div>
        </div>
    </section>
    <!-------------------------------- End of More from Category ---------------------------------------->
@endsection
