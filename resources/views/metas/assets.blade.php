    <meta name="application-name" content="Standard Digital">
    <link rel="apple-touch-icon" sizes="57x57" href="https://www.standardmedia.co.ke/assets/apple-touch-icon-192x192">
    <link rel="dns-prefetch" href="https://www.gstatic.com" >
    <link rel="dns-prefetch" href="https://tt.onthe.io" >
    <link rel="canonical" href="{{ url()->current() }}">
    <meta name="robots" content="index,follow" />
    <meta name="copyright" content="The Standard Group" />
    <meta name="online" content="online@standardmedia.co.ke" />
    <meta http-equiv="refresh" content="650">
    <meta name="google-translate-customization" content="9d90f29fa7bea403-55a95e98e906d1f5-gca3f4862da4da582-17" />
    <meta name="msvalidate.01" content="895B0BFD6A675ED63FA74B3DDCFB918B" />
    <meta name="keywords"  content="{{ $keywords ?? "Breaking news, news online, Kenya news, world news, Standard Digital, news video, KTN Home, weather, business, money, politics, law, technology, entertainment, education, travel, health, special reports, autos, KTN News" }}" />
    <meta name="author"  content="{{ $author ?? 'Standard Digital' }}" />
    <meta name="description" content="{{ $description ?? "Kenya's number one website that delivers realtime news across the globe. The top headlines covers politics, citizen journalism, culture, business, sports and entertainment. Standard Digital - Your Gateway" }}"/>
    <link rel="image_src" href="{{ $image ?? asset('assets/images/std.png') }}">
    <meta name="twitter:card" content="summary_large_image"/>
    <meta name="twitter:site" content="@StandardKenya"/>
    <meta name="twitter:creator" content="@StandardKenya"/>
    <meta property="twitter:title" content="{{ $title ?? "The Standard - Breaking News, Kenya News, World News and Videos" }}" />
    <meta property="twitter:description" content="{{ $description ?? "Kenya's number one website that delivers realtime news across the globe. The top headlines covers politics, citizen journalism, culture, business, sports and entertainment. Standard Digital - Your Gateway" }}" />
    <meta property="twitter:url" content="{{ url()->current() }}" />
    <meta property="twitter:image" content="{{ $image ?? asset('assets/images/std.png') }}" />
    <meta property="og:type" content="website" />
    <meta property="fb:app_id" content="118898458656" />
    <meta property="og:title" content="{{ $title ?? "The Standard - Breaking News, Kenya News, World News and Videos" }}" />
    <meta property="og:description" content="{{ $description ?? "Kenya's number one website that delivers realtime news across the globe. The top headlines covers politics, citizen journalism, culture, business, sports and entertainment. Standard Digital - Your Gateway" }}" />
    <meta property="og:site_name" content="The Standard" />
    <meta property="og:url" content="{{ url()->current() }}" >
    <meta property="og:image" content="{{ $image ?? asset('assets/images/std.png') }}"/>
    <meta property="og:image:secure_url" content="{{ $image ?? asset('assets/images/std.png') }}" />
    <meta name="site-live" content="02:00:45 05-04-2020" />
    <link rel="manifest" href="{{ asset('assets/manifest.json')}}" />
