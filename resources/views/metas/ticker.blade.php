<div class="bn-breaking-news" id="breakingNewsTicker">
    <div class="bn-label">BREAKING NEWS</div>
    <div class="bn-news">
        <ul>
            <li><a href="#">1.1. There many variations of passages of Lorem Ipsum available</a></li>
            <li><a href="#">1.2. Ipsum is simply dummy text of the printing and typesetting industry</a></li>
            <li><a href="#">1.3. Lorem Ipsum is simply dummy text of the printing and typesetting industry</a></li>
            <li><a href="#">1.4. Lorem simply dummy text of the printing and typesetting industry</a></li>
            <li><a href="#">1.5. Ipsum is simply dummy of the printing and typesetting industry</a></li>
            <li><a href="#">1.6. Lorem Ipsum simply dummy text of the printing and typesetting industry</a></li>
            <li><a href="#">1.7. Ipsum is simply dummy text of the printing typesetting industry</a></li>
        </ul>
    </div>
    <div class="bn-controls">
        <button><span class="bn-arrow bn-prev"></span></button>
        <button><span class="bn-action"></span></button>
        <button><span class="bn-arrow bn-next"></span></button>
    </div>
</div>
