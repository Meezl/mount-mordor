<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="theme-color" content="#ee2e25" />
    <title>{{ $title ?? "The Standard - Breaking News, Kenya News, World News and Videos" }}: The Standard</title>
    <link rel="shortcut icon" href="https://www.standardmedia.co.ke/images/std.png" type="image/png">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/index.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" integrity="sha256-UhQQ4fxEeABh4JrcmAJ1+16id/1dnlOEVCFOxDef9Lw=" crossorigin="anonymous" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
        @include('metas.assets')
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    @if((new \Jenssegers\Agent\Agent())->isMobile())
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({
                google_ad_client: "ca-pub-2204615711705377",
                enable_page_level_ads: true
            });
        </script>
    @elseif((new \Jenssegers\Agent\Agent())->isDesktop() && $page !== "home")
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({
                google_ad_client: "ca-pub-2204615711705377",
                enable_page_level_ads: true
            });
        </script>
    @endif
    @if($page === "article")
    @php $link = str_replace("/article/","/mobile/amp/article/", url()->current()); @endphp
    <link rel="amphtml" href="{{ $link }}" />';
    @endif
    <!-- IO Tags --->
    @if(($page !== "article") && ($page !== "home"))
        <script>
            window._io_config = window._io_config || {};
            window._io_config["0.2.0"] = window._io_config["0.2.0"] || [];
            window._io_config["0.2.0"].push({
                page_url: "{{ url()->current() }}",
                page_url_canonical: "{{ url()->current() }}",
                page_title: "{{ $title ?? "" }}",
                page_type: "default",
                page_language: "en"
            });
        </script>
    @endif
    @if($page === 'home')
        <script>
            window._io_config = window._io_config || {};
            window._io_config["0.2.0"] = window._io_config["0.2.0"] || [];
            window._io_config["0.2.0"].push({
                page_url: "{{ url()->current() }}",
                page_url_canonical: "{{ url()->current() }}",
                page_title: "{{ $title ?? "The Standard - Breaking News, Kenya News, World News and Videos"  }}",
                page_type: "main",
                page_language: "en"
            });
        </script>
    @endif
    <!-- IO Tags End --->
    <!-- Google Ads Script --->
    @if((new \Jenssegers\Agent\Agent())->isMobile())
        @if($page == "home")
            @include('Ads.pages-ads.mobile.home-page-mobile-ads')
        @elseif($page == "category")
            @include('Ads.pages-ads.mobile.category-page-mobile-ads')
        @elseif($page == "article")
            @include('Ads.pages-ads.mobile.article-page-mobile-ads')
        @elseif($page == "author")
            @include('Ads.pages-ads.mobile.author-page-mobile-ads')
        @elseif($page == "search")
            @include('Ads.pages-ads.mobile.author-page-mobile-ads')
        @elseif($page == "topic")
            @include('Ads.pages-ads.mobile.topic-page-mobile-ads')
        @endif
    @elseif((new \Jenssegers\Agent\Agent())->isDesktop())
        @if($page == "home")
            @include('Ads.pages-ads.desktop.home-page-ads')
        @elseif($page == "category")
            @include('Ads.pages-ads.desktop.category-page-ads')
        @elseif($page == "article")
            @include('Ads.pages-ads.desktop.article-page-ads')
        @elseif($page == "author")
            @include('Ads.pages-ads.desktop.author-page-ads')
        @elseif($page == "topic")
            @include('Ads.pages-ads.desktop.topic-page-ads')
        @elseif($page == "search")
            @include('Ads.pages-ads.desktop.topic-page-ads')
        @endif
    @endif
    <!-- Google Ads Script End --->
    @include('metas.schema')
    @include('metas.analytics')
    <!-- Facebook Script --->
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.1&appId=1601457996848009&autoLogAppEvents=1';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- Facebook Script End  --->
</head>

<body>
@include('metas.blukai')
