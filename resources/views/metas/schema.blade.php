<script type="application/ld+json">
        {
          "@context": "http://schema.org",
          "@type": "WebSite",
          "url": "{{ url()->current() }}",
          "potentialAction": {
            "@type": "SearchAction",
            "target": "{{ url('search') }}?q={search_term_string}",
            "query-input": "required name=search_term_string"
          }
        }
    </script>
<script type="application/ld+json">
        {
              "@context": "http://schema.org",
              "@type": "Organization",
              "url": "{{ url('/') }}",
              "logo": "{{ $logo ?? asset('assets/images/sglogo.png') }}"
        }
    </script>
<script type="application/ld+json">
        {
          "@context": "http://schema.org",
          "@type": "Person",
          "name": "Standard Digital - Your Gateway",
          "url": "{{ url('/') }}",
          "sameAs": [
                        "https://www.facebook.com/standardkenya/",
                        "https://www.instagram.com/standardkenya",
                        "https://ke.linkedin.com/company/standard-media-group-plc",
                        "https://plus.google.com/+standarddigital",
                        "https://twitter.com/StandardKenya"
                    ]
        }
</script>
