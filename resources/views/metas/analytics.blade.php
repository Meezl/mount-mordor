<!---------------------------- GA----------------------------->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-9511843-1', 'auto', {'useAmpClientId': true});
   @if(isset($page))
        @if( $page  == "article")
            ga('set',{'articleCategory': '{{ $utils->home->getCategoryName($article->categoryid)}}'});
        @endif
    @endif
    ga('send', 'pageview');
</script>
<!---------------------------- End of GA ----------------------------->

<!---------------------------- IO Code ----------------------------->
<script async src="https://cdn.onthe.io/io.js/cQygVOSM0DIS"></script>
<style>
    .quick-survey,.quick-survey:visited{
        z-index: 10000;
        position:fixed;
        background: #ddd;
        right:0;
        bottom: 0;
        color:#0FDA31 !important;
        text-decoration: none;
        padding: 0.1rem 0.3rem;
    }
</style>
@if(isset($page))
    @if(($page !== "article") && ($page !== "home"))
        <script>
            window._io_config = window._io_config || {};
            window._io_config["0.2.0"] = window._io_config["0.2.0"] || [];
            window._io_config["0.2.0"].push({
                page_url: "{{ url()->current() }}",
                page_url_canonical: "{{ url()->current() }}",
                page_title: "{{ $title ?? "The Standard - Breaking News, Kenya News, World News and Videos" }}",
                page_type: "default",
                page_language: "en"
            });
        </script>
    @endif
    @if($page == 'home')
        <script>
            window._io_config = window._io_config || {};
            window._io_config["0.2.0"] = window._io_config["0.2.0"] || [];
            window._io_config["0.2.0"].push({
                page_url: "{{ url()->current() }}",
                page_url_canonical: "{{ url()->current() }}",
                page_title: "{{ $title ?? "The Standard - Breaking News, Kenya News, World News and Videos" }}",
                page_type: "main",
                page_language: "en",
            });
        </script>
    @endif
@endif
<!---------------------------- End of IO Code ----------------------------->

<!--------------------------- Facebook Pixel Code ------------------------->
<script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '2092594837535172');
    fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=2092594837535172&ev=PageView&noscript=1"/></noscript>
<!------------------------- End Facebook Pixel Code ----------------------->

