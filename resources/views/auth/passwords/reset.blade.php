@include('auth.auth-meta')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-sm-12 col-md-6">
            <div class="modal-content mt-5" role="document">
                <div class="modal-body">
                    <div class="text-center auth-logo mb-1">
                        <a href="{{ url('/') }}">
                            <img src="https://www.standardmedia.co.ke/flash/epaper/logo.png" alt="Standard Digital Logo">
                        </a>
                    </div>
                    <div class="form-title text-center">
                        <h4>Forgot Password</h4>
                    </div>
                    <div class="row my-2">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif


                        @if ($message = Session::get('error'))
                            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif

                    </div>
                    <div class="d-flex flex-column text-center">
                        <form method="post" action="{{ url('reset-password') }}">
                            @csrf
                            <div class="form-group my-2">
                                <input type="email" required class="form-control" name="email" id="email1" placeholder="Your email address...">
                            </div>
                            <input type="hidden" class="form-control" name="redirecturl" value="{{ $url = $_GET['returnurl']}} ">
                            <button type="submit" class="btn btn-info btn-block btn-round">Send Password Reset Link</button>
                        </form>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <div class="signup-section">A member yet? <a href="{{ url('subscription/sign-in?returnurl=') }}{{ $url }}" data-dismiss="modal" data-toggle="modal"
                                                                     data-target="#regModal" class="register text-info"> Login</a>.</div>
                    <div class="signup-section"><a href="{{ url('subscription/sign-up?returnurl=') }}{{ $url }}" data-dismiss="modal" data-toggle="modal" data-target="#forgotModal" class="register text-info"> Register</a>.</div>
                </div>
            </div>
        </div>
        </div>
</div>
