@include('auth.auth-meta')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="modal-content mt-5" role="document">
                <div class="modal-body">
                    <div class="text-center auth-logo mb-1">
                        <a href="{{ url('/') }}">
                            <img src="https://www.standardmedia.co.ke/flash/epaper/logo.png" alt="Standard Digital Logo">
                        </a>
                    </div>
                    <div class="form-title text-center">
                        <h4>Register</h4>
                    </div>
                    <div class="row my-2">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif


                        @if ($message = Session::get('error'))
                            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif

                    </div>
                    <div class="d-flex flex-column text-center">
                        <form method="post" action="{{ route('register') }}">
                            @csrf
                            <div class="form-group my-2">
                                <input type="text" required class="form-control" name="name" id="name"
                                       placeholder="Enter your full name...">
                            </div>
                            <div class="form-group my-2">
                                <input type="email" required class="form-control" name="email" id="email"
                                       placeholder="Enter your email address...">
                            </div>
                            <div class="form-group my-2">
                                <input type="text" class="form-control" name="phone" id="phone"
                                       placeholder="Enter your phone number...">
                            </div>
                            <div class="form-group my-2">
                                <input type="password" required class="form-control" name="password" id="password"
                                       placeholder="Enter your password...">
                            </div>
                            <div class="form-group my-2">
                                <input type="password" required class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Confirm your password...">
                            </div>
                            <input type="hidden" class="form-control" name="redirecturl" value="{{ $url = $_GET['returnurl']}} ">
                            <button type="submit" class="btn btn-info btn-block btn-round">Register</button>
                        </form>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <div class="row">
                        <div class="signup-section">I have an account? <a href="{{ url('subscription/sign-in?returnurl=') }}{{ $url }}" data-dismiss="modal" data-toggle="modal" data-target="#loginModal" class="login text-info"> Sign In</a>.</div>
                    </div>
                    <div class="row">
                        <div class="signup-section"> <a href="{{ url('subscription/reset-password?returnurl=') }}{{ $url }}" data-dismiss="modal" data-toggle="modal" data-target="#loginModal" class="login text-info">Forgot password?</a>.</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
