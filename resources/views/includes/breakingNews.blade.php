<!-------------------------------- Breaking news ---------------------------------------->
<div class="row breaking mt-4 mx-0">
    <div class="card">
        <div class="card-header bg-red text-center text-white">
            <h3 class="text-white anim">BREAKING NEWS!</h3>
        </div>
        <div class="card-body px-5">
            <div class="card mb-3" >
                <div class="row no-gutters">
                    <div class="col-md-3"><div class="single-standard">
                            <div class="standard-image">
                                <a href="{{ $utils->assetLink("article", $breaking->id, $breaking->title) }}"><img src="{{ asset('assets/images/pic.jpg') }}" data-src="{{ $utils->imageLocation($breaking->thumbURL) }}" class="card-img lazy" alt="{{ $breaking->title }}"></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="card-body">
                            <a href="{{ $utils->assetLink("article", $breaking->id, $breaking->title) }}"><h2 class="card-title">{{ $breaking->title }}</h2></a>
                            <a href="{{ $utils->assetLink("article", $breaking->id, $breaking->title) }}"><p class="card-text"> {{ $breaking->long_title }}</p></a>
                            <p class="card-text"><a href="{{ $utils->assetLink("article", $breaking->id, $breaking->title) }}"><small class="text-muted float-right">READ FULL STORY</small></a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
