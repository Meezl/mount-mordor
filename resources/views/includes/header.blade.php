@include('metas.meta')

<!--search-->
<div id="myOverlay" class="overlay">
    <span class="closebtn" onclick="closeSearch()" title="Close Overlay">×</span>
    <div class="overlay-content">
        <form action="{{ url('/search') }}" method="get">
            @csrf
            <input type="text" placeholder="Search The Standard..." name="q">
            <button type="submit"><i class="fa fa-search"></i></button>
        </form>
    </div>
</div>
<!--search-->
<header class="header-area">
    <div id="mySidenav" class="sidenav">
        <a href="javascript:void(0)" class="closebtn border-0" onclick="closeNav()">&times;</a>
        <a class="navheading">Digital News</a>
        <a href="https://www.standardmedia.co.ke/videos/">Videos</a>
        <a href="{{ url('category/7/opinion') }}">Opinions</a>
        <a href="{{ url('category/52/cartoons')}}">Cartoons</a>
        <a href="{{ url('category/56/education')}}">Education</a>
        <a href="https://www.standardmedia.co.ke/ureport">U-Report</a>
        <a target="_blank" href="https://newsstand.standardmedia.co.ke/?utm_source=standard&utm_medium=webiste">E-Paper</a>
        <a class="navheading">Lifestyle & Entertainment</a>
        <a target="_blank" href="https://thenairobian.ke/">Nairobian</a>
        <a target="_blank" href="https://www.sde.ke/">SDE</a>
        <a target="_blank" href="https://www.standardmedia.co.ke/evewoman/?utm_source=standard&utm_medium=webiste">Eve Woman</a>
        <a target="_blank" href="https://www.travelog.ke/?utm_source=standard&utm_medium=webiste">Travelog</a>
        <a class="navheading">TV Stations</a>
        <a href="https://www.standardmedia.co.ke/ktn?utm_source=standard&utm_medium=webiste">KTN Home</a>
        <a href="https://www.standardmedia.co.ke/ktnnews?utm_source=standard&utm_medium=webiste">KTN News</a>
        <a href="http://www.btvkenya.ke/?utm_source=standard&utm_medium=webiste">BTV</a>
        <a href="https://www.farmers.co.ke/videolist?utm_source=standard&utm_medium=webiste">KTN Farmers TV</a>
        <a class="navheading">Radio Stations</a>
        <a href="https://www.standardmedia.co.ke/radiomaisha">Radio Maisha</a>
        <a href="https://www.spicefm.co.ke?utm_source=standard&utm_medium=webiste">Spice FM</a>
        <a href="https://www.vybezradio.co.ke?utm_source=standard&utm_medium=webiste">Vybez Radio</a>
        <a class="navheading">Enterprise</a>
        <a target="_blank" href="http://vas.standardmedia.co.ke/?utm_source=standard&utm_medium=webiste">VAS</a>
        <a target="_blank" href="https://tutorsoma.standardmedia.co.ke/?utm_source=standard&utm_medium=webiste">E-Learning</a>
        <a target="_blank" href="https://digger.co.ke/?utm_source=standard&utm_medium=webiste">Digger Classified</a>
        <a class="navheading">The Standard Group</a>
        <a href="https://www.standardmedia.co.ke/corporate">Corporate</a>
        <a href="https://www.standardgroup.co.ke/contact-us">Contact Us</a>
        <a href="https://www.standardmedia.co.ke/ratecard/rate_card.pdf">Rate Card</a>
        <a href="https://www.standardmedia.co.ke/recruitment">Vacancies</a>
        <a href="https://portal.standardmedia.co.ke/dcx_sg">DCX</a>
        <a href="https://portal.standardmedia.co.ke/omportal">O.M Portal</a>
        <a href="https://smtp.standardmedia.co.ke/owa">Corporate Email</a>
        <a href="https://rms.standardmedia.co.ke:73/">RMS</a>
    </div>
    <div id="mySidenavMob" class="sidenav sidenavMob">
        @if(Session::get('user') != null)
            <a href="#" class="profile"><i class="fa fa-user-circle-o"></i> Hi {{ Session::get('user')->name }}</a>
        @endif
        <a href="javascript:void(0)" class="closebtn border-0" onclick="closeNavMob()">&times;</a>
                 <ul>
                        <li class="nav-item">
                            <a class="nav-link navheading sectionborder" onclick="openNav()"><i class="fa fa-bars text-white"></i> MORE </a>
                        </li>
                        <li class="nav-item @if(url()->current() == url('/') ) active @endif">
                            <a class="nav-link d-none d-md-block d-lg-block " href="{{ url('/') }}">HOME</a>
                        </li>
                        <li class="nav-item @if(url()->current() == url('search') ) active @endif d-none d-md-block d-lg-block">
                            <a class="nav-link" href="{{ url('search') }}"><i class="fa fa-search"></i></a>
                        </li>
                        <li class="nav-item @if(url()->current() == url('category/1/national') ) active @endif">
                            <a class="nav-link" href="{{ url('category/1/national') }}">NATIONAL</a>
                        </li>
                        <li class="nav-item @if(url()->current() == url('category/5/world') ) active @endif">
                            <a class="nav-link" href="{{ url('category/5/world') }}">WORLD</a>
                        </li>
                        <li class="nav-item @if(url()->current() == url('business/category/4/economy') ) active @endif">
                            <a class="nav-link" href="{{ url('business/category/4/economy') }}">ECONOMY</a>
                        </li>
                        <li class="nav-item @if(url()->current() == url('category/3/politics') ) active @endif">
                            <a class="nav-link" href="{{ url('category/3/politics') }}">POLITICS</a>
                        </li>
                         <li class="nav-item @if(url()->current() == url('topic/coronavirus') ) active @endif">
                             <a class="nav-link" href="{{ url('topic/coronavirus') }}">COVID-19</a>
                         </li>
                         <li class="nav-item @if(url()->current() == url('sports/category/6/sports') ) active @endif">
                             <a class="nav-link" href="{{ url('sports/category/6/sports') }}">SPORTS</a>
                         </li>
                        <li class="nav-item">
                            <a class="nav-link" target="_blank" href="https://sde.co.ke">ENTERTAINMENT</a>
                        </li>
                        <li class="nav-item">
                            @if(Session::get('user') != null)
                                <a class="nav-link insibg" href="{{ url('category/486/the-standard-insider') }}">THE INSIDER</a>
                            @else
                                <a class="nav-link" data-scroll-nav="0" class="nav-link sectionborder" data-toggle="modal" data-dismiss="modal" data-target="#loginModal" href="#">THE INSIDER</a>
                            @endif
                        </li>

                        @if(Session::get('user') != null)
                            <li class="nav-item dropdown @if(Session::get('user') != null) {{ 'active' }} @else {{ '' }} @endif">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{Session::get('user')->name}} <span class="caret"></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @else
                            <li class="nav-item login">
                                <a data-scroll-nav="0" class="nav-link sectionborder" data-toggle="modal" data-dismiss="modal" data-target="#loginModal" href="#">Login</a>
                            </li>
                        @endif
                        <li class="nav-item ctabg ">
                            <a class="nav-link" target="_blank" href="https://newsstand.standardmedia.co.ke/">E-Paper @Ksh.10 </a>
                        </li>
                    </ul>
    </div>
    <ul class="navbar navbar-expand-md none">
        <li class="collapse navbar-collapse flex-column ">
            <a class="navbar-brand text-center" href="{{ url('/') }}">
                <img src="{{ asset('assets/images/logo.png') }}" alt="Standardmedia logo" id="myImagese">
            </a>
        </li>
    </ul>

    <div class="pos-f-t">
        <nav class="navbar navbar-light bg-light sticky-top px-3 block">

            <a class="navbar-brand text-center" href="{{ url('/') }}">
                <img src="{{ asset('assets/images/logo.png')}}"
                     alt="Standardmedia logo" class="w140">
            </a>
            <a class="nav-link mr-3 ml-auto d-lg-none d-block" onclick="openSearch()"><i
                    class="fa fa-search mt-1"></i></a>
            <button class="navbar-toggler" type="button" onclick="openNavMob()">
                <i class="fa fa-bars"></i>
            </button>
        </nav>
    </div>
    <div class="row">
        <div class="col-lg-12 none">
            <nav class="navbar navbar-expand-md text-center navbar-dark mx-3 bg-white sticky-top py-md-0">
                <a class="navbar-brand text-center d-none d-md-block d-lg-block" href="{{ url('/') }}">
                    <img src="{{ asset('assets/images/logo.png') }}" alt="Standardmedia logo" id="myImage" class="myimagese">
                </a>
                <a class="navbar-brand text-center" href="{{ url('/') }}">
                    <img src="{{ asset('assets/images/logo.png') }}" alt="Standardmedia logo" class="w140 d-block d-lg-none d-md-none">
                </a>
                <div class="navbar-toggler-right d-flex">
                    <a class="nav-link d-md-none d-lg-none d-block"  onclick="openSearch()"><i class="fa fa-search mt-1"></i></a>
                    <div class="humburger-button">
                        <button type="button" class="mobnav" onclick="openNavMob()">
                            <span class="navbar-toggler-icon"><i class="fa fa-bars"></i> </span>
                        </button>
                    </div>
                </div>
                <div class="collapse navbar-collapse flex-column " id="navbar">
                    <ul class="navbar-nav justify-content-around w-100 pl-3">
                        <li class="nav-item">
                            <a class="nav-link sectionborder" onclick="openNav()"><i class="fa fa-bars"></i> MORE</a>
                        </li>
                        <li class="nav-item @if(url()->current() == url('/') ) active @endif">
                            <a class="nav-link d-none d-md-block d-lg-block " href="{{ url('/') }}"><i class="fa fa-home"></i></a>
                        </li>
                        <li class="nav-item @if(url()->current() == url('search') ) active @endif d-none d-md-block d-lg-block">
                            <a class="nav-link" onclick="openSearch()"><i class="fa fa-search"></i></a>
                        </li>
                        <li class="nav-item @if(url()->current() == url('category/1/national') ) active @endif">
                            <a class="nav-link" href="{{ url('category/1/national') }}">NATIONAL</a>
                        </li>
                        <li class="nav-item @if(url()->current() == url('category/5/world') ) active @endif">
                            <a class="nav-link" href="{{ url('category/5/world') }}">WORLD</a>
                        </li>
                        <li class="nav-item @if(url()->current() == url('category/4/economy') ) active @endif">
                            <a class="nav-link" href="{{ url('category/4/economy') }}">ECONOMY</a>
                        </li>
                        <li class="nav-item @if(url()->current() == url('category/3/politics') ) active @endif">
                            <a class="nav-link" href="{{ url('category/3/politics') }}">POLITICS</a>
                        </li>
                        <li class="nav-item @if(url()->current() == url('topic/coronavirus') ) active @endif">
                            <a class="nav-link" href="{{ url('topic/coronavirus') }}">COVID-19</a>
                        </li>
                        <li class="nav-item @if(url()->current() == url('sports/category/6/sports') ) active @endif">
                            <a class="nav-link" href="{{ url('sports/category/6/sports') }}">SPORTS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" target="_blank" href="https://sde.co.ke">ENTERTAINMENT</a>
                        </li>
                        <li class="nav-item insibg text-dark">
                            @if(Session::get('user') != null)
                                <a class="nav-link" href="{{ url('category/486/the-standard-insider') }}">THE INSIDER</a>
                            @else
                                <a class="nav-link" data-scroll-nav="0" class="nav-link sectionborder" data-toggle="modal" data-dismiss="modal" data-target="#loginModal" href="#">THE INSIDER</a>
                            @endif
                        </li>

                        @if(Session::get('user') != null)
                            <li class="nav-item dropdown @if(Session::get('user') != null) {{ 'active' }} @else {{ '' }} @endif">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{Session::get('user')->name}} <span class="caret"></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @else
                            <li class="nav-item login">
                                <a data-scroll-nav="0" class="nav-link sectionborder" data-toggle="modal" data-dismiss="modal" data-target="#loginModal" href="#">Login</a>
                            </li>
                        @endif
                        <li class="nav-item ctabg ">
                            <a class="nav-link" target="_blank" href="https://newsstand.standardmedia.co.ke/"> E-Paper @Ksh.10 </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</header>
