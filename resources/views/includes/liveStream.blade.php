<div class="row breaking mt-4 mx-0 justify-content-center">
    <div class="card">
        <div class="card-header bg-red text-center text-white">
            <h3 class="text-white anim">HAPPENING NOW!</h3>
        </div>
        <div class="card-body px-2 px-md-5">
            <div class="card mb-3">
                <div class="row no-gutters">
                    <div class="col-md-7">
                        <div class="single-standard">
                            <div class="standard-image">
                                <iframe frameborder="0" width="800" height="500" src="https://www.youtube.com/embed/{{ $live->youtubelink }}/?rel=0&autoplay=1" allowfullscreen="" allow="autoplay"></iframe>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="card-body">
                            <h3 class="card-title">{{ $live->title }}</h3>
                            <p class="card-text"><small class="text-muted">LIVE <i class="fa fa-circle fa-fw live"></i></small></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
