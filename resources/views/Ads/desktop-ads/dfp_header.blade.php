<script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
<script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
<script>
  var googletag = googletag || {};
  window.googletag = window.googletag || {cmd: []};
  googletag.cmd = googletag.cmd || [];
</script>

<script>
@if($page == "home" || $page == "category" || $page == 'author' || $page == 'topic' || $page == 'search')
    googletag.cmd.push(function() {
         var leaderboardMapping = googletag.sizeMapping().addSize([970,90],[728,90]).build();
         var bannerMapping = googletag.sizeMapping().addSize([336,280], [300, 250]).addSize([250, 250], [320, 100],[320, 50]).build();
         googletag.defineSlot('/24409412/Roadblock_700x300', [700, 300], 'div-gpt-ad-1501160382799-0').addService(googletag.pubads());
         googletag.defineSlot('/24409412/home_main_advert1', [728, 90], 'div-gpt-ad-1486010480522-0').addService(googletag.pubads());
         googletag.defineSlot('/24409412/home_main_rightpanel_advert1', [300, 250], 'div-gpt-ad-1400140713631-0').addService(googletag.pubads());
         googletag.defineSlot('/24409412/home_main_advert2', [728, 90], 'div-gpt-ad-1486968612976-0').addService(googletag.pubads());
         googletag.defineSlot('/24409412/home_main_advert3', [728, 90], 'div-gpt-ad-1554182224906-0').addService(googletag.pubads());
         googletag.defineSlot('/24409412/home_main_rightpanel_advert2', [300, 250], 'div-gpt-ad-1486627050053-0').addService(googletag.pubads());
         googletag.defineSlot('/24409412/home_main_rightpanel_advert3', [300, 250], 'div-gpt-ad-1582865963023-0').addService(googletag.pubads());
         googletag.pubads().enableSingleRequest();
         googletag.enableServices();
    });
@endif
@if($page == "article" || $page == "category")
    @if($parentId == 4 || $catId == 4)
        googletag.cmd.push(function() {
            var leaderboardMapping = googletag.sizeMapping().addSize([970,90],[728,90]).build();
            var bannerMapping = googletag.sizeMapping().addSize([336,280], [300, 250]).addSize([250, 250], [320, 100],[320, 50]).build();
            googletag.defineSlot('/24409412/Roadblock_700x300', [700, 300], 'div-gpt-ad-1501160382799-0').addService(googletag.pubads());
            googletag.defineSlot('/24409412/business_home_top_728x90', [728, 90], 'div-gpt-ad-1502950638611-0').addService(googletag.pubads());
            googletag.defineSlot('/24409412/business_home_728x90_top_ad', [728, 90], 'div-gpt-ad-1448870249423-0').addService(googletag.pubads());
            googletag.defineSlot('/24409412/article_video_1x1', [1, 1], 'div-gpt-ad-1584621281588-0').addService(googletag.pubads());
            googletag.defineSlot('/24409412/business_rightpanel_advert1', [300, 250], 'div-gpt-ad-1486031230604-0').addService(googletag.pubads());
            googletag.defineSlot('/24409412/home_main_inner_middle_advert1', [728, 90], 'div-gpt-ad-1498022554617-0').addService(googletag.pubads());
            googletag.defineSlot('/24409412/business_rightpanel_advert2', [300, 250], 'div-gpt-ad-1486031093371-0').addService(googletag.pubads());
            googletag.defineSlot('/24409412/business_rightpanel_advert3', [300, 250], 'div-gpt-ad-1468478909150-0').addService(googletag.pubads());
            googletag.defineSlot('/24409412/business_home_728x90_top_ad', [728, 90], 'div-gpt-ad-1448870249423-0').addService(googletag.pubads()); googletag.defineSlot('/24409412/business_home_top_728x90', [728, 90], 'div-gpt-ad-1502950638611-0').addService(googletag.pubads());
            googletag.defineSlot('/24409412/home_main_advert1_category', [728, 90], 'div-gpt-ad-1501662159615-0').addService(googletag.pubads());
            googletag.defineSlot('/24409412/business_rightpanel_advert1', [300, 250], 'div-gpt-ad-1486031230604-0').addService(googletag.pubads());
            googletag.defineSlot('/24409412/business_rightpanel_advert2', [300, 250], 'div-gpt-ad-1486031093371-0').addService(googletag.pubads());
            googletag.defineSlot('/24409412/category_main_advert3', [728, 90], 'div-gpt-ad-1487324533960-0').addService(googletag.pubads());
            googletag.pubads().enableSingleRequest();
            googletag.enableServices();
        });
    @endif
@endif
googletag.cmd.push(function() {
    var leaderboardMapping = googletag.sizeMapping().addSize([970,90],[728,90]).build();
    var bannerMapping = googletag.sizeMapping().addSize([336,280], [300, 250]).addSize([250, 250], [320, 100],[320, 50]).build();
    googletag.defineSlot('/24409412/Roadblock_700x300', [700, 300], 'div-gpt-ad-1501160382799-0').addService(googletag.pubads());
    googletag.defineSlot('/24409412/article_video_1x1', [1, 1], 'div-gpt-ad-1584621281588-0').addService(googletag.pubads());
    googletag.defineSlot('/24409412/lifestyle_top_advert', [728, 90], 'div-gpt-ad-1448283785649-0').addService(googletag.pubads());
    googletag.defineSlot('/24409412/lifestyle_rightpanel_advert1', [300, 250], 'div-gpt-ad-1486111544943-0').addService(googletag.pubads());
    googletag.defineSlot('/24409412/home_main_advert1_category', [728, 90], 'div-gpt-ad-1501662159615-0').addService(googletag.pubads());
    googletag.defineSlot('/24409412/lifestyle_rightpanel_advert2', [300, 250], 'div-gpt-ad-1463034636872-0').addService(googletag.pubads());
    googletag.defineSlot('/24409412/category_main_advert3', [728, 90], 'div-gpt-ad-1487324533960-0').addService(googletag.pubads());
    googletag.defineSlot('/24409412/lifestyle_rightpanel_advert3', [300, 250], 'div-gpt-ad-1468479063439-0').addService(googletag.pubads());
    googletag.pubads().enableSingleRequest();
    googletag.pubads().collapseEmptyDivs();
    googletag.enableServices();
});
@if($page == "article" || $page == "category")
    @if($parentId == 41 || $catId == 41)
        googletag.cmd.push(function() {
        var leaderboardMapping = googletag.sizeMapping().addSize([970,90],[728,90]).build();
        var bannerMapping = googletag.sizeMapping().addSize([336,280], [300, 250]).addSize([250, 250], [320, 100],[320, 50]).build();
        googletag.defineSlot('/24409412/Roadblock_700x300', [700, 300], 'div-gpt-ad-1501160382799-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/article_video_1x1', [1, 1], 'div-gpt-ad-1584621281588-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/health_top_advert', [728, 90], 'div-gpt-ad-1485841380368-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/category_main_advert3', [728, 90], 'div-gpt-ad-1487324533960-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/health_top_advert', [728, 90], 'div-gpt-ad-1485841380368-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/home_main_inner_middle_advert1', [728, 90], 'div-gpt-ad-1498022554617-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/health_rightpanel_advert1', [300, 250], 'div-gpt-ad-1486031342498-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/health_rightpanel_advert2', [300, 250], 'div-gpt-ad-1485841904663-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/health_rightpanel_advert3', [300, 250], 'div-gpt-ad-1508836899528-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/home_main_advert1_category', [728, 90], 'div-gpt-ad-1501662159615-0').addService(googletag.pubads());
        googletag.pubads().enableSingleRequest();googletag.enableServices();
        });
    @endif
@endif
@if($page == "article" || $page == "category")
    @if($parentId == 5 || $catId == 5)
    googletag.cmd.push(function() {
        var leaderboardMapping = googletag.sizeMapping().addSize([970,90],[728,90]).build();
        var bannerMapping = googletag.sizeMapping().addSize([336,280], [300, 250]).addSize([250, 250], [320, 100],[320, 50]).build();
        googletag.defineSlot('/24409412/digital_mobile_roadblock', [300, 250], 'div-gpt-ad-1508916204798-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/Roadblock_700x300', [700, 300], 'div-gpt-ad-1501160382799-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/world_top_advert', [728, 90], 'div-gpt-ad-1486118000063-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/world_rightpanel_advert1', [300, 250], 'div-gpt-ad-1486118241460-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/article_video_1x1', [1, 1], 'div-gpt-ad-1584621281588-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/home_main_inner_middle_advert1', [728, 90], 'div-gpt-ad-1498022554617-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/world_rightpanel_advert2', [300, 250], 'div-gpt-ad-1486118183654-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/world_rightpanel_advert3', [300, 250], 'div-gpt-ad-1486118284719-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/category_main_advert3', [728, 90], 'div-gpt-ad-1487324533960-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/profitButton', [1, 1], 'div-gpt-ad-1509977037140-0').addService(googletag.pubads());
        googletag.pubads().enableSingleRequest();
        googletag.enableServices();
    });
    @endif
@endif
googletag.cmd.push(function() {
      var leaderboardMapping = googletag.sizeMapping().
      addSize([970,90],[728,90]).
      build();
      var bannerMapping = googletag.sizeMapping().
      addSize([336,280], [300, 250]).
      addSize([250, 250], [320, 100],[320, 50]).
      build();
      googletag.defineSlot('/24409412/Roadblock_700x300', [700, 300], 'div-gpt-ad-1501160382799-0').addService(googletag.pubads());
      googletag.defineSlot('/24409412/profitButton', [1, 1], 'div-gpt-ad-1509977037140-0').addService(googletag.pubads());
      googletag.defineSlot('/24409412/home_main_top_inner_advert1', [728, 90], 'div-gpt-ad-1447154680884-0').addService(googletag.pubads());
      googletag.defineSlot('/24409412/article_video_1x1', [1, 1], 'div-gpt-ad-1584621281588-0').addService(googletag.pubads());
      googletag.defineSlot('/24409412/gameyetu_floating_banner', [728, 90], 'div-gpt-ad-1579702544837-0').addService(googletag.pubads());
      googletag.defineSlot('/24409412/home_main_inner_middle_advert1', [728, 90], 'div-gpt-ad-1498022554617-0').addService(googletag.pubads());
      googletag.defineSlot('/24409412/home_main_rightpanel_inner_advert1', [300, 250], 'div-gpt-ad-1490019062957-0').addService(googletag.pubads());
      googletag.defineSlot('/24409412/native_1_320x100', [320, 100], 'div-gpt-ad-1507892431347-0').addService(googletag.pubads());
      googletag.defineSlot('/24409412/native_1_320x100', [320, 100], 'div-gpt-ad-1507892431347-0').addService(googletag.pubads());
      googletag.defineSlot('/24409412/native_desktop_1_320x250', [320, 100], 'div-gpt-ad-1517292729772-0').addService(googletag.pubads());
      googletag.defineSlot('/24409412/home_main_rightpanel_inner_advert2', [300, 250], 'div-gpt-ad-1485780596044-0').addService(googletag.pubads());
      googletag.defineSlot('/24409412/home_main_rightpanel_inner_advert3', [300, 250], 'div-gpt-ad-1509530393748-0').addService(googletag.pubads());
      googletag.defineSlot('/24409412/category_main_advert3', [728, 90], 'div-gpt-ad-1487324533960-0').addService(googletag.pubads());
      googletag.defineSlot('/24409412/native_desktop_1_320x250', [320, 100], 'div-gpt-ad-1507900527499-0').addService(googletag.pubads());
      googletag.defineSlot('/24409412/home_main_rightpanel_inner_advert3', [300, 250], 'div-gpt-ad-1509530393748-0').addService(googletag.pubads());
      googletag.defineSlot('/24409412/profitButton', [1, 1], 'div-gpt-ad-1509977037140-0').addService(googletag.pubads());
      googletag.pubads().enableSingleRequest();
      googletag.enableServices();
});
</script>
<script>
    window.googletag = window.googletag || {cmd: []};
    googletag.cmd.push(function() {
        googletag.defineSlot('/24409412/home_main_rightpanel_advert2', [300, 250], 'div-gpt-ad-1588418825435-0').addService(googletag.pubads());
        googletag.pubads().enableSingleRequest();
        googletag.enableServices();
    });
</script>
<script>
    window.googletag = window.googletag || {cmd: []};
    googletag.cmd.push(function() {
        googletag.defineSlot('/24409412/world_top_advert', [728, 90], 'div-gpt-ad-1588419213190-0').addService(googletag.pubads());
        googletag.pubads().enableSingleRequest();
        googletag.enableServices();
    });
</script>
<?php /* ?>
<script type="text/javascript">
  window._taboola = window._taboola || [];
  _taboola.push({article:'auto'});
  !function (e, f, u, i) {
    if (!document.getElementById(i)){
      e.async = 1;
      e.src = u;
      e.id = i;
      f.parentNode.insertBefore(e, f);
    }
  }(document.createElement('script'),
  document.getElementsByTagName('script')[0],
  '//cdn.taboola.com/libtrc/standardgroupplc-standardmedia/loader.js',
  'tb_loader_script');
  if(window.performance && typeof window.performance.mark == 'function')
    {window.performance.mark('tbl_ic');}
</script>
<?php */ ?>
