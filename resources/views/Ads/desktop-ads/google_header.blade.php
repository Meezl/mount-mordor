<meta name="msvalidate.01" content="8750838B58217639FD3943354B7B9BA6" />
<meta http-equiv="Cache-control" content="public">
<meta http-equiv="refresh" content="6000">
<link rel="shortcut icon" href="favicon.ico"/>


<script type='text/javascript'>
    (function() {
        var useSSL = 'https:' == document.location.protocol;
        var src = (useSSL ? 'https:' : 'http:') +
            '//www.googletagservices.com/tag/js/gpt.js';
        document.write('<scr' + 'ipt src="' + src + '"></scr' + 'ipt>');
    })();
</script>

<script type='text/javascript'>
    googletag.cmd.push(function() {
        googletag.defineSlot('/24409412/delpfish720x90', [728, 90], 'div-gpt-ad-1368715346236-0').addService(googletag.pubads()); 
    });
</script>
<script type='text/javascript'>
    googletag.cmd.push(function() {
        googletag.defineSlot('/24409412/KCB', [300, 250], 'div-gpt-ad-1366386484352-0').addService(googletag.pubads());
    });
</script>

<script type='text/javascript'>
    // category----inhose and ethiopia
    googletag.cmd.push(function() {
        googletag.defineSlot('/24409412/In_house_1', [300, 250], 'div-gpt-ad-1367593134827-0').addService(googletag.pubads()); 
    });
</script>

<script type='text/javascript'>
    googletag.cmd.push(function() {
        googletag.defineSlot('/24409412/SDE_300x250', [300, 250], 'div-gpt-ad-1368007625179-0').addService(googletag.pubads()); 
    });
</script>

<script type='text/javascript'>
    googletag.cmd.push(function() {
        googletag.defineSlot('/24409412/In_house_1', [300, 250], 'div-gpt-ad-1369741341767-0').addService(googletag.pubads());
    });
</script>
<script type='text/javascript'>
    googletag.defineSlot('/24409412/STD_category_728x90', [728, 90], 'div-gpt-ad-1369812044281-0').addService(googletag.pubads());
    googletag.enableServices();
</script>
<script type='text/javascript'>
    googletag.cmd.push(function() {
        googletag.defineSlot('/24409412/acwict-300x250', [300, 250], 'div-gpt-ad-1369990183646-0').addService(googletag.pubads());
        googletag.enableServices();
    });
</script>
<script type='text/javascript'>
    googletag.cmd.push(function() {
        googletag.defineSlot('/24409412/Windsor', [300, 250], 'div-gpt-ad-1370409733217-0').addService(googletag.pubads());
    });
</script>

<script type='text/javascript'>
    googletag.cmd.push(function() {
        googletag.defineSlot('/24409412/article_page_300x250', [300, 250], 'div-gpt-ad-1371103988733-0').addService(googletag.pubads());
    });
</script>

<script type='text/javascript'>
    googletag.defineSlot('/24409412/Inhouse_Adverts', [340, 110], 'div-gpt-ad-1369731470375-0').addService(googletag.pubads());
</script>

<script type='text/javascript'>
    googletag.cmd.push(function() {
        googletag.defineSlot('/24409412/Grupo_Intercom728x90', [728, 90], 'div-gpt-ad-1371134953243-0').addService(googletag.pubads());
    });
</script>
<script type='text/javascript'>
    googletag.cmd.push(function() {
        googletag.defineSlot('/24409412/Orange_Kenya_300x250', [300, 250], 'div-gpt-ad-1371461974590-0').addService(googletag.pubads());
    });
</script>
<script type='text/javascript'>
    googletag.cmd.push(function() {
        googletag.defineSlot('/24409412/Orange_728x90_advert', [728, 90], 'div-gpt-ad-1371531653328-0').addService(googletag.pubads());
    });
</script>
<script type='text/javascript'>
    googletag.cmd.push(function() {
        googletag.defineSlot('/24409412/USIU_700x90', [728, 90], 'div-gpt-ad-1371623207250-0').addService(googletag.pubads());
    });
</script>
<script type='text/javascript'>
    googletag.cmd.push(function() {
        googletag.defineSlot('/24409412/Qatar_Airways', [728, 90], 'div-gpt-ad-1371644141809-0').addService(googletag.pubads());
    });
</script>
<script type='text/javascript'>
    googletag.cmd.push(function() {
        googletag.defineSlot('/24409412/Orange_Kenya_300x250', [300, 250], 'div-gpt-ad-1371716394583-0').addService(googletag.pubads());
    });
</script>


<script type='text/javascript'>
    googletag.cmd.push(function() {
        googletag.defineSlot('/24409412/eadatahandler300x250', [300, 250], 'div-gpt-ad-1372317851014-0').addService(googletag.pubads());
    });
</script>

<script type='text/javascript'>
    googletag.cmd.push(function() {
        googletag.defineSlot('/24409412/Grupo_Intercom300x250', [300, 250], 'div-gpt-ad-1372839261631-0').addService(googletag.pubads());
    });
</script>
<script type='text/javascript'>
    googletag.cmd.push(function() {
        googletag.defineSlot('/24409412/std_category_page_top', [728, 90], 'div-gpt-ad-1372949210143-0').addService(googletag.pubads());
    });
</script>
<script type='text/javascript'>
    googletag.cmd.push(function() {
        googletag.defineSlot('/24409412/homepage_first_news_panel', [300, 250], 'div-gpt-ad-1372950190422-0').addService(googletag.pubads());
    });
</script>
<script type='text/javascript'>
    googletag.cmd.push(function() {
        googletag.defineSlot('/24409412/homepage_2nd_banner', [300, 250], 'div-gpt-ad-1372950857205-0').addService(googletag.pubads()); 
    });
</script>

<script type='text/javascript'>
    googletag.cmd.push(function() {
        googletag.defineSlot('/24409412/drticle_page_2nd_ad', [300, 250], 'div-gpt-ad-1372775515229-0').addService(googletag.pubads());
        googletag.pubads().enableSingleRequest();
        googletag.enableServices();
    });
</script>

<script type="text/javascript">
    //Casale pop under
    var casaleD=new Date();var casaleR=(casaleD.getTime()%8673806982)+Math.random();
    var casaleU=escape(window.location.href);
    var casaleHost=' type="text/javascript" src="http://as.casalemedia.com/s?s=';
    document.write('<scr'+'ipt'+casaleHost+'97071&amp;u=');
    document.write(casaleU+'&amp;f=1&amp;id='+casaleR+'"><\/scr'+'ipt>'); </script>