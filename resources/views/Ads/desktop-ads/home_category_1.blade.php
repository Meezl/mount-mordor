
<?php
$newspanels = $this->home_model->get_category_newspanel($categoryID, 6, 0);
$count_newspanel = 0;
foreach ($newspanels->result() as $panels):
    $count_newspanel++;
    ?>
	<?php if($count_newspanel==1){ ?>
		<div class="home-advert"><?php $this->load->view('themes/sdv1/adv/home_main_advert2'); ?></div>
	<?php }elseif($count_newspanel==2){ ?>
		<div class="home-advert"><?php $this->load->view('themes/sdv1/adv/home_main_advert4'); ?></div>
	<?php }elseif($count_newspanel==3){ ?>
		<div class="home-advert"><?php $this->load->view('themes/sdv1/adv/home_main_advert6'); ?></div>
	<?php }elseif($count_newspanel==4){ ?>
	<!--<div class="home-advert"><?php //$this->load->view('themes/sdv1/adv/home_main_advert7'); ?></div>-->
	<?php }elseif($count_newspanel==5){ ?>
		<!--<div class="home-advert"><?php //$this->load->view('themes/sdv1/adv/home_main_advert8'); ?></div>-->
	<?php } ?>
    <div class="category-panel" style="">
        <h3 class="<?php if ($count_newspanel == 1) { ?>kenya-header <?php } elseif ($count_newspanel == 2) { ?>politics-header <?php } elseif ($count_newspanel == 3) { ?>business-header <?php } elseif ($count_newspanel == 4) { ?>green-header<?php } elseif ($count_newspanel == 5) { ?> purple-header<?php } ?>"><a href="<?php if($panels->id==6){ echo base_url()."sports"; }else{ ?><?php echo base_url(); ?>?categoryID=<?php echo $panels->id; ?><?php } ?>"  style=""><?php echo $panels->name; ?></a></h3>
        <div class="category-main">
            <?php
            $start = 0;
            $mainheadline = $this->home_model->getArticlesListingForCategoryLayout($panels->id, 1, $start);
            $countpanels = 0;
            foreach ($mainheadline->result() as $mainhl):
                $countpanels++;
                ?>
                <?php if ($mainhl->thumbURL != "") { ?>
                    <a href="<?php echo base_url() ?>article/<?php echo $mainhl->id; ?>/<?php echo $this->home_model->slugify($mainhl->title); ?>"><div class="cat-img">	<img src="<?php echo $mainhl->thumbURL; ?>" class="mm" border="0"></div></a>
                <?php } ?>
                    <div class=" <?php if ($mainhl->thumbURL != "") { ?> cont-category <?php }else{ ?> cont-category-wide <?php } ?>">
                      <div class="cont-section">  
                          <h4><a href="<?php echo base_url() ?>article/<?php echo $mainhl->id; ?>/<?php echo $this->home_model->slugify($mainhl->title); ?>" style="">
            <?php echo $this->home_model->chrstring($mainhl->title); ?></a></h4>
                    <div style='display:inline-block;' class="cat-comment-mm <?php if ($count_newspanel == 1) { ?>cat-comments <?php } elseif ($count_newspanel == 2) { ?>cat-comments-red <?php } elseif ($count_newspanel == 3) { ?>cat-comments-blue<?php } ?>">&nbsp;<a href="<?php echo base_url() ?>article/<?php echo $mainhl->id; ?>/<?php echo $this->home_model->slugify($mainhl->title); ?>#disqus_thread" data-disqus-identifier="<?php echo $mainhl->id; ?>"> Comments</a> - <span class="time-ago"><?php echo $this->home_model->time_ago($mainhl->publishdate); ?></span></div>
                    <div class="clr"></div>
            
                   <div style=""> <?php echo $this->home_model->chrstring($mainhl->summary); ?></div>
                    <div class="clr"></div>
                    <?php if ($count_newspanel == 1) { ?> <a href="<?php echo base_url() ?>article/<?php echo $mainhl->id; ?>/<?php echo $this->home_model->slugify($mainhl->title); ?>" class="read-more-blue"><img src="<?php echo base_url(); ?>resources/theme_sdv1/images/go-blue-icon.png" border="0"></a>
                    <?php } elseif ($count_newspanel == 2) { ?> <a href="<?php echo base_url() ?>?article/<?php echo $mainhl->id; ?>/<?php echo $this->home_model->slugify($mainhl->title); ?>" class="read-more-red"><img src="<?php echo base_url(); ?>resources/theme_sdv1/images/go-red-icon.png" border="0"></a>
                    <?php } elseif ($count_newspanel == 3) { ?> <a href="<?php echo base_url() ?>?article/<?php echo $mainhl->id; ?>/<?php echo $this->home_model->slugify($mainhl->title); ?>" class="read-more-blue"><img src="<?php echo base_url(); ?>resources/theme_sdv1/images/go-blue-icon.png" border="0"></a>
        <?php } ?>
                    </div>
    <?php endforeach; ?>
                <div class="sponsored-section">
				<?php if($count_newspanel==1){ ?>
                    Sponsored by:
					<div><?php $this->load->view('themes/sdv1/adv/Sponsored_Kenya'); ?></div>
					<?php }elseif($count_newspanel==2){ ?>
					Sponsored by:
                    <div><?php $this->load->view('themes/sdv1/adv/Sponsored_theCounties'); ?></div>
					<?php }elseif($count_newspanel==3){ ?>
					<!--Sponsored by:
                    <div><?php //$this->load->view('themes/sdv1/adv/sponsored_Business'); ?></div>-->
					<?php }elseif($count_newspanel==4){ ?>
					<!--Sponsored by:
                    <div><?php //$this->load->view('themes/sdv1/adv/sponsored_World'); ?></div>-->
					<?php }elseif($count_newspanel==5){ ?>
					<!--Sponsored by:
                    <div><?php //$this->load->view('themes/sdv1/adv/sponsored_Sports'); ?></div>-->
					<?php } ?>
                </div>
                    <div class="clr"></div>
            </div>
        </div>
        <div class="clr"></div>
        <div class="cont-list">
            <ul class="r-separate">
                <?php
                $tart = 1;
                $otherhl = $this->home_model->getArticlesListingForCategoryLayout($panels->id, 3, $tart);
                foreach ($otherhl->result() as $ohl):
                    ?>
                    <li>
					<a href="<?php echo base_url() ?>article/<?php echo $ohl->id; ?>/<?php echo $this->home_model->slugify($ohl->title); ?>"  style=""><?php echo $this->home_model->chrstring($ohl->title); ?></a>
					 <div style='display:inline-block;'><strong class="<?php if ($count_newspanel == 1) { ?>dl-comments <?php } elseif ($count_newspanel == 2) { ?>dl-comments-red <?php } elseif ($count_newspanel == 3) { ?>dl-comments-blue<?php }else{ ?>dl-comments<?php } ?>" >&nbsp;<a href="<?php echo base_url() ?>article/<?php echo $ohl->id; ?>/<?php echo $this->home_model->slugify($ohl->title); ?>#disqus_thread" data-disqus-identifier="<?php echo $ohl->id; ?>" style="color:#ccc"> Comments</a></strong>
                  - <span class="time-ago"><?php echo $this->home_model->time_ago($ohl->publishdate); ?></span></div>
            
					</li>
    <?php endforeach; ?>  
            </ul>
            <ul>
                   <?php
                $tart = 4;
                $otherhl = $this->home_model->getArticlesListingForCategoryLayout($panels->id, 3, $tart);
                foreach ($otherhl->result() as $ohl):
                    ?>
                    <li>
					<a href="<?php echo base_url() ?>article/<?php echo $ohl->id; ?>/<?php echo $this->home_model->slugify($ohl->title); ?>"  style=""><?php echo $this->home_model->chrstring($ohl->title); ?></a>
					 	 <div style='display:inline-block;'><strong class="<?php if ($count_newspanel == 1) { ?>dl-comments <?php } elseif ($count_newspanel == 2) { ?>dl-comments-red <?php } elseif ($count_newspanel == 3) { ?>dl-comments-blue<?php }else{ ?>dl-comments <?php } ?>" >&nbsp;<a href="<?php echo base_url() ?>article/<?php echo $ohl->id; ?>/<?php echo $this->home_model->slugify($ohl->title); ?>#disqus_thread" data-disqus-identifier="<?php echo $ohl->id; ?>" style="color:#ccc"> Comments</a></strong>
                            - <span class="time-ago"><?php echo $this->home_model->time_ago($ohl->publishdate); ?></span></div>
			   </li>
    <?php endforeach; ?>  
            </ul>
            <div class="clr"></div>
        </div>
    </div>
        <?php if ($count_newspanel == 1) { ?>
    <div class="home-advert">  <?php $this->load->view('themes/sdv1/adv/home_main_advert3'); ?>  </div> 
      
        <?php $this->load->view('themes/sdv1/widgets/ktn_popular_videos'); ?>    
        <div class="clr"></div>
		
     
    <div class="home-advert"> <?php $this->load->view('themes/sdv1/adv/home_main_advert5'); ?>  </div> 
	
        <?php $this->load->view('themes/sdv1/widgets/home_columns'); ?>  
    <?php } ?>
<?php endforeach; ?>