<script  async="async"  src="https://www.googletagservices.com/tag/js/gpt.js"></script>
<script>
  var googletag = googletag || {};
  googletag.cmd = googletag.cmd || [];
@if($page == "home" || $page == "category")
  googletag.cmd.push(function() {
    googletag.defineSlot('/24409412/home_mobile_advert4', [300, 250], 'div-gpt-ad-1486967651852-0').addService(googletag.pubads());
    googletag.enableServices();
  });
  googletag.cmd.push(function() {
    googletag.defineSlot('/24409412/home_mobile_advert3', [300, 250], 'div-gpt-ad-1485783798639-0').addService(googletag.pubads());
    googletag.enableServices();
  });
  googletag.cmd.push(function() {
    googletag.defineSlot('/24409412/home_mobile_advert2', [300, 250], 'div-gpt-ad-1486967549842-0').addService(googletag.pubads());
    googletag.enableServices();
  });
  googletag.cmd.push(function() {
    googletag.defineSlot('/24409412/home_mobile_advert1', [300, 250], 'div-gpt-ad-1486967457274-0').addService(googletag.pubads());
    googletag.enableServices();
  });
@endif
@if($page == "article" || $page == "category")
      googletag.cmd.push(function() {
        googletag.defineSlot('/24409412/mobile_inner_article_advert4', [300, 250], 'div-gpt-ad-1487427912387-0').addService(googletag.pubads());
        googletag.enableServices();
      });
      googletag.cmd.push(function() {
        googletag.defineSlot('/24409412/mobile_inner_article_advert3', [300, 250], 'div-gpt-ad-1485784617192-0').addService(googletag.pubads());
        googletag.enableServices();
      });
      googletag.cmd.push(function() {
        googletag.defineSlot('/24409412/mobile_inner_article_advert2', [300, 250], 'div-gpt-ad-1485784540797-0').addService(googletag.pubads());
        googletag.enableServices();
      });
        googletag.cmd.push(function() {
        googletag.defineSlot('/24409412/mobile_inner_article_advert1', [300, 250], 'div-gpt-ad-1485784479649-0').addService(googletag.pubads());
        googletag.enableServices();
      });
@endif
</script>
