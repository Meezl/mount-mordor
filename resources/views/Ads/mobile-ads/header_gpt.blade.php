	<script  async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
<script>
  var googletag = googletag || {};
  googletag.cmd = googletag.cmd || [];
</script>

<script>
@if($page == 'home' || $page == 'author' || $page == 'topic' || $page == 'search' )
    googletag.cmd.push(function() {
        var bannerMapping = googletag.sizeMapping().addSize([336,280], [300, 250]).addSize([250, 250], [320, 100],[320, 50]).build();
            googletag.defineSlot('/24409412/home_mobile_main_top_320x100', [320, 100], 'div-gpt-ad-1542281308069-0').addService(googletag.pubads());
            googletag.defineSlot('/24409412/home_mobile_sticky_ad_320x50', [320, 50], 'div-gpt-ad-1571405295420-0').addService(googletag.pubads());
            googletag.defineSlot('/24409412/home_mobile_advert1', [300, 250], 'div-gpt-ad-1486967457274-0').addService(googletag.pubads());
            googletag.defineSlot('/24409412/home_mobile_advert2', [300, 250], 'div-gpt-ad-1486967549842-0').addService(googletag.pubads());
            googletag.defineSlot('/24409412/home_mobile_advert3', [300, 250], 'div-gpt-ad-1485783798639-0').addService(googletag.pubads());
            googletag.defineSlot('/24409412/home_mobile_advert4', [300, 250], 'div-gpt-ad-1513226945539-0').addService(googletag.pubads());
            googletag.defineSlot('/24409412/home_mobile_advert5', [300, 250], 'div-gpt-ad-1498032365554-0').addService(googletag.pubads());
        @if($roadBlockTime)
            @if((new \Illuminate\Http\Request())->cookie('roadBlock') && (new \Jenssegers\Agent\Agent())->isMobile())
                googletag.defineSlot('/24409412/digital_mobile_roadblock', [300, 250], 'div-gpt-ad-1508916204798-0').addService(googletag.pubads());
            @elseif((new \Illuminate\Http\Request())->cookie('roadBlock') && (new \Jenssegers\Agent\Agent())->isDesktop())
                googletag.defineSlot('/24409412/Roadblock_700x300', [700, 300], 'div-gpt-ad-1501160382799-0').addService(googletag.pubads());
            @endif
        @endif
        googletag.pubads().enableSingleRequest();
        googletag.enableServices();
    });
@endif
@if($page == 'article' || $page == 'category')
    @if(isset($parentId) || isset($catId))
        @if($parentId == 4 || $catId == 4)
            googletag.cmd.push(function() {
                var bannerMapping = googletag.sizeMapping().addSize([336,280], [300, 250]).addSize([250, 250], [320, 100],[320, 50]).build();
                googletag.defineSlot('/24409412/home_mobile_sticky_ad_320x50', [320, 50], 'div-gpt-ad-1571405295420-0').addService(googletag.pubads());
                var mobiletop = googletag.defineSlot('/24409412/business_mobile_main_top_320x100', [320, 100], 'div-gpt-ad-1523960718236-0').addService(googletag.pubads());
                @if($roadBlockTime)
                    @if((new \Illuminate\Http\Request())->cookie('roadBlock') && (new \Jenssegers\Agent\Agent())->isMobile())
                        googletag.defineSlot('/24409412/digital_mobile_roadblock', [300, 250], 'div-gpt-ad-1508916204798-0').addService(googletag.pubads());
                    @elseif((new \Illuminate\Http\Request())->cookie('roadBlock') && (new \Jenssegers\Agent\Agent())->isDesktop())
                        googletag.defineSlot('/24409412/Roadblock_700x300', [700, 300], 'div-gpt-ad-1501160382799-0').addService(googletag.pubads());
                    @endif
                @endif
                googletag.defineSlot('/24409412/article_video_1x1', [1, 1], 'div-gpt-ad-1584621281588-0').addService(googletag.pubads());
                var mobileinnerarticle1 = googletag.defineSlot('/24409412/business_rightpanel_advert1', [300, 250], 'div-gpt-ad-1486031230604-0').addService(googletag.pubads());
                var mobileinnerarticle2 = googletag.defineSlot('/24409412/business_rightpanel_advert2', [300, 250], 'div-gpt-ad-1486031093371-0').addService(googletag.pubads());
                var mobileinnerarticle3 = googletag.defineSlot('/24409412/business_rightpanel_advert3', [300, 250], 'div-gpt-ad-1468478909150-0').addService(googletag.pubads());
                var mobileinnerarticle4 = googletag.defineSlot('/24409412/business_rightpanel_advert4', [300, 250], 'div-gpt-ad-1449839026167-0').addService(googletag.pubads());
                var mobileinnerarticle5 = googletag.defineSlot('/24409412/mobile_inner_article_advert5', [300, 250], 'div-gpt-ad-1555499801517-0').addService(googletag.pubads());
                googletag.pubads().enableSingleRequest();
                googletag.enableServices();
            });
        @endif
    @endif
@endif
// googletag.cmd.push(function() {
//     var bannerMapping = googletag.sizeMapping().addSize([336,280], [300, 250]).addSize([250, 250], [320, 100],[320, 50]).build();
//     googletag.defineSlot('/24409412/home_mobile_sticky_ad_320x50', [320, 50], 'div-gpt-ad-1571405295420-0').addService(googletag.pubads());
//     googletag.defineSlot('/24409412/digital_mobile_roadblock', [300, 250], 'div-gpt-ad-1508916204798-0').addService(googletag.pubads());
//     googletag.defineSlot('/24409412/Roadblock_700x300', [700, 300], 'div-gpt-ad-1501160382799-0').addService(googletag.pubads());
//     googletag.defineSlot('/24409412/native_1_320x100', [320, 100], 'div-gpt-ad-1507892431347-0').addService(googletag.pubads());
//     googletag.defineSlot('/24409412/article_video_1x1', [1, 1], 'div-gpt-ad-1584621281588-0').addService(googletag.pubads());
//     var mobiletop = googletag.defineSlot('/24409412/kenya_mobile_main_top_320x100', [320, 100], 'div-gpt-ad-1523961231163-0').addService(googletag.pubads());
//     var mobileinnerarticle1 =  googletag.defineSlot('/24409412/mobile_inner_article_advert1', [300, 250], 'div-gpt-ad-1485784479649-0').addService(googletag.pubads());
//     var mobileinnerarticle2 =  googletag.defineSlot('/24409412/mobile_inner_article_advert2', [300, 250], 'div-gpt-ad-1485784540797-0').addService(googletag.pubads());
//     var mobileinnerarticle3 =  googletag.defineSlot('/24409412/mobile_inner_article_advert3', [300, 250], 'div-gpt-ad-1485784617192-0').addService(googletag.pubads());
//     var mobileinnerarticle4 =  googletag.defineSlot('/24409412/mobile_inner_article_advert4', [300, 250], 'div-gpt-ad-1503494944149-0').addService(googletag.pubads());
//     var mobileinnerarticle5 =  googletag.defineSlot('/24409412/mobile_inner_article_advert5', [300, 250], 'div-gpt-ad-1555499801517-0').addService(googletag.pubads());
//     googletag.defineSlot('/24409412/native_1_320x100', [320, 100], 'div-gpt-ad-1507892431347-0').addService(googletag.pubads());
//     googletag.pubads().enableSingleRequest();
//     googletag.enableServices();
// });
@if($page == 'article' || $page == 'category')
    @if(isset($parentId) || isset($catId))
        @if($parentId == 9 || $catId == 9)
            googletag.cmd.push(function() {
                var bannerMapping = googletag.sizeMapping().addSize([336,280], [300, 250]).addSize([250, 250], [320, 100],[320, 50]).build();
                googletag.defineSlot('/24409412/home_mobile_sticky_ad_320x50', [320, 50], 'div-gpt-ad-1571405295420-0').addService(googletag.pubads());
                @if($roadBlockTime)
                    @if((new \Illuminate\Http\Request())->cookie('roadBlock') && (new \Jenssegers\Agent\Agent())->isMobile())
                        googletag.defineSlot('/24409412/digital_mobile_roadblock', [300, 250], 'div-gpt-ad-1508916204798-0').addService(googletag.pubads());
                    @elseif((new \Illuminate\Http\Request())->cookie('roadBlock') && (new \Jenssegers\Agent\Agent())->isDesktop())
                        googletag.defineSlot('/24409412/Roadblock_700x300', [700, 300], 'div-gpt-ad-1501160382799-0').addService(googletag.pubads());
                    @endif
                @endif
                googletag.defineSlot('/24409412/article_video_1x1', [1, 1], 'div-gpt-ad-1584621281588-0').addService(googletag.pubads());
                googletag.defineSlot('/24409412/lifestyle_mobile_advert1', [300, 250], 'div-gpt-ad-1498131421317-0').addService(googletag.pubads());
                googletag.defineSlot('/24409412/lifestyle_rightpanel_advert2', [300, 250], 'div-gpt-ad-1463034636872-0').addService(googletag.pubads());
                googletag.defineSlot('/24409412/lifestyle_rightpanel_advert3', [300, 250], 'div-gpt-ad-1468479063439-0').addService(googletag.pubads());
                googletag.defineSlot('/24409412/lifestyle_rightpanel_advert4', [300, 250], 'div-gpt-ad-1400072127302-0').addService(googletag.pubads()); googletag.defineSlot('/24409412/mobile_inner_article_advert5', [300, 250], 'div-gpt-ad-1555499801517-0').addService(googletag.pubads());
                googletag.pubads().enableSingleRequest();
                googletag.enableServices();
            });
        @endif
    @endif
@endif

@if($page == 'article' )
    googletag.cmd.push(function() {
         var bannerMapping = googletag.sizeMapping().addSize([336,280], [300, 250]).addSize([250, 250], [320, 100],[320, 50]).build();
         @if($roadBlockTime)
            @if((new \Illuminate\Http\Request())->cookie('roadBlock') && (new \Jenssegers\Agent\Agent())->isMobile())
                googletag.defineSlot('/24409412/digital_mobile_roadblock', [300, 250], 'div-gpt-ad-1508916204798-0').addService(googletag.pubads());
            @elseif((new \Illuminate\Http\Request())->cookie('roadBlock') && (new \Jenssegers\Agent\Agent())->isDesktop())
                googletag.defineSlot('/24409412/Roadblock_700x300', [700, 300], 'div-gpt-ad-1501160382799-0').addService(googletag.pubads());
            @endif
         @endif
         googletag.defineSlot('/24409412/home_mobile_sticky_ad_320x50', [320, 50], 'div-gpt-ad-1571405295420-0').addService(googletag.pubads());
         var mobiletop = googletag.defineSlot('/24409412/kenya_mobile_main_top_320x100', [320, 100], 'div-gpt-ad-1523961231163-0').addService(googletag.pubads());
         var mobileinnerarticle1 =  googletag.defineSlot('/24409412/mobile_inner_article_advert1', [300, 250], 'div-gpt-ad-1485784479649-0').addService(googletag.pubads());
         var mobileinnerarticle2 =  googletag.defineSlot('/24409412/mobile_inner_article_advert2', [300, 250], 'div-gpt-ad-1485784540797-0').addService(googletag.pubads());
         var mobileinnerarticle3 =  googletag.defineSlot('/24409412/mobile_inner_article_advert3', [300, 250], 'div-gpt-ad-1485784617192-0').addService(googletag.pubads());
         googletag.defineSlot('/24409412/article_video_1x1', [1, 1], 'div-gpt-ad-1584621281588-0').addService(googletag.pubads());
         var mobileinnerarticle4 =  googletag.defineSlot('/24409412/mobile_inner_article_advert4', [300, 250], 'div-gpt-ad-1503494944149-0').addService(googletag.pubads());
         var mobileinnerarticle5 =  googletag.defineSlot('/24409412/mobile_inner_article_advert5', [300, 250], 'div-gpt-ad-1555499801517-0').addService(googletag.pubads());
         googletag.pubads().enableSingleRequest();
         googletag.enableServices();
    });
@endif
</script>

