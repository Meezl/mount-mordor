<script  async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
<script>
    var googletag = googletag || {};
    googletag.cmd = googletag.cmd || [];
</script>

<script>
    googletag.cmd.push(function() {
        var bannerMapping = googletag.sizeMapping().addSize([336,280], [300, 250]).addSize([250, 250], [320, 100],[320, 50]).build();
        googletag.defineSlot('/24409412/home_mobile_main_top_320x100', [320, 100], 'div-gpt-ad-1542281308069-0').addService(googletag.pubads());
        // googletag.defineSlot('/24409412/home_mobile_sticky_ad_320x50', [320, 50], 'div-gpt-ad-1571405295420-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/home_mobile_advert1', [300, 250], 'div-gpt-ad-1486967457274-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/home_mobile_advert2', [300, 250], 'div-gpt-ad-1486967549842-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/home_mobile_advert3', [300, 250], 'div-gpt-ad-1485783798639-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/home_mobile_advert4', [300, 250], 'div-gpt-ad-1513226945539-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/home_mobile_advert5', [300, 250], 'div-gpt-ad-1589357888356-0').addService(googletag.pubads());
        @if($roadBlockTime)
        @if((new \Illuminate\Http\Request())->cookie('roadBlock') && (new \Jenssegers\Agent\Agent())->isMobile())
        googletag.defineSlot('/24409412/digital_mobile_roadblock', [300, 250], 'div-gpt-ad-1508916204798-0').addService(googletag.pubads());
        @elseif((new \Illuminate\Http\Request())->cookie('roadBlock') && (new \Jenssegers\Agent\Agent())->isDesktop())
        googletag.defineSlot('/24409412/Roadblock_700x300', [700, 300], 'div-gpt-ad-1501160382799-0').addService(googletag.pubads());
        @endif
        @endif
        googletag.pubads().enableSingleRequest();
        googletag.enableServices();
    });
</script>

