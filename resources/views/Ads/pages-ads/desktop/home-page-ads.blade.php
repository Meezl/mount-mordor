<script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
<script>
    var googletag = googletag || {};
    window.googletag = window.googletag || {cmd: []};
    googletag.cmd = googletag.cmd || [];
</script>

<script>
    googletag.cmd.push(function() {
        var leaderboardMapping = googletag.sizeMapping().addSize([970,90],[728,90]).build();
        var bannerMapping = googletag.sizeMapping().addSize([336,280], [300, 250]).addSize([250, 250], [320, 100],[320, 50]).build();
        googletag.defineSlot('/24409412/home_main_advert1', [728, 90], 'div-gpt-ad-1486010480522-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/home_main_rightpanel_advert1', [300, 250], 'div-gpt-ad-1400140713631-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/home_main_advert2', [728, 90], 'div-gpt-ad-1486968612976-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/home_main_rightpanel_advert2', [300, 250], 'div-gpt-ad-1486627050053-0').addService(googletag.pubads());
        googletag.pubads().enableSingleRequest();
        googletag.enableServices();
    });
</script>
