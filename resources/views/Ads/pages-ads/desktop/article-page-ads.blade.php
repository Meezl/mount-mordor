<script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
<script>
    var googletag = googletag || {};
    window.googletag = window.googletag || {cmd: []};
    googletag.cmd = googletag.cmd || [];
</script>

<script>
    @if($parentId == 1 || $catId == 1)
    googletag.cmd.push(function() {
        var leaderboardMapping = googletag.sizeMapping().addSize([970,90],[728,90]).build();
        var bannerMapping = googletag.sizeMapping().addSize([336,280], [300, 250]).addSize([250, 250], [320, 100],[320, 50]).build();
        // googletag.defineSlot('/24409412/Roadblock_700x300', [700, 300], 'div-gpt-ad-1501160382799-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/home_main_top_inner_advert1', [728, 90], 'div-gpt-ad-1447154680884-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/home_main_inner_middle_advert1', [728, 90], 'div-gpt-ad-1498022554617-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/home_main_rightpanel_advert1', [300, 250], 'div-gpt-ad-1400140713631-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/home_main_rightpanel_advert2', [300, 250], 'div-gpt-ad-1486627050053-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/home_main_rightpanel_advert3', [300, 250], 'div-gpt-ad-1582865963023-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/home_main_rightpanel_advert4', [300, 250], 'div-gpt-ad-1530196033093-0').addService(googletag.pubads());
        googletag.pubads().enableSingleRequest();
        googletag.enableServices();
    });
    @elseif($parentId == 5 || $catId == 5)
    googletag.cmd.push(function() {
        var leaderboardMapping = googletag.sizeMapping().addSize([970,90],[728,90]).build();
        var bannerMapping = googletag.sizeMapping().addSize([336,280], [300, 250]).addSize([250, 250], [320, 100],[320, 50]).build();
        // googletag.defineSlot('/24409412/Roadblock_700x300', [700, 300], 'div-gpt-ad-1501160382799-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/world_top_advert', [728, 90], 'div-gpt-ad-1486118000063-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/home_main_inner_middle_advert1', [728, 90], 'div-gpt-ad-1498022554617-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/world_rightpanel_advert1', [300, 250], 'div-gpt-ad-1486118241460-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/world_rightpanel_advert2', [300, 250], 'div-gpt-ad-1486118183654-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/world_rightpanel_advert3', [300, 250], 'div-gpt-ad-1486118284719-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/world_rightpanel_advert4', [300, 250], 'div-gpt-ad-1486118336383-0').addService(googletag.pubads());
        googletag.pubads().enableSingleRequest();
        googletag.enableServices();
    });
    @elseif($parentId == 4 || $catId == 4)
    googletag.cmd.push(function() {
        var leaderboardMapping = googletag.sizeMapping().addSize([970,90],[728,90]).build();
        var bannerMapping = googletag.sizeMapping().addSize([336,280], [300, 250]).addSize([250, 250], [320, 100],[320, 50]).build();
        // googletag.defineSlot('/24409412/Roadblock_700x300', [700, 300], 'div-gpt-ad-1501160382799-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/business_main_top_inner_advert1', [728, 90], 'div-gpt-ad-1448870249423-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/business_article_top_advert', [728, 90], 'div-gpt-ad-1464965995785-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/business_main_rightpanel_advert1', [300, 250], 'div-gpt-ad-1486031230604-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/business_main_rightpanel_advert2', [300, 250], 'div-gpt-ad-1486031093371-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/business_main_rightpanel_advert3', [300, 250], 'div-gpt-ad-1468478909150-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/business_main_rightpanel_advert4', [300, 250], 'div-gpt-ad-1449839026167-0').addService(googletag.pubads());
        googletag.pubads().enableSingleRequest();
        googletag.enableServices();
    });
    @elseif($parentId == 41 || $catId == 41)
    googletag.cmd.push(function() {
        var leaderboardMapping = googletag.sizeMapping().addSize([970,90],[728,90]).build();
        var bannerMapping = googletag.sizeMapping().addSize([336,280], [300, 250]).addSize([250, 250], [320, 100],[320, 50]).build();
        // googletag.defineSlot('/24409412/Roadblock_700x300', [700, 300], 'div-gpt-ad-1501160382799-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/health_top_advert', [728, 90], 'div-gpt-ad-1589298618797-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/health_article_top_advert', [728, 90], 'div-gpt-ad-1485841380368-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/health_main_rightpanel_advert1', [300, 250], 'div-gpt-ad-1486031342498-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/health_main_rightpanel_advert2', [300, 250], 'div-gpt-ad-1485841904663-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/health_main_rightpanel_advert3', [300, 250], 'div-gpt-ad-1508836899528-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/health_main_rightpanel_advert4', [300, 250], 'div-gpt-ad-1484899457039-0').addService(googletag.pubads());
        googletag.pubads().enableSingleRequest();
        googletag.enableServices();
    });
    @elseif($parentId == 9 || $catId == 9)
    googletag.cmd.push(function() {
        var leaderboardMapping = googletag.sizeMapping().addSize([970,90],[728,90]).build();
        var bannerMapping = googletag.sizeMapping().addSize([336,280], [300, 250]).addSize([250, 250], [320, 100],[320, 50]).build();
        // googletag.defineSlot('/24409412/Roadblock_700x300', [700, 300], 'div-gpt-ad-1501160382799-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/lifestyle_main_top_inner_advert1', [728, 90], 'div-gpt-ad-1448283785649-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/lifestyle_top_advert', [728, 90], 'div-gpt-ad-1485842297677-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/lifestyle_main_rightpanel_advert1', [300, 250], 'div-gpt-ad-1486111544943-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/lifestyle_main_rightpanel_advert2', [300, 250], 'div-gpt-ad-1463034636872-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/lifestyle_main_rightpanel_advert3', [300, 250], 'div-gpt-ad-1468479063439-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/lifestyle_main_rightpanel_advert4', [300, 250], 'div-gpt-ad-1400072127302-0').addService(googletag.pubads());
        googletag.pubads().enableSingleRequest();
        googletag.enableServices();
    });
    @else
    googletag.cmd.push(function() {
        var leaderboardMapping = googletag.sizeMapping().addSize([970,90],[728,90]).build();
        var bannerMapping = googletag.sizeMapping().addSize([336,280], [300, 250]).addSize([250, 250], [320, 100],[320, 50]).build();
        // googletag.defineSlot('/24409412/Roadblock_700x300', [700, 300], 'div-gpt-ad-1501160382799-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/home_main_top_inner_advert1', [728, 90], 'div-gpt-ad-1447154680884-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/home_main_inner_middle_advert1', [728, 90], 'div-gpt-ad-1498022554617-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/home_main_rightpanel_advert1', [300, 250], 'div-gpt-ad-1400140713631-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/home_main_rightpanel_advert2', [300, 250], 'div-gpt-ad-1486627050053-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/home_main_rightpanel_advert3', [300, 250], 'div-gpt-ad-1582865963023-0').addService(googletag.pubads());
        googletag.defineSlot('/24409412/home_main_rightpanel_advert4', [300, 250], 'div-gpt-ad-1530196033093-0').addService(googletag.pubads());
        googletag.pubads().enableSingleRequest();
        googletag.enableServices();
    });
    @endif
</script>
